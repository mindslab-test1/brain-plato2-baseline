# TURING PLATO-2

## 도커 이미지 받기 및 컨테이너 생성하기

### 도커 이미지 받기

이미지에는 모델 체크포인트로 구성된 이미지와 코드로 구성된 이미지 두 종류가 있습니다. 모델 체크포인트를 사용하시는 경우 모델 체크포인트 이미지를 받으신 뒤 코드와 체크포인트로 이루어진 최종 이미지를 생성하여 이용하시면 됩니다.  
모델 체크포인트로 구성된 이미지는 태그가 `artifacts-{model_name}-v{model_version}`과 같은 형식으로 이루어져 있습니다. 여기서 모델 버전은 코드 버전이 아닌 하이퍼파라메터, 학습 데이터 차이 등으로 인해 모델에 생긴 차이에 대한 버전을 나타냅니다. 모델 체크포인트 버전과 코드에 대한 버전 호환 여부는 아래 섹션에서 확인하실 수 있습니다.

```bash
# Pull an image that is conposed entirely of model artifacts.
docker pull docker.maum.ai:443/brain/brain_plato_2:artifacts-plato_2_kcbert_pretrained-v1
```

### 사용할 도커 이미지 생성

컨텍스트로 사용될 레퍼지토리와 태그(예시에는 v0.1.0)를 지정하여 모델 체크포인트와 코드로 이루어진 이미지를 생성합니다. `--build-arg aratifacts_image={모델 체크포인트 이미지}`와 같이 이전에 받은 모델 체크포인트 이미지를 지정합니다.

```bash
docker build \
    --build-arg artifacts_image=docker.maum.ai:443/brain/brain_plato_2:artifacts-plato_2_kcbert_pretrained-v1 \
    -t docker.maum.ai:443/brain/brain_plato_2:v0.1.0-plato_2_kcbert_pretrained-v1-packed \
    -f dockerizing/dockerfiles/Dockerfile_packed \
    https://pms.maum.ai/bitbucket/scm/brain/brain-plato2-baseline.git#v0.1.0
```

### 컨테이너 생성 예시

생성된 이미지를 이용하여 컨테이너를 생성합니다.

```bash
docker run \
    --name plato_2 \
    --gpus '"device=0"' \
    -it \
    --network host \
    --ipc host \
    --log-opt max-size=10m \
    --log-opt max-file=10 \
    docker.maum.ai:443/brain/brain_plato_2:v0.1.0-plato_2_kcbert_pretrained-v1-packed \
    /bin/bash
```

### 컨테이너 내부의 소스 코드 디렉터리로 이동하기

```
docker exec -it turing_plato_2 /bin/bash
cd /workspace/plato_2
```

## 모델 체크포인트 호환 표

### plato_2_kcbert_pretrained

메신저, 대본, 인터뷰 등 대화 형식으로 구성된 여러 종류의 데이터를 함께 이용하여 학습된 모델입니다.

|코드 버전|모델 체크포인트 호환 버전|
|:-----:|:-------------------:|
|v0.1.0 |v1                   | 

## 대화에 대한 응답 생성하기

### 대화에 대한 응답 생성 예

#### 컨테이너 내부에 존재하는 체크포인트 및 설정 파일들 이용하기

##### inference_code_sample.py

응답 생성 예시가 적혀 있는 파일입니다. 응답 후보 생성에 해당하는 부분은 GPU로 보내고 생성된 응답 후보를 평가하는 부분은 CPU에 놓는 것을 볼 수 있습니다. GPU 메모리가 부족하여 응답 후보 평가 모델을 CPU에 놓는 상황이 가정되었고, 평가 모델을 GPU에 올리는 것도 가능합니다.  
먼저 `PlatoForGeneration` 모델 클래스를 이용하여 모델을 불러 오고, `DialogueSample` 클래스를 이용하여 대화 데이터를 생성한 뒤 이 대화 데이터를 모델에 넣어 응답 후보들을 생성합니다. 입력 데이터 작성 방법과 응답 생성 파라메터 설정은 아래에 추가로 설명됩니다.

```python
from plato.plato_model import PlatoForGeneration
from base_tokenizers.base_tokenizer_factories import BaseTokenizerFactory
from dialogue_dataset.dataclasses import DialogueSample
from plato import PretrainedBertConfigFactoryForPlato2
from pprint import pprint
from pytorch_lightning import seed_everything
from plato import PretrainedBertConfigFactoryForPlato2
from jsonargparse import ArgumentParser, ActionConfigFile

def parse_args():
    parser = ArgumentParser()
    parser.add_argument(
        '--fine_grained_generation_checkpoint_path',
        default='checkpoints/stage_2_1/checkpoints/plato_stage_2_1.ckpt',
    )
    parser.add_argument(
        '--response_coherence_estimation_checkpoint_path',
        default='checkpoints/stage_2_2/checkpoints/plato_stage_2_2.ckpt'
    )
    parser.add_argument(
        '--pretrained_config_factory_config_file_path',
        default='external_pretrained/kcbert/turing_model_large/config.json'
    )
    parser.add_argument(
        '--training_config_path_for_fine_grained',
        default='checkpoints/stage_2_1/config.yaml'
    )
    parser.add_argument(
        '--training_config_path_for_response_coherence_estimation',
        default='checkpoints/stage_2_2/config.yaml'
    )
    parser.add_subclass_arguments(BaseTokenizerFactory, 'base_tokenizer_factory')
    parser.add_argument('--config', action=ActionConfigFile)

    args = parser.parse_args()
    classes = parser.instantiate_subclasses(args)
    return args, classes

if __name__ == '__main__':
    args, classes = parse_args()

    model: PlatoForGeneration = PlatoForGeneration.from_config_paths(
        fine_grained_generation_checkpoint_path=args.fine_grained_generation_checkpoint_path,
        response_coherence_estimation_checkpoint_path=args.response_coherence_estimation_checkpoint_path,
        pretrained_config_factory=PretrainedBertConfigFactoryForPlato2(
            config_file_path=args.pretrained_config_factory_config_file_path
        ),
        base_tokenizer_factory=classes['base_tokenizer_factory'],
        training_config_path_for_fine_grained=args.training_config_path_for_fine_grained,
        training_config_path_for_response_coherence_estimation=args.training_config_path_for_response_coherence_estimation,
    )

    # Move the fine-grained generation model to the first GPU.
    model.fine_grained_generation.to('cuda:0')
    # Move the response coherence estimation model to CPU.
    model.response_coherence_estimation.to('cpu')

    dialogue_samples = [
        DialogueSample(
            response_speaker=1,
            response=None,
            context=[
                '내일 정말 좋은 일이 있을 것 같아.',
                '무슨 일이 있을 것 같은데?',
                '모르겠어. 그냥 좋은 일이 있을 것 같은 느낌이 들어.'
            ],
            context_speakers=[
                0, 1, 0,
            ]
        ),
    ]

    # Make the generation deterministic (optional).
    seed_everything(1)

    output = model.generate_wrapper(
        contexts=dialogue_samples,
        max_length=1024,
        max_new_tokens=129,
        do_sample=True,
        num_beams=1,
        temperature=1.0,
        top_k=0,
        top_p=0.5,
        bad_words_ids=[[30001]],
    )

    print('Output:')
    pprint(output)
    print(f'Selected response: {output[0][0]["response"]}')
```

##### configs/sample_configs/inference_code_sample_config.yaml

위의 예시에 사용된 설정 파일 예시입니다.

```yaml
fine_grained_generation_checkpoint_path: checkpoints/stage_2_1/checkpoints/plato_stage_2_1.ckpt
response_coherence_estimation_checkpoint_path: checkpoints/stage_2_2/checkpoints/plato_stage_2_2.ckpt
pretrained_config_factory_config_file_path: external_pretrained/kcbert/turing_model_large/config.json
training_config_path_for_fine_grained: checkpoints/stage_2_1/config.yaml
training_config_path_for_response_coherence_estimation: checkpoints/stage_2_2/config.yaml
base_tokenizer_factory:
  class_path: base_tokenizers.base_tokenizer_factories.HuggingFaceTokenizerWrapperFactory
  init_args:
    tokenizer_class_path: transformers.BertTokenizer
    tokenizer_directory: external_pretrained/kcbert/turing_tokenizer_large
```

##### 코드 실행

아래와 같이 실행하시면 생성된 응답들과 최종적으로 선택된 응답이 출력됩니다.

```bash
python inference_code_sample.py --config configs/sample_configs/inference_code_sample_config.yaml
```

##### 결과

생성된 응답들을 볼 수 있습니다. 응답들은 대화 맥락에 적절한지 평가되어 스코어가 매겨지고 스코어가 높은 순으로 정렬되어 제공됩니다. `probability`와 `logit`이 스코어에 해당합니다. `logit`과 `probability`는 각각 평가 모델이 내놓은 스코어가 sigmoid에 들어가기 전의 값, sigmoid에 들어간 후의 값입니다.  
실제로 모델을 이용할 때에는 `output[0][0]["response"]`와 같이 0번째에 있는 응답을 꺼내어 사용하시면 됩니다. 응답이 두 개의 리스트 안에 갇혀 있는 이유는 배치 디코딩이 지원되기 때문입니다. 만약 한 개가 아닌 n개의 샘플을 모델에 입력하면 n개의 후보 리스트들이 생성됩니다.

```python
Output:
[[{'logit': 3.536914825439453,
   'probability': 0.9717200994491577,
   'response': '그렇군요. 좋은 일이 있을 것 같은 느낌을 받으시는군요.'},
  {'logit': 3.5147321224212646,
   'probability': 0.9711039662361145,
   'response': '좋은 일이 있을 것 같은 느낌을 받으시는군요. 무슨 일이 있으셨나요?'},
  {'logit': 3.505871057510376,
   'probability': 0.9708544015884399,
   'response': '좋은 일이 있을 것 같은 느낌이 드시는군요.'},
  {'logit': 3.505871057510376,
   'probability': 0.9708544015884399,
   'response': '좋은 일이 있을 것 같은 느낌이 드시는군요.'},
  {'logit': 3.505871057510376,
   'probability': 0.9708544015884399,
   'response': '좋은 일이 있을 것 같은 느낌이 드시는군요.'},
  {'logit': 3.505871057510376,
   'probability': 0.9708544015884399,
   'response': '좋은 일이 있을 것 같은 느낌이 드시는군요.'},
  {'logit': 3.505871057510376,
   'probability': 0.9708544015884399,
   'response': '좋은 일이 있을 것 같은 느낌이 드시는군요.'},
  {'logit': 3.5058705806732178,
   'probability': 0.9708544015884399,
   'response': '좋은 일이 있을 것 같은 느낌이 드시는군요.'},
  {'logit': 3.5058705806732178,
   'probability': 0.9708544015884399,
   'response': '좋은 일이 있을 것 같은 느낌이 드시는군요.'},
  {'logit': 3.4911680221557617,
   'probability': 0.9704354405403137,
   'response': '좋은 일이 있을 것 같은 느낌을 받으셨군요.'},
  {'logit': 3.4841291904449463,
   'probability': 0.9702328443527222,
   'response': '좋은 일이 있을 것 같은 느낌을 받으시는군요.'},
  {'logit': 3.4841291904449463,
   'probability': 0.9702328443527222,
   'response': '좋은 일이 있을 것 같은 느낌을 받으시는군요.'},
  {'logit': 3.4694831371307373,
   'probability': 0.9698068499565125,
   'response': '좋은 일이 있을 것 같은 기분이 드시는군요.'},
  {'logit': 3.4680192470550537,
   'probability': 0.969763994216919,
   'response': '좋은 일이 있을 것 같은 기분이 드시는군요. 좋은 일이 있을 것 같은 느낌이 드시겠어요.'},
  {'logit': 3.46787428855896,
   'probability': 0.9697597622871399,
   'response': '좋은 일이 있을 것 같은 느낌이 들어 기쁘시군요.'},
  {'logit': 3.4643876552581787,
   'probability': 0.969657301902771,
   'response': '좋은 일이 있을 것 같은 기분이 드시는군요. 무슨 일이 있었는지 여쭤봐도 될까요?'},
  {'logit': 3.3700549602508545,
   'probability': 0.9667555093765259,
   'response': '좋은 일이 있을 것 같은 느낌이 드시는군요. 좋은 일이 있으시길 바라요.'},
  {'logit': 3.3684699535369873,
   'probability': 0.9667044878005981,
   'response': '좋은 일이 있을 것 같은 느낌이 들어 좋은 일이 있을 것 같은 느낌이 들어 좋으시겠어요.'},
  {'logit': 3.3625404834747314,
   'probability': 0.9665130972862244,
   'response': '좋은 일이 있을 것 같은 느낌을 받으시는군요. 좋은 일이 있으시길 바라요.'},
  {'logit': 3.2566256523132324,
   'probability': 0.9629104137420654,
   'response': '좋은 일이 있으시길 바라요.'}]]
Selected response: 그렇군요. 좋은 일이 있을 것 같은 느낌을 받으시는군요.
```

#### mlflow 이용하기 (Experimental)

모델 체크포인트로 구성된 이미지가 아닌 mlflow에서 모델을 받아 응답을 생성합니다. `staging`은 내부적으로 점검하고 있는 가장 최신 모델을 의미하고 `production`은 상용화되어 있는 가장 최신 모델을 의미합니다. 예시에서는 `staging` 상태에 있는 `plato_2_kcbert_pretrained` 모델을 불러 옵니다.

##### inference_with_mlflow_code_sample.py

```python
import mlflow
from dialogue_dataset.dataclasses import DialogueSample
from pprint import pprint
from pytorch_lightning import seed_everything
from plato.plato_model import PlatoForGeneration

if __name__ == '__main__':

    print('Loading the model from mlflow.')
    loaded_model: PlatoForGeneration = mlflow.pytorch.load_model('models:/plato_2_kcbert_pretrained/staging')

    dialogue_samples = [
        DialogueSample(
            response_speaker=1,
            response=None,
            context=[
                '내일 정말 좋은 일이 있을 것 같아.',
                '무슨 일이 있을 것 같은데?',
                '모르겠어. 그냥 좋은 일이 있을 것 같은 느낌이 들어.'
            ],
            context_speakers=[
                0, 1, 0,
            ]
        ),
    ]

    # Move the fine-grained generation model to the first GPU.
    loaded_model.fine_grained_generation.to('cuda:0')
    # Move the response coherence estimation model to CPU.
    loaded_model.response_coherence_estimation.to('cpu')

    # Make the generation deterministic (optional).
    seed_everything(1)

    output = loaded_model.generate_wrapper(
        contexts=dialogue_samples,
        max_length=1024,
        max_new_tokens=129,
        do_sample=True,
        num_beams=1,
        temperature=1.0,
        top_k=0,
        top_p=0.5,
        bad_words_ids=[[30001]],
    )

    print('Output:')
    pprint(output)

    print(f'Selected response: {output[0][0]["response"]}')
```

##### 코드 실행

mlflow가 실행되고 있는 곳을 환경 변수로 함께 지정하여 코드를 실행합니다. 생성된 후보 응답들과 최종적으로 선택된 응답이 출력됩니다.

```bash
MLFLOW_TRACKING_URI='http://10.122.65.174:5000' python inference_with_mlflow_code_sample.py
```

### 대화 내용 데이터 작성 방법

```python
from dialogue_dataset.dataclasses import DialogueSample

dialogue_samples = [
    DialogueSample(
        response_speaker=1,
        response=None,
        context=[
            '내일 정말 좋은 일이 있을 것 같아.',
            '무슨 일이 있을 것 같은데?',
            '모르겠어. 그냥 좋은 일이 있을 것 같은 느낌이 들어.'
        ],
        context_speakers=[
            0, 1, 0,
        ]
    ),
]
```

* `response_speaker`: 응답에 해당하는 발화를 누가 했는지 표시합니다.
* `response`: 응답이 생성되어야 하는 경우이므로 `None`으로 지정됩니다.
* `context`: 대화 내역입니다. 대화가 이루어진 순으로 발화 내용을 입력해 주시면 됩니다.
* `context_speakers`: 대화 내역에서 누가 어떤 발화를 한 것인지 표시합니다. 대화가 이루어진 순으로 대화에 참여한 화자를 숫자로 표시해 주시면 됩니다.

예시에서는 0번 사람이 첫 번째 발화를 하였고, 1번 사람이 두 번째 발화를 한 뒤, 0번 사람이 다시 세 번째이자 마지막 발화를 한 상황을 나타냅니다. 모델은 이제 `response_speaker`에 지정된 1번 사람이 할 말을 생성하게 됩니다.

#### 유의해야 하는 것들

* 한 사람이 두 번 이상 연속으로 말할 수 없습니다. 예를 들어서 `context_speakers=[0, 1, 1, 0]`은 1번 화자가 두 번 이상 연속해서 말하므로 부적절합니다. `context_speakers=[0, 1, 2, 1]`, `response_speaker=1`도 같은 이유로 부적절합니다.
* `context_speakers`는 0부터 시작해야 합니다. 예를 들어 `context_speakers=[1, 0, 1]`은 1부터 시작하므로 부적절합니다.
* 이전에 등장하지 않았던 화자는 이전에 등장했던 화자들의 값 중 가장 큰 값에 1이 더해진 값을 갖습니다. 예를 들어 `context_speakers=[0, 1, 0, 1, 3]`은 마지막 화자가 새로운 화자라면 3이 아닌 2가 가장 오른쪽에 나와야 하므로 부적절합니다.

* `context`와 `response` 발화 하나의 길이에는 제한이 있습니다. `data['init_args']['    dialogue_validator']['init_args']` 아래 `class_path: dialogue_dataset.UtteranceNumCharValidator`가 대화 내역과 응답에 해당하는 발화 하나의 최소, 최대 캐릭터 단위 길이를 지정합니다. `data['init_args']['    dialogue_validator']['init_args']` 아래 `class_path: dialogue_dataset.UtteranceNumTokensValidator`가 대화 내역과 응답에 해당하는 발화 하나의 최소, 최대 토큰 단위 길이를 지정합니다.
* `context`에 들어갈 수 있는 발화의 개수에 제한이 있습니다. 모델 학습에 이용된 설정 파일에서 `data['init_args']['window_size']`가 이 최대 발화 수에 해당하고 여기에는 응답에 해당하는 발화도 포함됩니다.
* `context_speakers`와 `response_speaker`에 들어갈 수 있는 숫자 범위가 존재합니다. 모델 학습에 이용된 설정 파일에서 `data['init_args']['    dialogue_validator']['init_args']` 아래 `class_path: dialogue_dataset.SpeakerIdsValidator`의 `max_speaker_id`가 최대로 허용되는 값을 지정하고, `min_speaker_id`가 최소로 가질 수 있는 값을 지정합니다.

### 응답 생성 파라메터 설정 방법

```python
output = model.generate_wrapper(
    contexts=[
        DialogueSample(
            response_speaker=1,
            response=None,
            context=[
                '내일 정말 좋은 일이 있을 것 같아.',
                '무슨 일이 있을 것 같은데?',
                '모르겠어. 그냥 좋은 일이 있을 것 같은 느낌이 들어.'
            ],
            context_speakers=[
                0, 1, 0,
            ]
        ),
    ],
    max_length=1024,
    max_new_tokens=129,
    do_sample=True,
    num_beams=1,
    temperature=1.0,
    top_k=0,
    top_p=0.5,
    bad_words_ids=[[30001]],
)
```

* `contexts`: `DialgoueSample`들로 이루어진 리스트입니다. 여러 개의 샘플들로 구성될 수 있습니다.
* `max_length`: 대화 내역에 해당되는 토큰들과 응답에 해당되는 토큰들을 합쳤을 때 최대로 생성될 수 있는 길이를 지정합니다. 학습에 이용된 설정 파일의 `data['init_args']['dialogue_validator']['init_args']['encoded_dialogue_validators']` 아래 `class_path: dialogue_dataset.DialogueNumTokensValidator` 부분 `total_num_max_tokens`보다 크면 안 됩니다.
* `max_new_tokens`: 생성되는 응답이 가질 수 있는 최대 길이입니다. `data['init_args']['dialogue_validator']['init_args']['encoded_dialogue_validators']` 아래 `class_path: dialogue_dataset.UtteranceNumTokensValidator` 부분 `response_max_num_tokens`보다 2 이상 클 수 없습니다.
* `do_sample`: 디코딩할 때 샘플링을 할지 greedy로 할지 지정합니다.
* `num_beams`: beam search 혹은 beam sample에서의 beam size를 지정합니다.
* `temperature`: 디코딩 시 토큰 소프트맥스 부분에 등장하는 온도를 지정합니다.
* `top_k`: 다음 토큰 분포에서 가장 높은 확률을 가지는 k개의 토큰만을 남기는 top-k에서의 k를 지정합니다.
* `top_p`: 다음 토큰 분포를 높은 확률 순으로 정렬한 뒤 cumulative 더하기를 하였을 때 p 안에 들지 않는 토큰들을 자르는 top-p에서의 p를 지정합니다.
* `bad_words_ids`: 생성되지 않아야 하는 토큰 아이디 시퀀스 리스트를 지정합니다. 학습에 이용된 설정 파일에서 익명화와 관련된 부분을 가리는 곳에 사용된 `data['init_args']['file_loader']['init_args']['replace_system_tokens_with']` 토큰에 해당하는 토큰 아이디가 지정되어야 합니다.

## Gradio 데모 페이지 실행하기
![Gradio sample image](./assets/gradio_sample.png)

### 데모 페이지 실행 방법

##### 실행 예시

아래와 같이 입력하여 데모 페이지를 실행할 수 있습니다.

```bash
python launch_gradio.py --config configs/sample_configs/plato_gradio_sample_config.yaml
```

##### 설정 파일 예시 (configs/sample_configs/plato_gradio_sample_config.yaml)

도커 이미지 안에 함께 들어 있는 예시 설정 파일입니다.

```yaml
plato_gradio:
  class_path: __main__.PlatoGradio
  init_args:
    plato_for_generation: 
      class_path: plato.plato_model.PlatoForGeneration
      init_args:
        fine_grained_generation_checkpoint_path: checkpoints/stage_2_1/checkpoints/plato_stage_2_1.ckpt
        response_coherence_estimation_checkpoint_path: checkpoints/stage_2_2/checkpoints/plato_stage_2_2.ckpt

        pretrained_config_factory:
          class_path: plato.PretrainedBertConfigFactoryForPlato2
          init_args:
            config_file_path: external_pretrained/kcbert/turing_model_large/config.json

        fine_grained_tokenizer: 
          class_path: plato.Plato2FineGrainedGenerationTokenizer
          init_args:
            tokenizer_constructor:
              class_path: base_tokenizers.HuggingFaceTokenizerWrapperFactory
              init_args:
                tokenizer_class_path: transformers.BertTokenizer
                tokenizer_directory: external_pretrained/kcbert/turing_tokenizer_large
            eos_token_id: 3
            bos_token_id: 30000
            pad_token_id: 0
            role_pad_token_id: 8
            turn_pad_token_id: 8
            position_pad_token_id: 300

        response_coherence_tokenizer:
          class_path: plato.Plato2ResponseCoherenceEstimationTokenizer
          init_args:
            tokenizer_constructor:
              class_path: base_tokenizers.HuggingFaceTokenizerWrapperFactory
              init_args:
                tokenizer_class_path: transformers.BertTokenizer
                tokenizer_directory: external_pretrained/kcbert/turing_tokenizer_large
            eos_token_id: 3
            bos_token_id: 30000
            pad_token_id: 0
            role_pad_token_id: 8
            turn_pad_token_id: 8
            position_pad_token_id: 300
            global_first_token_id: 2
            response_replacement_prob: 0.0

    generate_kwargs: 
        max_length: 1024
        max_new_tokens: 40
        do_sample: True
        num_beams: 1
        temperature: 1.0
        top_k: 0
        top_p: 0.5
        bad_words_ids: [[30001]]

    generation_device: 'cuda:0'
    estimation_device: 'cpu'
    max_num_turns: 4

server_port: 8080
server_name: '0.0.0.0'
flagging_dir: gradio_logs

logging_file_path: 'gradio_logs/gradio_logs.log'
logging_level: INFO
logging_max_bytes: 1000000
logging_backup_count: 10
```

* `plato_gradio`
  * `generate_kwargs`: 디코딩에 사용될 파라메터를 지정합니다. 위에서 설명된 응답 생성 파라메터 설정 방법과 같습니다.
  * `generation_device`: 응답 후보 생성을 담당하는 모델의 디바이스를 지정합니다.
  * `estimation_device`: 응답 후보가 적절한지 스코어를 매기는 모델의 디바이스를 지정합니다.
  * `max_num_turns`: 최대로 입력될 수 있는 턴 횟수를 지정합니다. 사용자-모델 발화 한 쌍을 하나의 턴으로 봅니다.
  * `fine_grained_tokenizer`
    * `global_first_token_id`: 디코딩 시에는 없어야 하는 부분이기 때문에 존재하지 않습니다.
* `server_port`: 서버 포트입니다.

## 모델 체크포인트 다운로드 및 설정 파일 확인 (Experimental)

1. 브라우저를 통해 http://10.122.65.174:5000에 접속한 뒤 좌측 상단의 'Models'를 클릭합니다.
2. 원하는 모델 버전을 선택한 뒤 'Source Run'에 해당하는 부분을 클릭합니다.
3. 'Artifacts' 아래에서 모델  체크포인트 및 설정 파일을 확인할 수 있습니다.