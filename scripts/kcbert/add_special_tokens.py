from tokenizers import AddedToken
from transformers import AutoTokenizer, AutoModelWithLMHead
from argparse import ArgumentParser
from tokenizers import AddedToken

def parse_argparse_args():
    parser = ArgumentParser()
    parser.add_argument('--pretrained_tokenizer_location', default='beomi/kcbert-base')
    parser.add_argument('--pretrained_model_location', default='beomi/kcbert-base')
    parser.add_argument('--tokenizer_destination_directory', default='external_pretrained/kcbert/turing_tokenizer_base')
    parser.add_argument('--model_destination_directory', default='external_pretrained/kcbert/turing_model_base')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_argparse_args()
    tokenizer = AutoTokenizer.from_pretrained(args.pretrained_tokenizer_location)
    special_tokens_to_be_added = {
        'bos_token': '[BOS]',
        'additional_special_tokens': [
            AddedToken('<DE_ID_1>'),
            AddedToken('<DE_ID_2>'),
            AddedToken('<DE_ID_3>'),
            AddedToken('<DE_ID_4>'),
            AddedToken('<DE_ID_5>'),
            AddedToken('<DE_ID_6>'),
            AddedToken('<DE_ID_7>'),
            AddedToken('<DE_ID_8>'),
            AddedToken('<DE_ID_9>'),
            AddedToken('<DE_ID_10>'),
        ],
    }
    num_tokens_added = tokenizer.add_special_tokens(special_tokens_to_be_added)
    tokenizer.save_pretrained(args.tokenizer_destination_directory)
    print('# of tokens added:', num_tokens_added)

    model = AutoModelWithLMHead.from_pretrained(args.pretrained_model_location)
    print('Original word embeddings:', model.bert.embeddings.word_embeddings)
    model.resize_token_embeddings(len(tokenizer))
    model.save_pretrained(args.model_destination_directory)
    # Pad token index will be there when the model is initialized again from the config.
    print('New word embeddings:', model.bert.embeddings.word_embeddings)