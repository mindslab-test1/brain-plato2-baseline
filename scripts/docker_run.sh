#!/bin/bash

docker run \
    --name hylee_baseline \
    --gpus '"device=4"' \
    -it \
    -v /raid/data/engines/:/root/engines \
    -v /raid/data/open/:/root/open \
    -v $(pwd)/../external_pretrained:/root/turing/external_pretrained \
    -v $(pwd):/root/turing \
    --network host \
    --ipc host \
    --log-opt max-size=10m \
    --log-opt max-file=10 \
    nvcr.io/partners/gridai/pytorch-lightning:v1.4.0 \
    /bin/bash