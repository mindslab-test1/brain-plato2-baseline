from pytorch_lightning.utilities.cli import LightningCLI, LightningArgumentParser
from plato import PlatoResponseCoherenceEstimation
from dialogue_dataset import DialogueDataModule
import torch
from typing import Optional

class PlatoFineGrainedCLI(LightningCLI):
    def before_fit(self) -> None:
        if self.config['plato_stage_one_checkpoint_path'] is not None:
            stage_one_state_dict = torch.load(self.config['plato_stage_one_checkpoint_path'], map_location='cpu')['state_dict']
            weight_loading_result = self.model.load_state_dict(
                stage_one_state_dict, 
                strict=False,
            )
            print('Stage 1 weight load result:', weight_loading_result)

        if self.config['plato_stage_2_2_checkpoint_path'] is not None:
            weight_loading_result = self.model.load_state_dict(
                torch.load(self.config['plato_stage_2_2_checkpoint_path'], map_location='cpu')['state_dict'], 
                strict=False,
            )
            print('Stage 2.2 weight load result:', weight_loading_result)

    def add_arguments_to_parser(self, parser: LightningArgumentParser) -> None:
        parser.add_argument('--plato_stage_one_checkpoint_path', type=Optional[str])
        parser.add_argument('--plato_stage_2_2_checkpoint_path', type=Optional[str])


if __name__ == '__main__':
    cli = PlatoFineGrainedCLI(
        datamodule_class=DialogueDataModule,
        model_class=PlatoResponseCoherenceEstimation,
        subclass_mode_data=True,
        # subclass_mode_model=True,
        save_config_overwrite=True,
    )
