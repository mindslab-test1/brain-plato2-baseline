from pytorch_lightning.utilities.cli import LightningCLI, LightningArgumentParser
from plato import (
    PretrainedWeightFactoryForPlato2,
    PlatoCoarseGrainedGeneration,
)
from dialogue_dataset import (
    DialogueDataModule
)
import torch

class TempLightningCLI(LightningCLI):
    
    def before_fit(self) -> None:
        self.model.load_pretrained_weights(self.config_init['pretrained_weight_factory'])
    
    def add_arguments_to_parser(self, parser: LightningArgumentParser) -> None:
        parser.add_subclass_arguments(PretrainedWeightFactoryForPlato2, 'pretrained_weight_factory')
        #parser.add_subclass_arguments(torch.optim.Adam, 'optimizer')
        #parser.add_subclass_arguments(torch.optim.lr_scheduler.LambdaLR, 'lr_scheduler')
        #parser.link_arguments('optimizer', 'model.init_args.optimizer_init')
        #parser.link_arguments('lr_scheduler', 'model.init_args.lr_scheduler_init')

        # parser.add_subclass_arguments(torch.optim.Adam, link_to='model.init_args.optimizer_init')
        # parser.add_lr_scheduler_args((torch.optim.lr_scheduler.LambdaLR,), link_to='model.init_args.lr_scheduler_init')
        # parser.add_lr_scheduler_args(torch.optim.lr_scheduler.ExponentialLR)


if __name__ == '__main__':
    cli = TempLightningCLI(
        PlatoCoarseGrainedGeneration,
        DialogueDataModule,
        subclass_mode_data=True,
        subclass_mode_model=True,
        save_config_overwrite=True,
    )
