from preprocessing.dialogue_preprocessing import utterance_processor
import re
import json, pathlib, itertools
from .dataclasses import (
    RawDialogue,
    DialogueSample,
)
from abc import ABC, abstractmethod
from typing import Optional, List, Sequence, Union
from tqdm import tqdm

class DialogueFileLoader(ABC):
    @abstractmethod
    def load_files(self):
        pass

class TuringProjectJsonLoader(DialogueFileLoader):
    def __init__(self, data_directory: str, glob_pattern: str = '**/*.json'):
        self.data_directory = data_directory
        self.glob_pattern = glob_pattern
    
    def load_file(self, file_path):
        with open(file_path) as f:
            dialogues = json.load(f)
        dialogues = dialogues['data']
        dialogues = [
            zip(*[ (utterance['utterance'], ord(utterance['speaker']) - 65) for utterance in dialogue['dialog']])
            for dialogue in dialogues
        ] 
        dialogues = [RawDialogue(utterances=list(utterances), speakers=list(speakers)) for utterances, speakers in dialogues]
        return dialogues
    
    def load_files(self):
        file_paths = pathlib.Path(self.data_directory).glob(self.glob_pattern)
        dialogues = list(itertools.chain.from_iterable([self.load_file(file_path) for file_path in file_paths]))
        return dialogues


class TuringProjectSystemTokenSubstitutionJsonLoader(DialogueFileLoader):
    def __init__(
        self, 
        data_directory: Union[str, Sequence[str]],
        glob_pattern: Optional[str] = '**/*.json',
        replace_system_tokens_with: Optional[str] = None,
    ):
        self.data_directory = [data_directory] if isinstance(data_directory, str) else data_directory
        self.glob_pattern = glob_pattern
        self.replace_system_tokens_with = replace_system_tokens_with
    
    def load_file(self, file_path: str):
        with open(file_path) as f:
            dialogues_file = json.load(f)

        dialogues = dialogues_file['data']

        dialogues = [
            zip(*[ (utterance['utterance'], utterance['speaker']) for utterance in dialogue['dialogue']])
            for dialogue in dialogues
        ] 
        dialogues = [RawDialogue(utterances=list(utterances), speakers=list(speakers)) for utterances, speakers in dialogues]

        system_token_patterns = dialogues_file['system_token_patterns'] if 'system_token_patterns' in dialogues_file else None
        if system_token_patterns is not None and self.replace_system_tokens_with is not None:
            self.replace_system_tokens_in_dialogues(raw_dialogues=dialogues, system_token_pattern='|'.join(system_token_patterns))

        return dialogues
    
    def load_files(self):
        file_paths = [pathlib.Path(data_directory).glob(self.glob_pattern) for data_directory in self.data_directory]
        file_paths = list(itertools.chain.from_iterable(file_paths))
        dialogues = list(itertools.chain.from_iterable([self.load_file(file_path) for file_path in tqdm(file_paths)]))
        return dialogues
    
    def replace_system_tokens_in_dialogues(self, raw_dialogues: List[RawDialogue], system_token_pattern: str):
        pattern = re.compile(system_token_pattern)
        for raw_dialogue in raw_dialogues:
            # for utterance in raw_dialogue.utterances:
            #     if pattern.search(utterance) is not None:
            #         print(utterance)
            #         print(pattern.sub(self.replace_system_tokens_with, utterance))
            #         print()
            raw_dialogue.utterances = [
                pattern.sub(self.replace_system_tokens_with, utterance) 
                for utterance in raw_dialogue.utterances
            ]