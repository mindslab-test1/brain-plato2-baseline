from __future__ import annotations
from base_tokenizers.base_tokenizers import BaseTokenizerInterface
from dataclasses import dataclass
import dataclasses, random
from typing import Callable, List, Sequence, Union, Optional, Generator, Tuple
import copy

def get_char_for_random_init() -> str:
    """Only used when testing the class. Should be ignored.
    """
    return '0'

def get_id_for_random_init() -> str:
    """Only used when testing the class. Should be ignored.
    """
    # TODO: The range could be given as an argument from the caller.
    return random.randint(0, 1000000)

@dataclass
class RawDialogue:
    """A class representation of a raw string dialogue.

    Attributes:
        utterances (List[str]): A list of utterances in string form.
        speakers (List[int], optional): A list of speakers in integer form. 
            Each integer uniquely identifies a corresponding speaker.
            Defaults to None.
    """
    utterances: List[str]
    speakers: List[int] = None

    def __post_init__(self):
        """Checks if the lengths of the utterances and the speakers match.

        Raises:
            AssertionError: If the lengths of the utterances and the speakers are not equal to each other.
        """
        if self.speakers is not None:
            assert len(self.utterances) == len(self.speakers)

    def __len__(self) -> int:
        """Returns the number of utterances.

        Returns:
            int: The number of utterances.
        """
        return len(self.utterances)

    def iterate_with_window(
        self, 
        window_size: int, 
        start: Optional[int] = 0, 
        end: Optional[int] = None
    ) -> Generator[DialogueSample, None, None]:
        """Iterate the raw dialogue by sliding a window of size 'window_size'.

        Args:
            window_size (int): The window size of the sliding window.
            start (Optional[int], optional): The starting index of the response (inclusive). Defaults to 0.
            end (Optional[int], optional): The ending index of the response (exclusive). Defaults to the length of the list of utterances.

        Yields:
            Generator[DialogueSample, None, None]: An instance of DialougeSample class sliced by the sliding window.
        
        Examples:
            >>> from dialogue_dataset.dataclasses import RawDialogue
            >>> raw_dialogue = RawDialogue(['0', '1', '2', '3', '4', '5'], [0, 1, 2, 3, 4, 5])
            >>> for dialogue_sample in raw_dialogue.iterate_with_window(3, 1, 5):
            ...     print(dialogue_sample)
            ... 
            DialogueSample(response_speaker=1, response='1', context_speakers=[0], context=['0'])
            DialogueSample(response_speaker=2, response='2', context_speakers=[0, 1], context=['0', '1'])
            DialogueSample(response_speaker=3, response='3', context_speakers=[1, 2], context=['1', '2'])
            DialogueSample(response_speaker=4, response='4', context_speakers=[2, 3], context=['2', '3'])
        """
        end = len(self) if end is None else end
        assert end <= len(self), 'end must be less than or equal to the length of the dialogue.'

        for end_index in range(start, end):
            start_idx = max(end_index - window_size + 1, 0)

            sample = DialogueSample(
                context=self.utterances[start_idx:end_index],
                response=self.utterances[end_index],
                context_speakers=self.speakers[start_idx:end_index],
                response_speaker=self.speakers[end_index]
            )
            yield copy.deepcopy(sample)
        return

    def transform_by(self, utterance_transform: Callable[[str], str]):
        """A method that does not seem to be used by any of the other modules.

        Args:
            utterance_transform (Callable[[str], str]): An utterance transform function.
        """
        # TODO: This method does not seem to be used at all. Remove it.
        self.utterances = [utterance_transform(utterance) for utterance in self.utterances]

@dataclass
class DialogueSample:
    """A class representation of a context, response pair of a dialogue.

    Attributes:
        response_speaker (int): A response speaker representation written as an integer.
        response (Union[str, List[int]]): A response. Defaults to ''.
        context_speakers (List[int]): A list of context speaker representations in integer form. Defaults to [].
        context (List[Union[str, List[int]]]): A list of contexts. Defaults to [].
    """
    response_speaker: int 
    response: Union[str, List[int]] = ''
    context_speakers: List[int] = dataclasses.field(default_factory=list)
    context: List[Union[str, List[int]]] = dataclasses.field(default_factory=list)

    def __post_init__(self):
        """Checks if the lengths of the contexts and the context speakers are the same.

        Raises:
            AssertionError: If the lengths of the contexts and the context speakers are not the same.
        """
        if self.response is None:
            self.response = ''
        if self.context is None:
            self.context = []
        assert len(self.context) == len(self.context_speakers), f'context, context speakers lengths are not the same. {self}'

    def is_encoded(self) -> bool:
        """Checks whether the dialogue sample is encoded or not encoded.

        Note:
            The edge caeses are not considered.

        Returns:
            bool: True if the sample is encoded, False if not encoded.
        """
        # # TODO: Well, let's take care of the edge cases later.
        if isinstance(self.response, str) and all((isinstance(context_utterance, str) for context_utterance in self.context)):
            return False
        else:
            return True

    def encode_with(self, tokenizer: BaseTokenizerInterface):
        """Encodes the dialogue sample with a DialogueTokenizer.

        Args:
            tokenizer (BaseTokenizerInterface): An object that supports BaseTokenizerInterface, with which the dialogue sample will be encoded.
        """
        if not self.is_encoded():
            self.context = [tokenizer.encode(context_utterance) for context_utterance in self.context]
            self.response = tokenizer.encode(self.response)
    
    @classmethod
    def make_random_string_dialogue_sample(
        self, 
        num_contexts_range: Tuple[int, int],
        context_length_range: Tuple[int, int],
        context_speaker_range: Tuple[int, int],
        response_length_range: Tuple[int, int],
        response_speaker_range: Tuple[int, int],
    ):
        """Only used when testing the class. Should be ignored.
        """
        context = [get_char_for_random_init() * random.randint(*context_length_range) for _ in range(random.randint(*num_contexts_range))]
        dialogue_sample = DialogueSample(
            response=get_char_for_random_init() * random.randint(*response_length_range),
            context=context,
            response_speaker=random.randint(*response_speaker_range),
            context_speakers=[random.randint(*context_speaker_range) for _ in range(len(context))],
        )
        return dialogue_sample

    @classmethod
    def make_random_encoded_dialogue_sample(
        self, 
        num_contexts_range: Tuple[int, int],
        context_length_range: Tuple[int, int],
        context_speaker_range: Tuple[int, int],
        response_length_range: Tuple[int, int],
        response_speaker_range: Tuple[int, int],
    ):
        """Only used when testing the class. Should be ignored.
        """
        context = [[get_id_for_random_init()] * random.randint(*context_length_range) for _ in range(random.randint(*num_contexts_range))]
        dialogue_sample = DialogueSample(
            response=[get_id_for_random_init()] * random.randint(*response_length_range),
            context=context,
            response_speaker=random.randint(*response_speaker_range),
            context_speakers=[random.randint(*context_speaker_range) for _ in range(len(context))],
        )
        return dialogue_sample