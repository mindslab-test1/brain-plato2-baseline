from typing import Callable, Sequence, List, Optional
from transformers.utils.dummy_pt_objects import Seq2SeqTrainer
from .dialogue_dataset import DialogueSample


# TODO: Inherit from these.
class EncodedDialogueValidator:
    def is_valid(self, dialogue_sample: DialogueSample) -> bool:
        assert dialogue_sample.is_encoded(), 'The dialogue sample is not encoded.'


class StringDialogueValidator:
    def is_valid(self, dialogue_sample: DialogueSample) -> bool:
        assert not dialogue_sample.is_encoded(), 'The dialogue sample is encoded.'


class UtteranceNumCharValidator(StringDialogueValidator):
    def __init__(
        self,
        context_min_num_chars: Optional[int] = float('-inf'), 
        context_max_num_chars: Optional[int] = float('inf'),
        response_min_num_chars: Optional[int] = float('-inf'), 
        response_max_num_chars: Optional[int] = float('inf'),
        allow_response_len_zero: bool = True,
    ):
        self.context_min_num_chars = context_min_num_chars
        self.context_max_num_chars = context_max_num_chars
        self.response_min_num_chars = response_min_num_chars
        self.response_max_num_chars = response_max_num_chars
        self.allow_response_len_zero = allow_response_len_zero

    def is_valid(self, dialogue_sample: DialogueSample) -> bool:
        if not all([self.context_min_num_chars <= len(utterance) <= self.context_max_num_chars for utterance in dialogue_sample.context]):
            return False

        if self.allow_response_len_zero and len(dialogue_sample.response) == 0:
            if not len(dialogue_sample.response) <= self.response_max_num_chars:
                return False
        else:
            if not self.response_min_num_chars <= len(dialogue_sample.response) <= self.response_max_num_chars:
                return False

        return True


class AllowedResponseSpeakerIdsValidator(StringDialogueValidator):
    def __init__(
        self,
        allowed_response_speaker_ids : Sequence[int],
    ):
        self.allowed_response_speaker_ids = {*allowed_response_speaker_ids}
    
    def is_valid(self, dialogue_sample: DialogueSample) -> bool:
        if dialogue_sample.response_speaker is None:
            return False

        if dialogue_sample.response_speaker not in self.allowed_response_speaker_ids:
            return False
        
        return True


class DialogueNumTokensValidator(EncodedDialogueValidator):
    def __init__(
        self, 
        num_system_tokens_in_context_unrelated_to_utterance: int,
        num_system_tokens_per_context_utterance: int,
        num_system_tokens_in_non_empty_response: int,
        num_system_tokens_in_empty_response: int,
        # TODO: inf could have been used.
        context_num_max_tokens: Optional[int] = None,
        response_num_max_tokens: Optional[int] = None,
        total_num_max_tokens: Optional[int] = None,
    ):
        self.context_num_max_tokens = context_num_max_tokens
        self.response_num_max_tokens = response_num_max_tokens
        self.total_num_max_tokens = total_num_max_tokens
        self.num_system_tokens_in_context_not_related_to_utterance = num_system_tokens_in_context_unrelated_to_utterance
        self.num_system_tokens_per_context_utterance = num_system_tokens_per_context_utterance
        self.num_system_tokens_in_non_empty_response = num_system_tokens_in_non_empty_response
        self.num_system_tokens_in_empty_response = num_system_tokens_in_empty_response

    def is_valid(self, dialogue_sample: DialogueSample) -> bool:
        # E.g., CLS token at the start of the context.
        num_context_system_tokens = self.num_system_tokens_in_context_not_related_to_utterance

        # E.g., EOS tokens in the context
        num_context_system_tokens += len(dialogue_sample.context) * self.num_system_tokens_per_context_utterance

        # E.g., BOS, EOS tokens in the response.
        if len(dialogue_sample.response) > 0:
            num_response_system_tokens = self.num_system_tokens_in_non_empty_response
        else:
            num_response_system_tokens = self.num_system_tokens_in_empty_response

        num_context_tokens = sum([len(utterance) for utterance in dialogue_sample.context])

        num_response_tokens = len(dialogue_sample.response) if dialogue_sample.response is not None else 0

        total_num_tokens = num_context_tokens + num_response_tokens + num_context_system_tokens + num_response_system_tokens

        if self.total_num_max_tokens is not None and total_num_tokens > self.total_num_max_tokens:
            return False
        
        if self.response_num_max_tokens is not None and num_response_tokens + num_response_system_tokens > self.response_num_max_tokens:
            return False

        if self.context_num_max_tokens is not None and num_context_tokens + num_context_system_tokens > self.context_num_max_tokens:
            return False

        return True


class UtteranceNumTokensValidator(EncodedDialogueValidator):
    def __init__(
        self, 
        context_min_num_tokens: Optional[int] = float('-inf'), 
        context_max_num_tokens: Optional[int] = float('inf'),
        response_min_num_tokens: Optional[int] = float('-inf'), 
        response_max_num_tokens: Optional[int] = float('inf'),
    ):
        self.context_min_num_tokens = context_min_num_tokens
        self.context_max_num_tokens = context_max_num_tokens
        self.response_min_num_tokens = response_min_num_tokens
        self.response_max_num_tokens = response_max_num_tokens

    def is_valid(self, dialogue_sample: DialogueSample) -> bool:
        if not all([self.context_min_num_tokens <= len(utterance) <= self.context_max_num_tokens for utterance in dialogue_sample.context]):
            return False

        if not self.response_min_num_tokens <= len(dialogue_sample.response) <= self.response_max_num_tokens:
            return False

        return True


class SpeakerIdsValidator(EncodedDialogueValidator):
    def __init__(
        self, 
        max_speaker_id: int, 
        min_speaker_id: int = 0
    ):
        self.min_speaker_id = min_speaker_id
        self.max_speaker_id = max_speaker_id

    def is_valid(self, dialogue_sample: DialogueSample) -> bool:
        if len(dialogue_sample.context_speakers) > 0:
            # TODO: Do not iterate twice.
            if min(dialogue_sample.context_speakers) < self.min_speaker_id:
                return False

            if max(dialogue_sample.context_speakers) > self.max_speaker_id:
                return False

        if dialogue_sample.response_speaker < self.min_speaker_id:
            return False

        if dialogue_sample.response_speaker > self.max_speaker_id:
            return False
        
        return True


class DialogueValidator:
    def __init__(
        self, 
        encoded_dialogue_validators: List[EncodedDialogueValidator] = None, 
        string_dialogue_validators: List[StringDialogueValidator] = None,
    ):
        self.encoded_dialogue_validators = encoded_dialogue_validators
        self.string_dialogue_validators = string_dialogue_validators

    def is_valid_encoded_dialogue(self, dialogue_sample: DialogueSample) -> bool:
        assert dialogue_sample.is_encoded(), 'The dialogue sample is not encoded.'

        if self.encoded_dialogue_validators is None:
            return True

        for dialogue_validator in self.encoded_dialogue_validators:
            if not dialogue_validator.is_valid(dialogue_sample):
                return False
        return True

    def is_valid_string_dialogue(self, dialogue_sample: DialogueSample) -> bool:
        assert not dialogue_sample.is_encoded(), 'The dialogue sample is encoded.'

        if self.string_dialogue_validators is None:
            return True

        for dialogue_validator in self.string_dialogue_validators:
            if not dialogue_validator.is_valid(dialogue_sample):
                return False
        return True