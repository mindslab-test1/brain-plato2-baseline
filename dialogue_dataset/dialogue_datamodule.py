from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, dataloader
import random
from typing import Union, List, Dict, Optional, Literal
from dialogue_dataset import (
    IterableDialogueDataset, 
)
from .dialogue_tokenizer import DialogueTokenizer
from .file_loaders import DialogueFileLoader
from .dialogue_sample_transforms import DialogueSampleTransform
from .raw_dialogue_transforms import RawDialogueTransform
from .validators import DialogueValidator
from bucketed_datasets.iterable import (
    BucketedIterableDataset, 
    BucketedIterableDatasetCallback
)
from bucketed_datasets.length_calculators import LengthCalculator

class DialogueDataModule(LightningDataModule):
    """Pytorch Lightning Data Module for dialouge datasets.
    """
    def __init__(
            self, 
            file_loader: DialogueFileLoader,
            val_split_seed: int,
            val_split_ratio: float,
            max_length_per_batch: int,
            num_samples_in_pool: int,
            num_buckets: int,
            tokenizer: DialogueTokenizer,
            window_size: int,
            num_workers: int,
            prefetch_factor: int,
            pin_memory: bool,
            dialogue_validator: DialogueValidator,
            length_calculator: LengthCalculator,
            shuffle: bool = True,
            raw_dialogue_transform: Optional[RawDialogueTransform] = None,
            string_dialogue_sample_transform: Optional[DialogueSampleTransform] = None,
            bucketed_iterable_dataset_callbacks: List[BucketedIterableDatasetCallback] = [],
            infinite: bool = False,
            start_index: Optional[int] = 0,
        ):
        """Initializes the datamodule with its arguments.

        Args:
            file_loader (DialogueFileLoader): A DialougeFileLoader which dictates how data files should be loaded.
            val_split_seed (int): A validation split seed with which train/validation sets will be split.
            val_split_ratio (float): The percentage of validation samples in train/validation sets.
                "ratio" may not have been the best term to describe the argument.
            max_length_per_batch (int): The maximum size of a batch.
                Guarantees that max(*next_sample_lengths) * len(next_samples) is less than or equal to max_length_per_batch.
            num_samples_in_pool (int): The maximum number of samples a pool can contain.
            num_buckets (int): The number of buckets.
            tokenizer (DialogueTokenizer): A DialogueTokenizer instance that is responsible for encoding dialogues and collating samples.
            window_size (int): The size of the sliding window.
            num_workers (int): The num_workers argument to the pytorch DataLoader class.
            prefetch_factor (int): The prefetch_factor argument to the pytorch DataLoader class.
            pin_memory (bool): The pin_memory argument to the pytorch DataLoader Class.
            dialogue_validator (DialogueValidator): A DialogueValidator instance that filters out invalid dialogue samples.
            length_calculator (LengthCalculator): Used when calculating a dialogue sample's length to bucketize samples.
            shuffle (bool, optional): If True, shuffles the raw dialogues before iterating over the them. Defaults to True.
            raw_dialogue_transform (Optional[RawDialogueTransform], optional): A transformation that dictates how a raw dialogue should be
                transformed before making samples of dialogues by slicing it with a sliding window. Defaults to None.
            string_dialogue_sample_transform (Optional[DialogueSampleTransform], optional): A transformation that dictates how an unencoded 
                dialouge sample should be transformed before encoding it with a dialogue tokenizer. Defaults to None.
            bucketed_iterable_dataset_callbacks (List[BucketedIterableDatasetCallback], optional): A list of callbacks to carry out extra
                works such as drawing a progress bar. Defaults to [].
            infinite (bool, optional): If True, it will cycle the dataset infinitely. Defaults to False.
            start_index (Optional[int], optional): The starting index of the response. If set to 1, for example, the first utterance in a
                raw dialogue will not be chosen as the response when making samples of dialogues. Defaults to 0.
        """
        super().__init__()
        self.tokenizer = tokenizer
        self.file_loader = file_loader
        self.val_split_seed = val_split_seed
        self.val_split_ratio = val_split_ratio
        self.window_size = window_size
        self.num_workers = num_workers
        self.prefetch_factor = prefetch_factor
        self.dialogue_validator = dialogue_validator
        self.shuffle = shuffle
        self.raw_dialogue_transform = raw_dialogue_transform
        self.max_length_per_batch = max_length_per_batch
        self.num_samples_in_pool = num_samples_in_pool
        self.num_buckets = num_buckets
        self.bucketed_iterable_datset_callbacks = bucketed_iterable_dataset_callbacks
        self.string_dialogue_sample_transform = string_dialogue_sample_transform
        self.length_calculator = length_calculator
        self.pin_memory = pin_memory
        self.infinite = infinite
        self.start_index = start_index

    def prepare_data(self):
        pass

    def setup(self, stage: Literal['fit']) -> None:
        """Makes train/validation splits, and initializes datasets.

        Args:
            stage (Literal['fit']): Only 'fit' is supported.
        """
        if stage == 'fit':
            dialogues = self.file_loader.load_files()
            num_utterances = [len(dialogue) for dialogue in dialogues]
            
            dialogue_num_utterance_pairs = list(zip(dialogues, num_utterances))
            total_num_utterances = sum(num_utterances)

            val_dialogues_list = []
            train_dialogues_list = []
            cur_val_utterances = 0
            random.Random(self.val_split_seed).shuffle(dialogue_num_utterance_pairs)

            for dialogue, num_utterance in dialogue_num_utterance_pairs: 
                if self.val_split_ratio > cur_val_utterances / total_num_utterances:
                    cur_val_utterances += num_utterance
                    val_dialogues_list.append(dialogue)
                else:
                    train_dialogues_list.append(dialogue)

            dataset_constructor = lambda x: IterableDialogueDataset(
                dialogues=x,
                window_size=self.window_size,
                tokenizer=self.tokenizer,
                dialogue_validator=self.dialogue_validator,
                shuffle=self.shuffle,
                raw_dialogue_transform=self.raw_dialogue_transform,
                string_dialogue_sample_transform=self.string_dialogue_sample_transform,
                start_index=self.start_index,
            )

            self.train = BucketedIterableDataset(
                samples=train_dialogues_list,
                dataset_constructor=dataset_constructor,
                length_calculator=self.length_calculator,
                max_length_per_batch=self.max_length_per_batch,
                num_samples_in_pool=self.num_samples_in_pool,
                num_buckets=self.num_buckets,
                callbacks=self.bucketed_iterable_datset_callbacks,
                infinite=self.infinite,
            )
            
            self.val = BucketedIterableDataset(
                samples=val_dialogues_list,
                dataset_constructor=dataset_constructor,
                length_calculator=self.length_calculator,
                max_length_per_batch=self.max_length_per_batch,
                num_samples_in_pool=self.num_samples_in_pool,
                num_buckets=self.num_buckets,
                callbacks=self.bucketed_iterable_datset_callbacks,
            )

    def train_dataloader(self) -> DataLoader:
        """Returns the train data loader.

        Returns:
            DataLoader: The train data loader.
        """
        return DataLoader(
            dataset=self.train,
            collate_fn=self.tokenizer.collate_fn,
            batch_size=None,
            num_workers=self.num_workers,
            prefetch_factor=self.prefetch_factor,
            pin_memory=self.pin_memory,
        )

    def val_dataloader(self) -> DataLoader:
        """Returns the validation data loader.

        Returns:
            DataLoader: The validation data loader.
        """
        return DataLoader(
            dataset=self.val,
            collate_fn=self.tokenizer.collate_fn,
            batch_size=None,
            num_workers=self.num_workers,
            prefetch_factor=self.prefetch_factor,
            pin_memory=self.pin_memory,
        )