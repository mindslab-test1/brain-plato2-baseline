from unittest import TestCase
from dialogue_dataset.validators import UtteranceNumCharValidator
from dialogue_dataset.dataclasses import DialogueSample
import random, copy
        

class TestUtteranceNumCharValidator(TestCase):
    VALIDATOR_CONTEXT_NUM_RANGE = [0, 100]
    VALIDATOR_RESPONSE_NUM_CHARS_RANGE = [0, 1000]
    VALIDATOR_ALLOW_RESPONSE_LEN_ZERO_PROB = 0.5

    NUM_CONTEXTS_RANGE = [0, 100]
    CONTEXT_LENGTH_RANGE = [0, 1000]
    CONTEXT_SPEAKER_RANGE = [0, 100]
    RESPONSE_LENGTH_RANGE = [0, 1000]
    RESPONSE_SPEAKER_RANGE = [0, 100]

    SEED = 0

    def test_with_random_arguments(self):
        random.seed(TestUtteranceNumCharValidator.SEED)
        for _ in range(10000):
            # TODO: Make a dataclass that takes care of this logic.
            context_num_chars = [random.randint(*TestUtteranceNumCharValidator.VALIDATOR_CONTEXT_NUM_RANGE) for _ in range(2)]
            context_min_num_chars, context_max_num_chars = sorted(context_num_chars)
            response_num_chars = [random.randint(*TestUtteranceNumCharValidator.VALIDATOR_RESPONSE_NUM_CHARS_RANGE) for _ in range(2)]
            response_min_num_chars, response_max_num_chars = sorted(response_num_chars)
            allow_response_len_zero = True if random.random() < TestUtteranceNumCharValidator.VALIDATOR_ALLOW_RESPONSE_LEN_ZERO_PROB else False

            validator = UtteranceNumCharValidator(
                context_min_num_chars=context_min_num_chars,
                context_max_num_chars=context_max_num_chars,
                response_min_num_chars=response_min_num_chars,
                response_max_num_chars=response_max_num_chars,
                allow_response_len_zero=allow_response_len_zero,
            )
            dialogue_sample = DialogueSample.make_random_string_dialogue_sample(
                num_contexts_range=TestUtteranceNumCharValidator.NUM_CONTEXTS_RANGE,
                context_length_range=TestUtteranceNumCharValidator.CONTEXT_LENGTH_RANGE,
                context_speaker_range=TestUtteranceNumCharValidator.CONTEXT_SPEAKER_RANGE,
                response_length_range=TestUtteranceNumCharValidator.RESPONSE_LENGTH_RANGE,
                response_speaker_range=TestUtteranceNumCharValidator.RESPONSE_SPEAKER_RANGE
            )

            dialogue_sample_before_validation = copy.deepcopy(dialogue_sample)

            context_is_valid = all(
                [context_min_num_chars <= len(utterance) <= context_max_num_chars for utterance in dialogue_sample.context]
            )
            if validator.is_valid(dialogue_sample):
                self.assertTrue(context_is_valid)
                if len(dialogue_sample.response) == 0 and allow_response_len_zero:
                    self.assertTrue(len(dialogue_sample.response) <= response_max_num_chars)
                else:
                    self.assertTrue(response_min_num_chars <= len(dialogue_sample.response) <= response_max_num_chars)
            else:
                if len(dialogue_sample.response) == 0 and allow_response_len_zero:
                    self.assertFalse(
                        context_is_valid 
                        and len(dialogue_sample.response) <= response_max_num_chars
                    )
                else:
                    self.assertFalse(
                        context_is_valid 
                        and (response_min_num_chars <= len(dialogue_sample.response) <= response_max_num_chars)
                    )
            
            self.assertEqual(dialogue_sample, dialogue_sample_before_validation)