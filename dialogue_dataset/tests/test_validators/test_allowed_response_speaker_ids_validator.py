from unittest import TestCase
from dialogue_dataset.validators import AllowedResponseSpeakerIdsValidator
from dialogue_dataset.dataclasses import DialogueSample
from typing import Optional

def _construct_dialogue_sample_with_given_response_speaker_id(
    response_speaker_id: Optional[int] = None,
):
    dialogue_sample = DialogueSample(
        response_speaker=response_speaker_id,
        response='Some random string',
        context_speakers=[0, 1, 2],
        context=['0', '1', '2'],
    )
    return dialogue_sample

class TestAllowedResponseSpeakerValidator(TestCase):
    def test_if_return_value_is_false_for_disallowed_response_speaker_ids(self):
        allowed_response_speaker_ids = [1, 3, 7]
        response_speaker_ids_validator = AllowedResponseSpeakerIdsValidator(
            allowed_response_speaker_ids=allowed_response_speaker_ids
        )

        for disallowed_response_speaker_id in [0, 2, 4, 5, 6]:
            dialogue_sample = _construct_dialogue_sample_with_given_response_speaker_id(disallowed_response_speaker_id)
            self.assertFalse(response_speaker_ids_validator.is_valid(dialogue_sample))

    def test_if_return_value_is_true_for_allowed_response_speaker_ids(self):
        allowed_response_speaker_ids = [1, 3, 7]
        response_speaker_ids_validator = AllowedResponseSpeakerIdsValidator(
            allowed_response_speaker_ids=allowed_response_speaker_ids
        )

        for allowed_response_speaker_id in allowed_response_speaker_ids:
            dialogue_sample = _construct_dialogue_sample_with_given_response_speaker_id(allowed_response_speaker_id)
            self.assertTrue(response_speaker_ids_validator.is_valid(dialogue_sample))