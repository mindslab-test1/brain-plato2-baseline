from unittest import TestCase
from dialogue_dataset.validators import DialogueNumTokensValidator
from dialogue_dataset.dataclasses import DialogueSample
import random, copy
from dataclasses import dataclass, asdict
from typing import Optional

@dataclass
class DialogueNumTokensValidatorArguments:
    num_system_tokens_in_context_unrelated_to_utterance: int
    num_system_tokens_per_context_utterance: int
    num_system_tokens_in_non_empty_response: int
    num_system_tokens_in_empty_response: int
    context_num_max_tokens: Optional[int] = None
    response_num_max_tokens: Optional[int] = None
    total_num_max_tokens: Optional[int] = None


class TestDialogueNumTokensValidator(TestCase):
    NUM_SYSTEM_TOKENS_IN_CONTEXT_UNRELATED_TO_UTTERANCE_RANGE = [0, 10]
    NUM_SYSTEM_TOKENS_PER_CONTEXT_UTTERANCE_RANGE = [0, 10]
    NUM_SYSTEM_TOKENS_IN_NON_EMPTY_RESPONSE_RANGE = [0, 10]
    NUM_SYSTEM_TOKENS_IN_EMPTY_RESPONS_RANGE = [0, 10]
    CONTEXT_NUM_MAX_TOKENS_RANGE = [0, 1000]
    RESPONSE_NUM_MAX_TOKENS_RANGE = [0, 1000]
    TOTAL_NUM_MAX_TOKENS_RANGE = [0, 3000]

    CONTEXT_NUM_MAX_TOKENS_NONE_PROB = 0.1
    RESPONSE_NUM_MAX_TOKENS_NONE_PROB = 0.1
    TOTAL_NUM_MAX_TOKENS_NONE_PROB = 0.1

    NUM_CONTEXTS_RANGE = [0, 100]
    CONTEXT_LENGTH_RANGE = [0, 1000]
    CONTEXT_SPEAKER_RANGE = [0, 100]
    RESPONSE_LENGTH_RANGE = [0, 1000]
    RESPONSE_SPEAKER_RANGE = [0, 100]

    SEED = 0

    def test_with_random_arguments(self):
        random.seed(TestDialogueNumTokensValidator.SEED)
        # TODO: Why does it take so long?
        for _ in range(300):
            validator_arguments = DialogueNumTokensValidatorArguments(
                num_system_tokens_in_context_unrelated_to_utterance=random.randint(
                    *TestDialogueNumTokensValidator.NUM_SYSTEM_TOKENS_IN_CONTEXT_UNRELATED_TO_UTTERANCE_RANGE
                ),
                num_system_tokens_per_context_utterance=random.randint(
                    *TestDialogueNumTokensValidator.NUM_SYSTEM_TOKENS_PER_CONTEXT_UTTERANCE_RANGE
                ),
                num_system_tokens_in_non_empty_response=random.randint(
                    *TestDialogueNumTokensValidator.NUM_SYSTEM_TOKENS_IN_NON_EMPTY_RESPONSE_RANGE
                ),
                num_system_tokens_in_empty_response=random.randint(
                    *TestDialogueNumTokensValidator.NUM_SYSTEM_TOKENS_IN_EMPTY_RESPONS_RANGE
                ),
                context_num_max_tokens=random.randint(
                    *TestDialogueNumTokensValidator.CONTEXT_NUM_MAX_TOKENS_RANGE
                ),
                response_num_max_tokens=random.randint(
                    *TestDialogueNumTokensValidator.RESPONSE_LENGTH_RANGE
                ),
                total_num_max_tokens=random.randint(
                    *TestDialogueNumTokensValidator.TOTAL_NUM_MAX_TOKENS_RANGE
                )
            )

            if random.random() < TestDialogueNumTokensValidator.CONTEXT_NUM_MAX_TOKENS_NONE_PROB:
                delattr(validator_arguments, 'context_num_max_tokens')
            if random.random() < TestDialogueNumTokensValidator.RESPONSE_NUM_MAX_TOKENS_NONE_PROB:
                delattr(validator_arguments, 'response_num_max_tokens')
            if random.random() < TestDialogueNumTokensValidator.TOTAL_NUM_MAX_TOKENS_NONE_PROB:
                delattr(validator_arguments, 'total_num_max_tokens')

            validator = DialogueNumTokensValidator(
                **asdict(validator_arguments)
            )
            dialogue_sample = DialogueSample.make_random_encoded_dialogue_sample(
                num_contexts_range=TestDialogueNumTokensValidator.NUM_CONTEXTS_RANGE,
                context_length_range=TestDialogueNumTokensValidator.CONTEXT_LENGTH_RANGE,
                context_speaker_range=TestDialogueNumTokensValidator.CONTEXT_SPEAKER_RANGE,
                response_length_range=TestDialogueNumTokensValidator.RESPONSE_LENGTH_RANGE,
                response_speaker_range=TestDialogueNumTokensValidator.RESPONSE_SPEAKER_RANGE
            )
            dialogue_sample_before_validation = copy.deepcopy(dialogue_sample)

            num_context_utterance_ids = sum(len(utterance) for utterance in dialogue_sample.context)
            num_utterance_related_system_tokens = validator_arguments.num_system_tokens_per_context_utterance * len(dialogue_sample.context) 
            num_utterance_unrelated_system_tokens = validator_arguments.num_system_tokens_in_context_unrelated_to_utterance
            num_total_context_ids = num_context_utterance_ids + num_utterance_related_system_tokens + num_utterance_unrelated_system_tokens

            if len(dialogue_sample.response) == 0:
                num_total_response_ids = len(dialogue_sample.response) + validator_arguments.num_system_tokens_in_empty_response
            else:
                num_total_response_ids = len(dialogue_sample.response) + validator_arguments.num_system_tokens_in_non_empty_response

            num_total_tokens = num_total_context_ids + num_total_response_ids
            msg = f'{dialogue_sample}, {validator_arguments}'
            if validator.is_valid(dialogue_sample):
                if getattr(validator_arguments, 'context_num_max_tokens', None) is not None:
                    self.assertTrue(num_total_context_ids <= validator_arguments.context_num_max_tokens, msg)
                if getattr(validator_arguments, 'response_num_max_tokens', None) is not None:
                    self.assertTrue(num_total_response_ids <= validator_arguments.response_num_max_tokens, msg)
                if getattr(validator_arguments, 'total_num_max_tokens', None) is not None:
                    self.assertTrue(num_total_tokens <= validator_arguments.total_num_max_tokens, msg)
            else:
                if getattr(validator_arguments, 'context_num_max_tokens', None) is not None:
                    context_length_is_valid =  num_total_context_ids <= validator_arguments.context_num_max_tokens
                else:
                    context_length_is_valid =  True

                if getattr(validator_arguments, 'response_num_max_tokens', None) is not None:
                    response_length_is_valid = num_total_response_ids <= validator_arguments.response_num_max_tokens
                else:
                    response_length_is_valid =  True

                if getattr(validator_arguments, 'total_num_max_tokens', None) is not None:
                    total_length_is_valid = num_total_tokens <= validator_arguments.total_num_max_tokens
                else:
                    total_length_is_valid =  True

                self.assertFalse(
                    context_length_is_valid and response_length_is_valid and total_length_is_valid
                )

            self.assertEqual(dialogue_sample, dialogue_sample_before_validation)
    