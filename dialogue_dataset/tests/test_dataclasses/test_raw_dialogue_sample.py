import contextlib
from unittest import TestCase
from dialogue_dataset.dataclasses import RawDialogue
from dialogue_dataset.dataclasses import DialogueSample
import random, copy
from dataclasses import dataclass, asdict
from typing import Optional

@dataclass
class DialogueNumTokensValidatorArguments:
    num_system_tokens_in_context_unrelated_to_utterance: int
    num_system_tokens_per_context_utterance: int
    num_system_tokens_in_non_empty_response: int
    num_system_tokens_in_empty_response: int
    context_num_max_tokens: Optional[int] = None
    response_num_max_tokens: Optional[int] = None
    total_num_max_tokens: Optional[int] = None


class TestRawDialogue(TestCase):
    SEED = 0

    def test_iterate_with_window_iterates_responses_completely(self):
        utterance_length = 20
        window_size = 4
        start = 0

        raw_dialogue = RawDialogue(
            utterances=[str(i) for i in range(utterance_length)],
            speakers=[str(i) for i in range(utterance_length)],
        )
        for i, dialogue_sample in enumerate(raw_dialogue.iterate_with_window(window_size=window_size), start=start):
            self.assertEqual(dialogue_sample.response, str(i))
            self.assertEqual(dialogue_sample.response_speaker, str(i))
        self.assertEqual(i, utterance_length - start - 1)

    def test_iterate_with_window_context_lengths(self):
        utterance_length = 20
        window_size = 4
        start = 0

        raw_dialogue = RawDialogue(
            utterances=[str(i) for i in range(utterance_length)],
            speakers=[str(i) for i in range(utterance_length)],
        )
        correct_context_lengths = [ min(i, window_size - 1) for i in range(utterance_length - start)]
        num_iterations = 0
        for dialogue_sample, correct_context_len in zip(
            raw_dialogue.iterate_with_window(window_size=window_size, start=start), correct_context_lengths):
            self.assertEqual(len(dialogue_sample.context), correct_context_len)
            self.assertEqual(len(dialogue_sample.context_speakers), correct_context_len)
            num_iterations += 1
        self.assertEqual(num_iterations, utterance_length - start)
    
    def test_iterate_with_window_contexts_content(self):
        utterance_length = 20
        window_size = 4
        start = 0

        raw_dialogue = RawDialogue(
            utterances=[str(i) for i in range(utterance_length)],
            speakers=[str(i) for i in range(utterance_length)],
        )
        correct_contexts = [ [str(j) for j in range(max(i - window_size + 1, start), i)] for i in range(start, utterance_length)]
        num_iterations = 0
        for dialogue_sample, correct_context in zip(
            raw_dialogue.iterate_with_window(window_size=window_size, start=start), correct_contexts):
            self.assertEqual(dialogue_sample.context, correct_context)
            self.assertEqual(dialogue_sample.context_speakers, correct_context)
            num_iterations += 1
        self.assertEqual(num_iterations, utterance_length - start)
    
    def test_utterance_speaker_init_length_assertion(self):
        utterances = [str(i) for i in range(10)]
        speakers = [i for i in range(9)]

        with self.assertRaises(AssertionError):
            RawDialogue(
                utterances=utterances,
                speakers=speakers,
            )