from abc import ABC, abstractmethod
from .dialogue_tokenizer import DialogueTokenizer
from .dataclasses import DialogueSample
import copy
from typing import Sequence

# TODO: Removing space in an utterance and what not...

class DialogueSampleTransform(ABC):
    @abstractmethod
    def __call__(self, dialogue_sample: DialogueSample) -> DialogueSample:
        pass


class CompressSpeakerIdsById(DialogueSampleTransform):
    def __call__(self, dialogue_sample: DialogueSample) -> DialogueSample:
        speaker_ids = copy.copy(dialogue_sample.context_speakers)
        speaker_ids.append(dialogue_sample.response_speaker)

        speaker_ids = list(set(speaker_ids))
        speaker_ids.sort()
        speaker_ids_map = dict(zip(speaker_ids, range(len(speaker_ids))))

        dialogue_sample.context_speakers = [speaker_ids_map[speaker_id] for speaker_id in dialogue_sample.context_speakers]
        dialogue_sample.response_speaker = speaker_ids_map[dialogue_sample.response_speaker]

        return dialogue_sample


class CompressSpeakerIdsByTheOrderInWhichTheyAppeared(DialogueSampleTransform):
    def __call__(self, dialogue_sample: DialogueSample) -> DialogueSample:
        speaker_ids_map = {}
        cur_speaker_id = 0
        context_speaker_ids = []

        for speaker_id in dialogue_sample.context_speakers:
            if speaker_id not in speaker_ids_map:
                speaker_ids_map[speaker_id] = cur_speaker_id
                cur_speaker_id += 1
            context_speaker_ids.append(speaker_ids_map[speaker_id])
        dialogue_sample.context_speakers = context_speaker_ids

        if dialogue_sample.response_speaker in speaker_ids_map:
            dialogue_sample.response_speaker = speaker_ids_map[dialogue_sample.response_speaker]  
        else: 
            dialogue_sample.response_speaker = cur_speaker_id

        return dialogue_sample


class DialogueSampleTransformComposition(DialogueSampleTransform):
    def __init__(self, dialogue_sample_transforms: Sequence[DialogueSampleTransform] = []):
        self.dialogue_sample_transforms = dialogue_sample_transforms

    def __call__(self, dialogue_sample: DialogueSample) -> DialogueSample:
        for dialogue_sample_transform in self.dialogue_sample_transforms:
            dialogue_sample = dialogue_sample_transform(dialogue_sample) 
        return dialogue_sample
