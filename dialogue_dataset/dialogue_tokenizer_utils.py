import torch, copy,  math
from dialogue_dataset import (
    DialogueSample, 
)
import numpy as np


def mask_dialogue_token_ids(dialogue: DialogueSample, masking_rate: float, mask_token_id: int, **kwargs):
    dialogue_copy = copy.deepcopy()
    utterances = dialogue_copy.context + [dialogue_copy.response]
    utterance_lengths = [len(utterance) for utterance in utterances]

    input_ids = np.concatenate(utterances)
    mask_indices = np.random.choice(
        np.arange(len(input_ids)),
        size=math.floor(len(input_ids) * masking_rate),
        replace=False,
    )
    input_ids[mask_indices] = mask_token_id
    masked_input_ids = np.split(input_ids, np.cumsum(utterance_lengths))[:-1]

    dialogue_copy.context = masked_input_ids[:-1]
    dialogue_copy.response = masked_input_ids[-1]

    return dialogue_copy

def make_mlm_labels(masked_input_ids, original_input_ids, mask_token_id, **kwargs):
    mlm_labels = torch.where(masked_input_ids == mask_token_id, original_input_ids, -100)
    return mlm_labels

