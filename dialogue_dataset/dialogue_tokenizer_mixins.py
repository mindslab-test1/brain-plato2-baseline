import torch, itertools, copy, math
from torch.nn.utils.rnn import pad_sequence
from dialogue_dataset import DialogueSample
from typing import (
    List,
    Dict,
)
import numpy as np

# TODO: Is there a reason that these classes should exist as mixins? Most of them do not contain attributes.
# TODO: Should these be located here? Can't a separate module be made?

class BaseTokenizerCompositionMixin:
    def encode(self, text):
        return self.tokenizer.encode(text)
    
    def decode(self, ids):
        return self.tokenizer.decode(ids)


class TokenEmbeddingIndexProcessingMixin:
    def make_context_input_ids_list(
        self, 
        context_input_ids_list: List[int],
        context_bos_token_id: int = None, 
        context_eos_token_id: int = None
    ) -> List[torch.LongTensor]:
        encoded_context_input_ids_list = []
        for utterance in context_input_ids_list:
            bos_token_id_list = [context_bos_token_id] if context_bos_token_id is not None else []
            eos_token_id_list = [context_eos_token_id] if context_eos_token_id is not None else []
            processed_utterance = bos_token_id_list + utterance + eos_token_id_list
            processed_utterance = torch.tensor(processed_utterance)
            encoded_context_input_ids_list.append(processed_utterance) 
        if len(encoded_context_input_ids_list) == 0:
            encoded_context_input_ids_list = [torch.LongTensor(size=(0,))]
        return encoded_context_input_ids_list

    def make_response_input_ids(
        self, 
        response_input_ids: torch.LongTensor, 
        response_bos_token_id: int = None, 
        response_eos_token_id: int = None
    ) -> torch.LongTensor:
        bos_token_id_list = [response_bos_token_id] if response_bos_token_id is not None else []
        eos_token_id_list = [response_eos_token_id] if response_eos_token_id is not None else []
        if len(response_input_ids) > 0:
            response_input_ids = bos_token_id_list + response_input_ids + eos_token_id_list
        else:
            response_input_ids = bos_token_id_list + response_input_ids
        response_input_ids = torch.tensor(response_input_ids)
        return response_input_ids


class UnifiedAttentionTokenIndexProcessingMixin:
    """Unified attention token ids processing mixin.
    """
    def make_generation_labels(self, input_ids, bos_token_index, **kwargs):
        labels = torch.full_like(input=input_ids, fill_value=-100)
        # Assume there is an eos token at the end of the input ids.
        gold_response = input_ids[bos_token_index+1:]
        labels[bos_token_index : bos_token_index + len(gold_response)] = gold_response
        return labels
    
    def make_seq2seq_mask(
        self, 
        sequence_length: int, 
        bos_token_index: int, 
        **kwargs
    ):
        attention_mask = torch.full(size=(sequence_length, sequence_length), fill_value=1, dtype=torch.long) 
        attention_mask[:bos_token_index, bos_token_index:] = 0
        attention_mask[bos_token_index:, bos_token_index:] = torch.tril(attention_mask[bos_token_index:, bos_token_index:])
        return attention_mask
    
    def make_bidirectional_mask(self, sequence_length, **kwargs):
        attention_mask = torch.full(size=(sequence_length, sequence_length), fill_value=1, dtype=torch.long) 
        return attention_mask
    
    def make_unidirectional_mask(self, sequence_length, **kwargs):
        attention_mask = torch.ones([sequence_length] * 2, dtype=torch.long)
        attention_mask = torch.tril(attention_mask)
        return attention_mask


class UtteranceLevelIndexProcessingMixin:
    def make_position_ids(
        self, input_ids_list: List[List[int]], 
        position_global_first_token_id: int = None
    ):
        position_ids = [torch.tensor([position_global_first_token_id])] if position_global_first_token_id is not None else []
        for utterance in input_ids_list:
            utterance_position_ids = torch.arange(len(utterance))
            position_ids.append(utterance_position_ids)

        # if len(position_ids) == 0:
        #     position_ids = 
        position_ids = torch.cat(position_ids, dim=0)
        return position_ids

    def make_role_ids(
        self, 
        input_ids_list: List[List[int]], 
        role_id_list: List[int], 
        role_global_first_token_id: int = None
    ):
        role_ids = [torch.tensor([role_global_first_token_id])] if role_global_first_token_id is not None else []
        if len(role_ids) == 0:
            role_ids = [torch.LongTensor(size=(0,))]

        for utterance, speaker in zip(input_ids_list, role_id_list):
            utterance_role_ids = torch.full(fill_value=speaker, size=utterance.size(), dtype=torch.long) 
            role_ids.append(utterance_role_ids)
        role_ids = torch.cat(role_ids, dim=0)
        return role_ids
    
    def make_turn_ids(
        self, 
        input_ids_list,
        turn_global_first_token_id: int = None,
        reverse: bool = True,
        lowest_id: int = 0,
    ):
        turn_ids = [torch.tensor([turn_global_first_token_id])] if turn_global_first_token_id is not None else []
        if len(turn_ids) == 0:
            turn_ids = [torch.LongTensor(size=(0,))]

        for turn, utterance in enumerate(input_ids_list, start=-len(input_ids_list) - lowest_id + 1 if reverse else lowest_id):
            utterance_turn_ids = torch.full(fill_value=abs(turn), size=utterance.size(), dtype=torch.long) 
            turn_ids.append(utterance_turn_ids)
        turn_ids = torch.cat(turn_ids, dim=0)
        return turn_ids


class MaskedLanguageModelTokenProcessingMixin:
    def mask_dialogue_token_ids(self, dialogue: DialogueSample, **kwargs):
        dialogue_copy = copy.deepcopy(dialogue)
        utterances = dialogue_copy.context + [dialogue_copy.response]
        utterance_lengths = [len(utterance) for utterance in utterances]

        input_ids = np.concatenate(utterances)
        mask_indices = np.random.choice(
            np.arange(len(input_ids)),
            size=math.floor(len(input_ids) * self.masking_rate),
            replace=False,
        )
        input_ids[mask_indices] = self.mask_token_id
        masked_input_ids = np.split(input_ids, np.cumsum(utterance_lengths))[:-1]
        masked_input_ids = [list(input_ids) for input_ids in masked_input_ids]

        dialogue_copy.context = masked_input_ids[:-1]
        dialogue_copy.response = masked_input_ids[-1]
        
        # print('origin:', dialogue)
        # print('masked:', dialogue_copy)
        # print()

        return dialogue_copy

    def make_mlm_labels(self, masked_input_ids, original_input_ids, **kwargs):
        mlm_labels = torch.where(masked_input_ids == self.mask_token_id, original_input_ids, -100)
        return mlm_labels


class DictionaryCollateFnMixin:
    # TODO: I am not really sure if this should be a mixin.
    def pad_sequences(self, batch: List[Dict]):
        model_input_dict = {}

        for key in getattr(self, 'zero_dim_keys', ()):
            samples = [sample[key] for sample in batch]
            concatenated_samples = torch.stack(samples)
            model_input_dict[key] = concatenated_samples

        for key, pad_id in getattr(self, 'one_dim_key_pad_pairs', ()):
            samples = [sample[key] for sample in batch]
            padded_samples = pad_sequence(samples, batch_first=True, padding_value=pad_id)
            model_input_dict[key] = padded_samples

        for key, pad_id in getattr(self, 'two_dim_key_pad_pairs', ()):
            samples = [sample[key] for sample in batch]
            first_dim_max_len = max([sample.size(0) for sample in samples])
            second_dim_max_len = max([sample.size(0) for sample in samples])
            padded_sample = torch.full(size=[len(samples), first_dim_max_len, second_dim_max_len], fill_value=pad_id)
            for batch_idx, sample in enumerate(samples):
                padded_sample[batch_idx, :sample.size(0), :sample.size(1)] = sample
            model_input_dict[key] = padded_sample

        return model_input_dict

    def concatenate_inputs(self, model_input_dict: Dict[str, torch.Tensor]):
        if not hasattr(self, 'final_key_concatenation_keys_pairs'):
            return model_input_dict

        for final_key, concatenation_keys in self.final_key_concatenation_keys_pairs:
            concatenation_list = [model_input_dict[key] for key in concatenation_keys]
            model_input_dict[final_key] = torch.cat(concatenation_list, dim=0)

        combinded_concatenation_keys = itertools.chain.from_iterable(
            [concatenation_keys for _, concatenation_keys in self.final_key_concatenation_keys_pairs]
        )
        combinded_concatenation_keys = set(combinded_concatenation_keys)
        final_keys = set([final_key for final_key, _ in self.final_key_concatenation_keys_pairs])

        for concatenation_key in combinded_concatenation_keys:
            if concatenation_key not in final_keys:
                del model_input_dict[concatenation_key]

        return model_input_dict