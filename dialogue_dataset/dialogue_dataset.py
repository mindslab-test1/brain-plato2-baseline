import random, copy
from torch.utils.data import IterableDataset
from typing import (
    Generator,
    Optional,
    Sequence,
    Any,
    Iterator,
)
from .dataclasses import (
    DialogueSample,
    RawDialogue,
)
from .validators import DialogueValidator
from .raw_dialogue_transforms import RawDialogueTransform
from .dialogue_sample_transforms import DialogueSampleTransform
from .dialogue_tokenizer import DialogueTokenizer
from bucketed_datasets.iterable import GeneratorIsEmpty


class IterableDialogueDataset(IterableDataset):
    """An iterable dataset with sliding window functionality.
    """
    def __init__(
        self, 
        dialogues: Sequence[RawDialogue], 
        tokenizer: DialogueTokenizer,
        window_size: int,
        dialogue_validator: Optional[DialogueValidator] = None,
        shuffle: bool = True,
        raw_dialogue_transform: Optional[RawDialogueTransform] = None, 
        string_dialogue_sample_transform: Optional[DialogueSampleTransform] = None, 
        start_index: Optional[int] = 0,
    ):
        """Initializes the dataset with a list of dialogues and other configureatiions.

        Args:
            dialogues (Sequence[RawDialogue]): A list of dialogues.
            tokenizer (DialogueTokenizer): A dialogue tokenizer to dialogue samples.
            window_size (int): The size of the sliding window.
            dialogue_validator (Optional[DialogueValidator], optional): A DialogueValidator instance that filters out invalid dialogue samples. 
                Defaults to None.
            shuffle (bool, optional): If True, shuffles the raw dialogues before iterating over the them. Defaults to True.
            raw_dialogue_transform (Optional[RawDialogueTransform], optional): A transformation that dictates how a raw dialogue should be
                transformed before making samples of dialogues by slicing it with a sliding window. Defaults to None.
            string_dialogue_sample_transform (Optional[DialogueSampleTransform], optional): A transformation that dictates how an unencoded 
                dialouge sample should be transformed before encoding it with a dialogue tokenizer. Defaults to None.
            start_index (Optional[int], optional): The starting index of the response. If set to 1, for example, the first utterance in a
                raw dialogue will not be chosen as the response when making samples of dialogues. Defaults to 0.
        """
        super().__init__()
        self.shuffle = shuffle
        self.window_size = window_size
        self.dialogues = dialogues
        self.tokenizer = tokenizer
        self.dialogue_validator = dialogue_validator
        self.num_processed_samples = 0
        self.raw_dialogue_transform = raw_dialogue_transform
        self.string_dialogue_sample_transform = string_dialogue_sample_transform
        self.start_index = start_index

    def __iter__(self) -> Iterator[Any]:
        """Yield samples using sliding-window.

        A deep copy of 

        Raises:
            GeneratorIsEmpty: If there are no samples yield from the dataset.

        Returns:
            Iterator[Any]: An iterator that yields an encoded sample at a time, encoded by the dialogue tokenizer.
        """
        self.num_processed_samples = 0
        if self.shuffle:
            random.shuffle(self.dialogues) 
        
        for dialogue in self.dialogues:
            # copying the dialogue just in case.
            for dialogue_window in self.iterate_dialogue_windows(copy.deepcopy(dialogue)):
                yield self.tokenizer.encode_dialogue(dialogue_window)

        raise GeneratorIsEmpty

    def iterate_dialogue_windows(self, dialogue: RawDialogue) -> Generator[DialogueSample, None, None]:
        """

        Args:
            dialogue (RawDialogue): TODO

        Yields:
            Generator[DialogueSample, None, None]: TODO
        """
        if self.raw_dialogue_transform is not None:
            dialogue = self.raw_dialogue_transform(dialogue)

        # Sliding window. Returns a deep copy of the sample.
        for dialogue_sample in dialogue.iterate_with_window(window_size=self.window_size, start=self.start_index, end=None):
            self.num_processed_samples += 1

            if not self.dialogue_validator.is_valid_string_dialogue(dialogue_sample):
                continue

            if self.string_dialogue_sample_transform is not None:
                dialogue_sample = self.string_dialogue_sample_transform(dialogue_sample)
            
            dialogue_sample.encode_with(self.tokenizer)

            if not self.dialogue_validator.is_valid_encoded_dialogue(dialogue_sample):
                continue

            yield dialogue_sample

        return

    def __len__(self) -> int:
        """Returns the total number of utterances in the list of dialogues.

        The actual calculation only gets performed once, because it returns a cached value on later calls.

        Returns:
            int: The total number of utterances in the list of dialogues.
        """
        if getattr(self, 'total_num_utterances', None) == None:
            self.total_num_utterances = sum((len(dialogue) for dialogue in self.dialogues))
        return self.total_num_utterances

    @property 
    def num_samples_left(self) -> int:
        """Returns an APPROXIIMATE number of samples left in the dataset.

        The actual number of samples left in the dataset may DIFFER from the returned value.
        The reason being that TODO

        Returns:
            int: TODO
        """
        return len(self) - self.num_processed_samples