from .dataclasses import DialogueSample
from abc import ABC, abstractmethod
from typing import List, Any

class DialogueTokenizer(ABC):
    # TODO: Inherit or not to inherit, that is the question.

    @abstractmethod
    def encode(self, text: str) -> List[int]:
        pass

    @abstractmethod
    def encode_dialogue(self, dialogue: DialogueSample) -> Any:
        pass

    @abstractmethod
    def collate_fn(self, batch) -> Any:
        pass

    @abstractmethod
    def decode(self, ids: List[int]) -> str:
        pass