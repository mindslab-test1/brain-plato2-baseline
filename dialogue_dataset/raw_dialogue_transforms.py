from abc import ABC, abstractmethod
from .dataclasses import RawDialogue
import re
from typing import Sequence


class RawDialogueTransform(ABC):
    @abstractmethod
    def __call__(self, raw_dialogue: RawDialogue):
        pass 


# TODO: Remove. This logic is taken care of by the FileLoader.
class ReplaceSystemTokens(RawDialogueTransform):
    def __init__(self, replace_with: str):
        self.replace_with = replace_with

    def __call__(self, raw_dialogue: RawDialogue):
        pattern = getattr(raw_dialogue, 'system_token_pattern', None)

        if pattern is not None:
            raw_dialogue.utterances = [
                re.sub(pattern, self.replace_with, utterance) 
                for utterance in raw_dialogue.utterances
            ]
        return raw_dialogue


class WhitespaceStripper(RawDialogueTransform):
    def __call__(self, raw_dialogue: RawDialogue):
        raw_dialogue.utterances = [utterance.strip() for utterance in raw_dialogue.utterances]
        return raw_dialogue


class RawDialogueTransformComposition(RawDialogueTransform):
    def __init__(self, raw_dialogue_transforms: Sequence[RawDialogueTransform]):
        self.raw_dialouge_transforms = raw_dialogue_transforms
    
    def __call__(self, raw_dialogue: RawDialogue):
        for raw_dialogue_transform in self.raw_dialouge_transforms:
            raw_dialogue = raw_dialogue_transform(raw_dialogue)
        return raw_dialogue