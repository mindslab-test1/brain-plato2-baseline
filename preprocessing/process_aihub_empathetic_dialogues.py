import json, glob, re
from dialogue_preprocessing.file_processors import FileProcessor
from dialogue_preprocessing.dialogue_preprocessing_cli import DialoguePreprocessingCLI
from pathlib import Path

class EmpatheticDialoguesFileProcessor(FileProcessor):
    def __init__(self, data_dir: str, raw_file_suffix: str):
        self.data_dir = data_dir
        self.raw_file_suffix = raw_file_suffix

    @FileProcessor.wrap_extract_dialogues_with_tqdm
    def extract_dialogues(self, file_path):
        with open(file_path, 'r') as f:
            file_dict = json.load(f)
        return file_dict

    def extract_utterance_speaker_id_pairs(self, dialogue):
        speaker_id_utterance_key_values = dialogue['talk']['content']
        utterance_speaker_id_pairs = [
            (re.search(r'[^\d]*', utterance).group(), re.search(r'[^\d]*', speaker_id).group()) 
            for speaker_id, utterance in speaker_id_utterance_key_values.items()
        ]
        return utterance_speaker_id_pairs
    
    def load_files(self):
        files = glob.glob(self.data_dir + '/**/*' + self.raw_file_suffix, recursive=True)
        files = [file_path for file_path in files if re.fullmatch(r'^감성대화말뭉치\(최종데이터\).*\.json$', Path(file_path).name)]
        return files


if __name__ == '__main__':
    DialoguePreprocessingCLI()