import json, glob
from dialogue_preprocessing.file_processors import FileProcessor
from dialogue_preprocessing.dialogue_preprocessing_cli import DialoguePreprocessingCLI

class AIHubSNSFileProcessor(FileProcessor):
    def __init__(self, data_dir: str, raw_file_suffix: str):
        self.data_dir = data_dir
        self.raw_file_suffix = raw_file_suffix

    def load_files(self):
        files = glob.glob(self.data_dir + '/**/*' + self.raw_file_suffix, recursive=True)
        return files

    @FileProcessor.wrap_extract_dialogues_with_tqdm
    def extract_dialogues(self, file_path):
        with open(file_path, 'r') as f:
            file_dict = json.load(f, strict=False)
        dialogues = file_dict['data']
        return dialogues
    
    def extract_utterance_speaker_id_pairs(self, dialogue):
        utterance_objects = dialogue['body']
        utterance_speaker_id_pairs = [
            (utterance_obj['utterance'], utterance_obj['participantID'].strip())
            for utterance_obj in utterance_objects
            if 
                len(utterance_obj['utterance'].strip()) > 0
                and len(utterance_obj['participantID'].strip()) > 0
        ]
        return utterance_speaker_id_pairs


if __name__ == '__main__':
    DialoguePreprocessingCLI()