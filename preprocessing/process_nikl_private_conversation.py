import json, glob, re
from dialogue_preprocessing.file_processors import FileProcessor
from dialogue_preprocessing.dialogue_preprocessing_cli import DialoguePreprocessingCLI
from pathlib import Path
from typing import List, Tuple, Dict


class NIKLPrivateConversationFileProcessor(FileProcessor):
    def __init__(self, data_dir: str, raw_file_suffix: str):
        self.data_dir = data_dir
        self.raw_file_suffix = raw_file_suffix

    def load_files(self) -> List[str]:
        files = glob.glob(self.data_dir + '/**/*' + self.raw_file_suffix, recursive=True)
        files = [
            file_path for file_path in files 
            if re.fullmatch(r'SDRW.*\.json', Path(file_path).name) is not None
        ]
        return files

    def extract_dialogues(self, file_path: str) -> List[Dict]:
        with open(file_path, 'r') as f:
            file_dict = json.load(f, strict=False)
        dialogues = file_dict['document']
        return dialogues
    
    def extract_utterance_speaker_id_pairs(self, dialogue: Dict) -> List[Tuple[str]]:
        utterance_objects = dialogue['utterance']
        utterance_speaker_id_pairs = [
            (utterance_obj['form'], utterance_obj['speaker_id'].strip()) 
            for utterance_obj in utterance_objects
            if 
                len(utterance_obj['form'].strip()) > 0 
                and len(utterance_obj['speaker_id'].strip()) > 0 
        ]
        return utterance_speaker_id_pairs


if __name__ == '__main__':
    DialoguePreprocessingCLI()