import json, pprint, glob, re, random, unicodedata
from pathlib import Path
from argparse import ArgumentParser
from tqdm import tqdm

def extract_dialog(json_dict):
    data = json_dict['data']
    # Just in case the gaps between frames are not 1.
    frames = [data[frame] for frame in sorted(data.keys(), key=lambda key: int(key))]

    actor = json_dict['actor']
    script_starts = {}
    for actor_id in actor.keys():
        script_starts[actor_id.strip()] = None

    person_ids = {}
    cur_person_id = 'A'
    
    dialog = []
    cur_turn = 0
    prev_dialogue = None
    for frame in frames:
        # Make the order deterministic, just in case any of the speakers start speaking at the sanme frame.
        # Keys are not always an integer representation of a string, so convertion to int is not performed.
        objects = [frame[obj_id] for obj_id in sorted(frame.keys())]
        for text, person_id in [ (obj['text'], obj['person_id'].strip()) for obj in objects if 'text' in obj ]:
            # If the uterance is not already extracted (since a speaker cannot start speaking two difference things at the same frame).
            if script_starts[person_id] != text['script_start']:
                assert cur_turn < 1000, 'id excceded 999'

                script_starts[person_id] = text['script_start']
                # If speaker id is not already assigned.
                if person_id not in person_ids:
                    assert ord('A') <= ord(cur_person_id) <= ord('Z'), 'speaker code exceeded Z'
                    person_ids[person_id] = cur_person_id
                    cur_person_id = chr(ord(cur_person_id) + 1)
                
                if prev_dialogue is None:
                    prev_dialogue = {
                        'id': str(cur_turn).zfill(3),
                        'speaker': person_ids[person_id],
                        'utterance': [' '.join(unicodedata.normalize('NFKC', text['script']).split())],
                    }
                    continue

                if prev_dialogue['speaker'] != person_ids[person_id]:
                    # Remoce \t and \xa0 in the utterance string and replace three consecutive dots symbol with "...".
                    prev_dialogue['utterance'] = ' '.join(prev_dialogue['utterance'])
                    dialog.append(prev_dialogue)
                    cur_turn += 1
                    prev_dialogue = {
                        'id': str(cur_turn).zfill(3),
                        'speaker': person_ids[person_id],
                        'utterance': [' '.join(unicodedata.normalize('NFKC', text['script']).split())],
                    }
                else:
                    prev_dialogue['utterance'].append(' '.join(unicodedata.normalize('NFKC', text['script']).split()))

    if prev_dialogue is not None:
        prev_dialogue['utterance'] = ' '.join(prev_dialogue['utterance'])
        dialog.append(prev_dialogue)

    return dialog

def process_json_file(json_file_path, object_id):
    with open(json_file_path, 'r') as f:
        file_dict = json.load(f)

    dialog = extract_dialog(file_dict)
    output_dict = {
        'id': object_id,
        'title': Path(json_file_path).name,
        'dialog': dialog,
    } 
    return output_dict

def parse_args():
    parser = ArgumentParser()
    parser.add_argument('--code')
    parser.add_argument('--data_dir')
    parser.add_argument('--destination_path')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    dialogues = []
    files = glob.glob(args.data_dir+'/**/*.json', recursive=True)
    files = sorted(files, key= lambda s: int(re.search('clip_(\d+).json', s).group(1))) 
    file_idx = 0
    for file_path in tqdm(files):
        assert file_idx <= 99999999, 'the number of dialogues exceeded 99999999'
        try:
            object_id = args.code + str(file_idx).zfill(8)
            dialogue = process_json_file(file_path, object_id=object_id)
            if len(dialogue['dialog']) < 2:
                print('len(dialog) is less than 2. This conversation will not be added to the final output.')
                print(file_path)
                print(dialogue['dialog'])
                continue
            dialogues.append(dialogue)
            file_idx += 1
        except UnicodeDecodeError as e:
            print(e)
            print(f'Failed to decode {file_path}')
            pass
        except Exception as e:
            print(e)
            print(file_path)
            raise e
    
    print('Print out some samples')
    samples = random.sample(dialogues, 3)
    for sample in samples:
        pprint.pprint(sample)
    
    dest_path = Path(args.destination_path)
    # What is wrong with with_stem()?
    dest_path = dest_path.with_name(f'{dest_path.stem}_({len(dialogues)}){dest_path.suffix}')
    with open(dest_path, 'w') as f:
        json.dump(dialogues, f, indent=4, sort_keys=True, ensure_ascii=False)
    print(f'Saved to {dest_path}')