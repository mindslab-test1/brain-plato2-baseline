import json
from abc import ABC, abstractmethod
from typing import Any, Iterable, Dict, List
from pathlib import Path
import functools
from tqdm import tqdm

class FileProcessor(ABC): 
    @abstractmethod
    def extract_dialogues(self, file_path: str) -> Iterable[Any]:
        pass

    @abstractmethod
    def extract_utterance_speaker_id_pairs(self, dialogue: Any) -> Iterable[Dict[str, str]]:
        pass

    @abstractmethod
    def load_files(self) -> Iterable[Any]:
        pass

    @staticmethod
    def wrap_extract_dialogues_with_tqdm(extract_dialogues):
        @functools.wraps(extract_dialogues)
        def wrapper(self, file_path: str, *args, **kwargs):
            return tqdm(extract_dialogues(self, file_path, *args, **kwargs), desc=file_path)
        return wrapper