from typing import Any, Iterable, Dict, List, Callable
from .utterance_transforms import UtteranceTransform
from .partial_utterances_joiners import PartialUtterancesJoiner

class UtteranceProcessor:
    def __init__(
        self, 
        # join_partial_utterances_callback: Callable[[List[str]], str] = lambda x: x,
        join_partial_utterances_callback: PartialUtterancesJoiner = lambda x: x,
        partial_utterance_processors: Iterable[UtteranceTransform] = [],
        complete_utterance_processors: Iterable[UtteranceTransform] = [],
    ):
        self.partial_utterance_processors = partial_utterance_processors
        self.complete_utterance_processors = complete_utterance_processors
        self.join_partial_utterances_callback = join_partial_utterances_callback

    def process_partial_utterance(self, partial_utterance: str) -> Iterable[Any]:
        for partial_utterance_processor in self.partial_utterance_processors:
            partial_utterance = partial_utterance_processor(partial_utterance)
        return partial_utterance
    
    def process_complete_utterance(self, partial_utterance: str) -> Iterable[Any]:
        for complete_utterance_processor in self.complete_utterance_processors:
            partial_utterance = complete_utterance_processor(partial_utterance)
        return partial_utterance

    def join_partial_utterances(self, partial_utterances: List[str]) -> str:
        return self.join_partial_utterances_callback(partial_utterances)
