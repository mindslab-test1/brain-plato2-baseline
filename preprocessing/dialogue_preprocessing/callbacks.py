import json, glob, re, random, unicodedata, os
from typing import Callable, Sequence
from json.decoder import JSONDecodeError
from pathlib import Path
from argparse import ArgumentParser, Namespace
from tqdm import tqdm
import itertools
from abc import ABC, abstractmethod
from typing import Any, Iterable, Dict, List, Iterable
from pprint import pprint
from .dialogue_statistics_calculator import DialogueStatisticsCalculator
from .file_processors import FileProcessor
from .dialogue_file_writers import DialogueFileWriter
from .utterance_transforms import UtteranceTransform
from .utterance_processor import UtteranceProcessor

# TODO: Solve the type hint thingy.

class DialoguePreprocessorCallback(ABC):
    def on_load_files_end(self, preprocessor: 'DialoguePreprocessor', files: Iterable) -> None:
        pass

    def on_save_dialogue_start(self, preprocessor: 'DialoguePreprocessor', dialogues) -> None:
        pass


class RandomSamplesPrinterCallback(DialoguePreprocessorCallback):
    def __init__(self, num_random_samples_to_print: int = 10):
        self.num_random_samples_to_print = num_random_samples_to_print

    def on_save_dialogue_start(self, preprocessor: 'DialoguePreprocessor', dialogues) -> None:
        if self.num_random_samples_to_print > 0:
            print('Here are some random samples, do they seem alright?')
            samples = random.sample(dialogues, min(len(dialogues), self.num_random_samples_to_print))
            for sample in samples:
                pprint(sample)


class LoadedFilesPrinterCallback(DialoguePreprocessorCallback):
    def on_load_files_end(self, preprocessor: 'DialoguePreprocessor', files: Iterable) -> None:
        print("Below are the list of files that will be processed.")
        # TODO: Well, it might not be a list, but who cares.
        if isinstance(files, list):
            pprint(files)
        else:
            pprint(list(files))
        print()


class StatisticsPrinterCallback(DialoguePreprocessorCallback):
    def on_save_dialogue_start(self, preprocessor: 'DialoguePreprocessor', dialogues) -> None:
        statistics_calculator = DialogueStatisticsCalculator(dialogues)
        print(f'Total number of dialogues: {statistics_calculator.total_num_dialogues:,}')
        print(f'Total number of utterances: {statistics_calculator.total_num_utterances:,}')
        print(f'Average number of utterances per dialogue: {statistics_calculator.avg_utterances_per_dialogue:.2f}')
        print(f'Median of the number of utterances in a dialogue: {statistics_calculator.median_utterances_per_dialogue:.2f}')
        print(f'Maximum number of speakers: {statistics_calculator.max_num_speakers:,}')
        print(f'Average number of speakers: {statistics_calculator.avg_num_speakers:.2f}')
        print(f'Maximum number of characters in an utterance: {statistics_calculator.max_num_characters_per_utterance:,}')
        print(f'Average number of characters per utterance: {statistics_calculator.avg_num_characters_per_utterance:.2f}')


class DialoguePreprocessorHooksMixin:
    def on_load_files_end(self, files: Iterable) -> None:
        for callback in self.callbacks:
            callback.on_load_files_end(self, files)

    def on_save_dialogue_start(self, dialogues) -> None:
        for callback in self.callbacks:
            callback.on_save_dialogue_start(self, dialogues)
