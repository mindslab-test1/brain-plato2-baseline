from abc import ABC, abstractmethod
from posixpath import join
from typing import List

class PartialUtterancesJoiner(ABC):
    @abstractmethod
    def __call__(self, partial_utterances: List[str]) -> str:
        pass


class StringJoiner(PartialUtterancesJoiner):
    def __init__(self, join_with: str):
        self.join_with = join_with

    def __call__(self, partial_utterances: List[str]) -> str:
        return self.join_with.join(partial_utterances)