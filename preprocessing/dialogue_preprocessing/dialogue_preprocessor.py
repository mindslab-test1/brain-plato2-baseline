from json.decoder import JSONDecodeError
from pathlib import Path
from argparse import ArgumentParser, Namespace
from tqdm import tqdm
import itertools
from typing import Any, Iterable, Dict, List, Iterable, Sequence
from pprint import pprint
from .file_processors import FileProcessor
from .dialogue_file_writers import DialogueFileWriter
from .utterance_processor import UtteranceProcessor
from .callbacks import DialoguePreprocessorCallback, DialoguePreprocessorHooksMixin

# TODO: Add logging


class DialoguePreprocessor(
    DialoguePreprocessorHooksMixin,
):
    def __init__(
        self, 
        code: str, 
        file_processor: FileProcessor,
        utterance_processor: UtteranceProcessor,
        dialogue_file_writer: DialogueFileWriter,
        callbacks: Sequence[DialoguePreprocessorCallback] = [],
        **kwargs
    ):
        # TODO: Take care of adding the code somewhere else.
        self.code = code

        self.file_processor = file_processor
        self.utterance_processor = utterance_processor
        self.dialogue_file_writer = dialogue_file_writer
        self.callbacks = callbacks

    def execute(self):
        dialogues_list = []

        files = self.file_processor.load_files()
        self.on_load_files_end(files)

        print('Let us begin.')
        # TODO: Control the progress bar through callbacks.
        pbar = tqdm(files)
        for file_path in pbar:
            pbar.set_description(f'Processing {file_path}')
            try:
                dialogues = self._process_file(file_path)
                dialogues_list.append(dialogues)
            except Exception as e:
                print(e)
                print(f'An exception occured while processing {file_path}.')
                if input('Enter \'y\' if you want to stop: ').lower() == 'y':
                    raise e
                else:
                    continue

        # Flatten the list.
        dialogues = list(itertools.chain.from_iterable(dialogues_list)) 

        # Assign an id to each of the dialogues.
        # TODO: Separate this logic.
        for dialogue_idx, dialogue in enumerate(dialogues):
            # TODO: Separate these logics below.
            dialogue['id'] = dialogue_idx
            dialogue['code'] = self.code

        self.on_save_dialogue_start(dialogues)
        self.dialogue_file_writer.save_dialogues(dialogues)

    def _process_file(self, file_path: str):
        dialogues = []

        # TODO: Control the progress bar through callbacks.
        extracted_dialogues = self.file_processor.extract_dialogues(file_path)

        for utterance_objects in extracted_dialogues:

            utterance_speaker_id_pairs = self.file_processor.extract_utterance_speaker_id_pairs(utterance_objects)

            try:
                dialogue = self._extract_dialogue(utterance_speaker_id_pairs) 
            except Exception as e:
                print(f'An exception occured while processing {file_path}.')
                print(e)
                if input('Enter \'y\' if you want to raise the exception: ').lower() == 'y':
                    raise e
                else:
                    continue

            output_dict = {
                'dialogue': dialogue,
                'file_name': Path(file_path).name,
            } 

            if len(output_dict['dialogue']) < 2:
                print('len(dialogue) is less than 2. This conversation will not be added to the final output.')
                print(file_path)
                pprint(output_dict['dialogue'])
                continue

            dialogues.append(output_dict)

        return dialogues

    def _extract_dialogue(self, utterance_speaker_id_pairs: Iterable[Dict[str, str]]):
        dialogue = []
        speaker_ids = {}
        cur_speaker_id = 0
        cur_turn = 0
        prev_utterance = None

        for utterance, speaker_id in utterance_speaker_id_pairs:
            # TODO: Add validation check callback for the pair.

            # Register the current speaker ID if it is the first time seeing this speaker. 
            if speaker_id not in speaker_ids:
                speaker_ids[speaker_id] = cur_speaker_id
                cur_speaker_id += 1

            # Save the first utterance of the dialogue.
            if prev_utterance is None:
                prev_utterance = {
                    # TODO: Get the number from a config.
                    'id': cur_turn,
                    'speaker': speaker_ids[speaker_id],
                    'utterance': [utterance],
                }
                continue
            
            # Process accumulated utterances if there has been a turn change.
            # And set the current utterance as the previous utterance.
            if prev_utterance['speaker'] != speaker_ids[speaker_id]:
                prev_utterance['utterance'] = [
                    self.utterance_processor.process_partial_utterance(partial_utterance) 
                    for partial_utterance in prev_utterance['utterance']
                ]
                prev_utterance['utterance'] = self.utterance_processor.join_partial_utterances(prev_utterance['utterance'])
                prev_utterance['utterance'] = self.utterance_processor.process_complete_utterance(prev_utterance['utterance'])

                # Add to the dialogue list if the accumulated utterance is not just whitespaces.
                if not prev_utterance['utterance'].isspace() and len(prev_utterance['utterance']) > 0:
                    dialogue.append(prev_utterance)
                    cur_turn += 1

                prev_utterance = {
                    'id': cur_turn,
                    'speaker': speaker_ids[speaker_id],
                    'utterance': [utterance],
                }
            # If it was the same speaker as the previous one, append the utterance at the end of the atterances list.
            else:
                prev_utterance['utterance'].append(utterance)

        # Check if there are any leftovers.
        if prev_utterance is not None:
            prev_utterance['utterance'] = [
                self.utterance_processor.process_partial_utterance(partial_utterance) 
                for partial_utterance in prev_utterance['utterance']
            ]
            prev_utterance['utterance'] = self.utterance_processor.join_partial_utterances(prev_utterance['utterance'])
            prev_utterance['utterance'] = self.utterance_processor.process_complete_utterance(prev_utterance['utterance'])
            if not prev_utterance['utterance'].isspace() and len(prev_utterance['utterance']) > 0:
                dialogue.append(prev_utterance)

        return dialogue

    @staticmethod 
    def add_argparse_args(parent_parser: ArgumentParser):
        parser = parent_parser.add_argument_group('DialogueProcessor arguments.')
        parser.add_argument('--code')
        parser.add_argument('--destination_path')
        return parent_parser

    @classmethod 
    def from_argparse_args(cls, args: Namespace, file_processor: FileProcessor, utterance_processor: UtteranceProcessor):
        # I Know...
        args_dict = vars(args)
        return cls(**args_dict, file_processor=file_processor, utterance_processor=utterance_processor) 
    
