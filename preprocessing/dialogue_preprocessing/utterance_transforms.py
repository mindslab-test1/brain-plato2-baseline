import unicodedata
from abc import ABC, abstractmethod

# Just in case more complicated cases that require multiple attributes arise.
class UtteranceTransform(ABC):
    @abstractmethod
    def __call__(self, utterance: str):
        pass 

class NFKCNormalizer(UtteranceTransform):
    def __call__(self, utterance: str):
        return unicodedata.normalize('NFKC', utterance)

class WhitespaceNormalizer(UtteranceTransform):
    def __call__(self, utterance: str):
        return ' '.join(utterance.split())

# TODO: Is it double p? I am too afraid to Google it at the office.
class Stripper(UtteranceTransform):
    def __call__(self, utterance: str):
        return utterance.strip()