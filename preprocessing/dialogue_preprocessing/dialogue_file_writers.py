import json
from abc import ABC, abstractmethod
from typing import Any, Iterable, Dict, List, Sequence, Tuple, Optional
from pathlib import Path
import git, sys


class DialogueFileWriter(ABC):
    def save_dialogues(self, dialogues) -> None:
        object_to_save = self.make_object_to_save(dialogues)
        dest_path = self.make_file_name(dialogues)
        self.save_file(object_to_save=object_to_save, dest_path=dest_path)

    @abstractmethod
    def make_object_to_save(self, dialogues):
        pass
    
    @abstractmethod
    def make_file_name(self, dialogues):
        pass
    
    @abstractmethod
    def save_file(self, dest_path, object_to_save):
        pass


class DefaultMakeObjectToSaveMixin:
    def make_object_to_save(self, dialogues):
        # TODO: Make a class that represents the object to be saved, so that it does not have to be a dict.
        dialogues_key = getattr(self, 'make_object_to_save_dialogues_key', 'data')
        result = {
            dialogues_key: dialogues,
        }

        for key_value_adder in self.key_value_adders:
            result = key_value_adder.add_key_value_pair_to(result)

        return result


# class DialogueObjectToBeSavedWrapper(ABC):
#     @abstractmethod
#     @property
#     def raw_object_to_be_saved(self):
#         pass

#     def add(self):
#         pass

# class DialogueObjectToBeSavedDictionaryWrapper(ABC):
#     def __init__(self, dialogues_key: str, dialogues: List):
#         self.dialogue_object = {dialogues_key: dialogues}

#     @property
#     def raw_object_to_be_saved(self):
#         return self.dialogue_object 
    

class KeyValueAdder(ABC):
    @abstractmethod 
    def add_key_value_pair_to(self, dialogue_object_to_be_saved: Dict):
        pass


class GeneralKeyValueAdder(KeyValueAdder):
    def __init__(self, key: str, value: Any = None):
        self.key = key
        self.value = value
    
    def add_key_value_pair_to(self, dialogue_object_to_be_saved: Dict):
        assert self.key not in dialogue_object_to_be_saved, f'The key {self.key} already exists.'
        dialogue_object_to_be_saved[self.key] = self.value
        return dialogue_object_to_be_saved


class CommitHashAdder(KeyValueAdder):
    def __init__(self, key: str = 'commit_hash'):
        self.key = key
    
    def add_key_value_pair_to(self, dialogue_object_to_be_saved: Dict):
        assert self.key not in dialogue_object_to_be_saved, f'The key {self.key} already exists.'
        hexsha = git.Repo().head.object.hexsha
        dialogue_object_to_be_saved[self.key] = hexsha
        return dialogue_object_to_be_saved


class CommandLineArgumentsAdder(KeyValueAdder):
    def __init__(self, key: str = 'argv'):
        self.key = key

    def add_key_value_pair_to(self, dialogue_object_to_be_saved: Dict):
        assert self.key not in dialogue_object_to_be_saved, f'The key {self.key} already exists.'
        argv = sys.argv
        dialogue_object_to_be_saved[self.key] = argv
        return dialogue_object_to_be_saved
        

class DefaultMakeFileNameMixin:
    def make_file_name(self, dialogues, suffix: str = None):
        dest_path = Path(self.destination_path)
        # Create the parent directories if they do not already exist.
        dest_path.parent.mkdir(parents=True, exist_ok=True)
        # What is wrong with with_stem()?
        if suffix is not None:
            dest_path = dest_path.with_name(f'{dest_path.stem}_{suffix}_({len(dialogues)}){dest_path.suffix}')
        else:
            dest_path = dest_path.with_name(f'{dest_path.stem}_({len(dialogues)}){dest_path.suffix}')
        return dest_path


class DefaultSaveFileMixin:
    def save_file(self, dest_path: str, object_to_save):
        print(f'Writing the contents to {dest_path}...') 
        with open(dest_path, 'w') as f:
            json.dump(object_to_save, f, indent=4, sort_keys=True, ensure_ascii=False)
        print(f'Saved to {dest_path}') 


# TODO: I don't really see the reason for using mixins.
class DefaultDialogueFileWriter(
    DefaultMakeObjectToSaveMixin,
    DefaultMakeFileNameMixin,
    DefaultSaveFileMixin,
    DialogueFileWriter,
):
    def __init__(
        self,
        destination_path: str,
        make_object_to_save_dialogues_key: Optional[str],
        key_value_adders: Sequence[KeyValueAdder] = [],
    ):
        super().__init__() 
        self.make_object_to_save_dialogues_key = make_object_to_save_dialogues_key
        self.destination_path = destination_path 
        self.key_value_adders = key_value_adders


class SplitDialogueFileWriter(
    DefaultDialogueFileWriter
):
    def __init__(
        self,
        destination_path: str,
        make_object_to_save_dialogues_key: Optional[str],
        key_value_adders: Sequence[KeyValueAdder] = [],
        num_dialogues_per_file: Optional[int] = None,
    ):
        super().__init__(
            make_object_to_save_dialogues_key=make_object_to_save_dialogues_key,
            destination_path=destination_path,
            key_value_adders=key_value_adders,
        ) 
        self.num_dialogues_per_file = num_dialogues_per_file

    def save_dialogues(self, dialogues) -> None:
        if self.num_dialogues_per_file is not None:
            dialogues_list = [
                dialogues[i : i + self.num_dialogues_per_file] 
                for i in range(0, len(dialogues), self.num_dialogues_per_file)
            ]
            for i, dialogues in enumerate(dialogues_list):
                object_to_save = self.make_object_to_save(dialogues)
                dest_path = self.make_file_name(dialogues, suffix=i)
                self.save_file(object_to_save=object_to_save, dest_path=dest_path)
        else:
            super().save_dialogues(dialogues)