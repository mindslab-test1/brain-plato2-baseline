import dialogue_preprocessing
from jsonargparse import ArgumentParser, ActionConfigFile


class DialoguePreprocessingCLI:
    def __init__(self):
        parser = ArgumentParser()
        parser.add_class_arguments(dialogue_preprocessing.dialogue_preprocessor.DialoguePreprocessor, 'dialogue_preprocessor')
        parser.add_argument('--config', action=ActionConfigFile)

        args = parser.parse_args()
        classes = parser.instantiate_classes(args)

        classes['dialogue_preprocessor'].execute()