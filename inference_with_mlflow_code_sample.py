import mlflow
from dialogue_dataset.dataclasses import DialogueSample
from pprint import pprint
from pytorch_lightning import seed_everything
from plato.plato_model import PlatoForGeneration

if __name__ == '__main__':

    print('Loading the model from mlflow.')
    loaded_model: PlatoForGeneration = mlflow.pytorch.load_model('models:/plato_2_kcbert_pretrained/staging')

    dialogue_samples = [
        DialogueSample(
            response_speaker=1,
            response=None,
            context=[
                '내일 정말 좋은 일이 있을 것 같아.',
                '무슨 일이 있을 것 같은데?',
                '모르겠어. 그냥 좋은 일이 있을 것 같은 느낌이 들어.'
            ],
            context_speakers=[
                0, 1, 0,
            ]
        ),
    ]

    # Move the fine-grained generation model to the first GPU.
    loaded_model.fine_grained_generation.to('cuda:0')
    # Move the response coherence estimation model to CPU.
    loaded_model.response_coherence_estimation.to('cpu')

    # Make the generation deterministic (optional).
    seed_everything(1)

    output = loaded_model.generate_wrapper(
        contexts=dialogue_samples,
        max_length=1024,
        max_new_tokens=129,
        do_sample=True,
        num_beams=1,
        temperature=1.0,
        top_k=0,
        top_p=0.5,
        bad_words_ids=[[30001]],
    )

    print('Output:')
    pprint(output)

    print(f'Selected response: {output[0][0]["response"]}')