from pytorch_lightning.callbacks import Callback
import torch, copy
from dialogue_dataset.dialogue_tokenizer import (
    DialogueTokenizer
)
from typing import Optional, Dict, Any

class PrintPredictionsCallback(Callback):
    def __init__(self, tokenizer: DialogueTokenizer, interval: int = 10000):
        self.tokenizer = tokenizer
        self.interval = interval

    def on_validation_batch_start(self, trainer, pl_module, batch, batch_idx, dataloader_idx):
        if batch_idx == 0 and dataloader_idx == 0:
            self._print_predictions(batch, pl_module)
    
    def on_train_batch_start(self, trainer, pl_module, batch, batch_idx, dataloader_idx):
        pl_module.eval()
        if batch_idx % self.interval == 0 and dataloader_idx == 0:
            self._print_predictions(batch, pl_module)
        pl_module.train()
        
    def _print_predictions(self, batch, pl_module):
        with torch.no_grad():
            for key, value in batch.items():
                batch[key] = value.to(pl_module.device)

            logits = pl_module(**batch)['logits']
            predictions = torch.argmax(logits, dim=-1)
            input_ids = batch['input_ids']

            for original, prediction in zip(input_ids.tolist(), predictions.tolist()):
                print('Origianl:', self.tokenizer.decode(original))
                print('Prediction:', self.tokenizer.decode(prediction))
                print('Origianl ids:', original)
                print('Prediction ids:', prediction)
                print('')


class LogPredictionsToTensorboardCallback(Callback):
    def __init__(
        self, 
        tokenizer: DialogueTokenizer, 
        interval: int = 10000,
        max_num_samples: Optional[int] = float('inf'),
        eos_token_id: Optional[int] = None
    ):
        self.tokenizer = tokenizer
        self.interval = interval
        self.max_num_samples = max_num_samples
        self.eos_token_id = eos_token_id
        self.prev_log_step = float('-inf')

    def on_validation_batch_start(self, trainer, pl_module, batch, batch_idx, dataloader_idx):
        if batch_idx == 0 and dataloader_idx == 0:
            predictions = self._make_predictions_string(batch, pl_module)
            tensorboard = pl_module.logger.experiment
            # tensorboard.add_text(f'valid_epoch_{pl_module.current_epoch}_predictions', predictions, pl_module.global_step)
            tensorboard.add_text(f'valid_predictions', predictions, pl_module.global_step)
    
    def on_train_batch_start(self, trainer, pl_module, batch, batch_idx, dataloader_idx):
        if self.prev_log_step != pl_module.global_step and pl_module.global_step % self.interval == 0:
            pl_module.eval()
            predictions = self._make_predictions_string(batch, pl_module)
            tensorboard = pl_module.logger.experiment
            tensorboard.add_text(f'train_predictions', predictions, pl_module.global_step)
            self.prev_log_step = pl_module.global_step
            pl_module.train()

    @torch.no_grad() 
    def _make_predictions_string(self, batch, pl_module):
        for key, value in batch.items():
            batch[key] = value.to(pl_module.device)

        logits = pl_module(**batch)['logits']
        predictions = torch.argmax(logits, dim=-1)
        input_ids = batch['input_ids']
        labels = batch['labels']
        bos_token_indices = batch['bos_token_index'].tolist() if 'bos_token_index' in batch else [0] * len(labels)

        eos_token_id = None
        if self.eos_token_id is not None:
            eos_token_id = self.eos_token_id
        elif getattr(self.tokenizer, 'eos_token_id', None) is not None:
            eos_token_id = self.tokenizer.eos_token_id

        if eos_token_id is not None:
            # Assume that each label sequence contains exactly one eos token.
            eos_token_indices = (batch['labels'] == eos_token_id).nonzero(as_tuple=True)[1].tolist()
        else:
            eos_token_indices = batch['eos_token_index'].tolist() if 'eos_token_index' in batch else [None] * len(labels)
        
        predictions_list = []
        for sample_idx, (input_id, prediction, label, bos_token_index, eos_token_index) in enumerate(zip(
                input_ids.tolist(), predictions.tolist(), labels.tolist(), bos_token_indices, eos_token_indices,
            )):
            if sample_idx >= self.max_num_samples:
                break
            label = [idx for idx in label if idx >= 0]
            prediction = prediction[bos_token_index : eos_token_index + 1]
            partial_prediction_texts = []
            partial_prediction_texts.append(f'Decoded input_ids: {self.tokenizer.decode(input_id)}')
            partial_prediction_texts.append(f'Decoded labels: {self.tokenizer.decode(label)}')
            partial_prediction_texts.append(f'Prediction: {self.tokenizer.decode(prediction)}')
            partial_prediction_texts.append(f'input_ids: {input_id}')
            partial_prediction_texts.append(f'labels: {label}')
            partial_prediction_texts.append(f'Prediction ids: {prediction}')
            prediction_text = '  \n'.join(partial_prediction_texts)
            predictions_list.append(prediction_text)
        
        predictions_string = '  \n  \n'.join(predictions_list)
        return predictions_string


class LogModelGenerationsToTensorboardCallback(Callback):
    def __init__(
        self, 
        tokenizer: DialogueTokenizer, 
        generate_config: Dict[str, Any],
        interval: int = 10000,
        max_num_samples: Optional[int] = float('inf'),
        eos_token_id: Optional[int] = None,
    ):
        self.tokenizer = tokenizer
        self.generate_config = generate_config
        self.interval = interval
        self.max_num_samples = max_num_samples
        self.eos_token_id = eos_token_id
        self.prev_log_step = float('-inf')

    def on_validation_batch_start(self, trainer, pl_module, batch, batch_idx, dataloader_idx):
        if batch_idx == 0 and dataloader_idx == 0:
            predictions = self._make_generations_string(copy.deepcopy(batch), pl_module)
            tensorboard = pl_module.logger.experiment
            # tensorboard.add_text(f'valid_epoch_{pl_module.current_epoch}_predictions', predictions, pl_module.global_step)
            tensorboard.add_text(f'valid_generations', predictions, pl_module.global_step)
    
    def on_train_batch_start(self, trainer, pl_module, batch, batch_idx, dataloader_idx):
        if self.prev_log_step != pl_module.global_step and pl_module.global_step % self.interval == 0:
            pl_module.eval()
            predictions = self._make_generations_string(copy.deepcopy(batch), pl_module)
            tensorboard = pl_module.logger.experiment
            tensorboard.add_text(f'train_generations', predictions, pl_module.global_step)
            self.prev_log_step = pl_module.global_step
            pl_module.train()

    @torch.no_grad() 
    def _make_generations_string(self, batch, pl_module):
        # TODO: This is not a good idea.
        process_batch_for_generation = getattr(pl_module, 'process_batch_for_generation', None)
        if process_batch_for_generation is not None and callable(process_batch_for_generation):
            processed_batch, input_ids, labels = process_batch_for_generation(batch) 
        else:
            processed_batch, input_ids, labels = batch, batch['input_ids'], batch['labels']

        for key, value in processed_batch.items():
            processed_batch[key] = value.to(pl_module.device)

        
        # TODO: Clean up the if statements.
        if 'max_length' in self.generate_config:
            if pl_module.config.is_decoder and self.generate_config['max_length'] is not None:
                self.generate_config['max_length'] = self.generate_config['max_length'] + processed_batch['input_ids'].size(1)

        # Overwrite items in batch with generate_config, just in case there is an overlap.
        arguments_to_the_generate_method = {
            **processed_batch,
            **self.generate_config,
        }
    
        generated_ids = pl_module.generate(
            **arguments_to_the_generate_method
        )
        if pl_module.config.is_decoder:
            generated_ids = generated_ids[:, arguments_to_the_generate_method['input_ids'].size(1):]

        eos_token_id = None
        if self.eos_token_id is not None:
            eos_token_id = self.eos_token_id
        elif getattr(self.tokenizer, 'eos_token_id', None) is not None:
            eos_token_id = self.tokenizer.eos_token_id

        if eos_token_id is not None:
            # Assume that each label sequence contains exactly one eos token.
            eos_token_indices = (generated_ids == eos_token_id).nonzero(as_tuple=True)[1].tolist()
        else:
            [None] * generated_ids.size(0)
        
        generations_list = []
        for sample_idx, (input_id, generation, label, eos_token_index) in enumerate(zip(
                input_ids.tolist(), generated_ids.tolist(), labels.tolist(), eos_token_indices,
            )):
            if sample_idx >= self.max_num_samples:
                break
            label = [idx for idx in label if idx >= 0]
            generation = generation[:eos_token_index + 1]
            partial_generation_texts = []
            partial_generation_texts.append(f'Decoded input_ids: {self.tokenizer.decode(input_id)}')
            partial_generation_texts.append(f'Decoded labels: {self.tokenizer.decode(label)}')
            partial_generation_texts.append(f'Model generation: {self.tokenizer.decode(generation)}')
            partial_generation_texts.append(f'input_ids: {input_id}')
            partial_generation_texts.append(f'labels: {label}')
            partial_generation_texts.append(f'Model generation ids: {generation}')
            generation_text = '  \n'.join(partial_generation_texts)
            generations_list.append(generation_text)
            
            if sample_idx >= self.max_num_samples:
                break
        
        generations_string = '  \n  \n'.join(generations_list)
        return generations_string
