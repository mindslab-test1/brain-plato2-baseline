import torch
from typing import Optional

def calculate_accuracy(predictions: torch.Tensor, labels: torch.Tensor, padding_idx: Optional[int] = None):
    with torch.no_grad():
        # Get the top-1 prediction indices.
        predictions = torch.argmax(predictions, dim=-1)
        # Reset the padding index to -1.
        if padding_idx is not None:
            labels = torch.where(
                labels == padding_idx,
                torch.tensor(-1, dtype=torch.long, device=labels.device),
                labels
            )
        # Calculate the number of correct predictions.
        num_correct_predictions = torch.count_nonzero(predictions == labels, dim=None)
        # Count the number of non-padding targets. (Negative indices are assumed to be invalid targets.)
        num_targets = torch.count_nonzero(labels >= 0, dim=None)
        # Calculate the accuracy by dividing the number of correct predictions by the number of non-padding targets.
        accuracy = torch.true_divide(num_correct_predictions, num_targets)
        return accuracy 