import torch

def process_attention_mask(attention_mask: torch.LongTensor) -> torch.FloatTensor:
    if attention_mask.dim() == 3:
        attention_mask = attention_mask.unsqueeze(1)

    # 1 becomes 0.0 and 0 becomes -10000.0, which would make the attention scores very small when added.
    attention_mask = (1.0 - attention_mask) * -10000.0
    return attention_mask