class LinearWarmUpDecay:
    def __init__(
        self, 
        learning_rate_warm_up_step: int,
        learning_rate_decay_end_step: int,
        learning_rate_min_decay_factor: float = 0.0,
    ):
        self.learning_rate_warm_up_step = learning_rate_warm_up_step
        self.learning_rate_decay_end_step = learning_rate_decay_end_step
        self.learning_rate_min_decay_factor = learning_rate_min_decay_factor

    def __call__(self, current_step):
        if current_step < self.learning_rate_warm_up_step:
            return current_step / self.learning_rate_warm_up_step
        else:
            return max(
                (self.learning_rate_decay_end_step - current_step) / \
                (self.learning_rate_decay_end_step - self.learning_rate_warm_up_step),
                self.learning_rate_min_decay_factor
            )