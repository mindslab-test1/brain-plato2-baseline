# import json, glob, random, copy, math, torch
# from warnings import resetwarnings
# from torch.utils.data import IterableDataset, Dataset
# from dataclasses import dataclass
# from contextlib import contextmanager
# from typing import List, Dict
# from more_itertools import peekable
# from tqdm import tqdm

# class JsonLoader:
#     def __init__(self, file_path, config):
#         self.file_path = file_path
    
#     def load_dialogues(self):
#         with open(self.file_path) as f:
#             dialogues = json.load(f)
#         dialogues = dialogues['data']
#         dialogues = [
#             [ {'utterance': utterance['utterance'], 'speaker': ord(utterance['speaker']) - 65} 
#             for utterance in dialogue['dialog']]
#             for dialogue in dialogues
#         ] 
#         return dialogues

# class SampleMaker:
#     def __init__(self, config, tokenizer, span_extractor, dialogues):
#         self.config = config
#         self.span_extractor = span_extractor
#         self.dialogues = dialogues
    
#     def __iter__(self):
#         cur_num_tokens = 0
#         prev_index = 0
#         for cur_index, utterance_object in enumerate(self.dialogue):
#             utterance = utterance_object['utterance']
#             num_chars = len(utterance)
#             utterance_ids = self.tokenizer.encode(utterance)
#             num_tokens = len(utterance_ids)
#             speaker = utterance_object['speaker']

#             if len(utterance) > self.config.max_num_chars_per_utterance:
#                 if len(cur_index - prev_index) == 0:
#                     cur_num_tokens = 0
#                     continue
#                 else:
#                     yield [prev_index, cur_index - 1]
#                     cur_num_tokens = 0

#             if len(self.tokenizer.encode(utterance)) > self.config.max_num_tokens_per_utterance:
#                 if len(cur_index - prev_index) == 0:
#                     cur_num_tokens = 0
#                     continue
#                 else:
#                     yield [prev_index, cur_index - 1]
#                     cur_num_tokens = 0

#             if cur_index - prev_index + 1 > self.config.max_num_utterances_per_dialogue:
#                 cur_window = []
#                 cur_num_tokens = 0
#                 continue

#     def iterate(self):
#         for dialogue_index, dialogue in enumerate(self.dialogues):
#             for utterance_span in self.span_extractor.extract(dialogue):
#                 span = Span(*(dialogue_index,) + utterance_span)
#                 yield span
#         return
    
#     def extract_dialogue_from_span(self, span):
#         return self.dialogues[span.dialogue_index, span.first_utterance_index : span.last_utterance_index + 1]


# class SpanExtractor:
#     def __init__(self, tokenizer, config):
#         self.tokenizer = tokenizer
#         self.config = config
#         self.num_processed_utterancces = 0
    
#     def extract(self, dialogue):
#         cur_num_tokens = 0
#         prev_index = -1

#         for cur_index, utterance_object in enumerate(dialogue):
#             utterance = utterance_object['utterance']
#             speaker = utterance_object['speaker']
#             self.num_processed_utterancces = 0

#             num_tokens = self.tokenizer.encode(utterance)
#             cur_num_tokens += num_tokens

#             if len(utterance) > self.config.max_num_chars_per_utterance:
#                 full = True

#             if num_tokens > self.config.max_num_tokens_per_utterance:
#                 full = True

#             # If it was not the first utterance.
#             if full and cur_index - prev_index > 1:
#                 yield (prev_index + 1, cur_index)
#                 prev_index = cur_index
#                 cur_num_tokens = 0

#             # If the window is full.
#             elif cur_index - prev_index == self.config.max_num_utterances_per_dialogue:
#                 yield (prev_index + 1, cur_index)
#                 prev_index = cur_index
#                 cur_num_tokens = 0

#             elif cur_num_tokens > self.config.max_num_tokens_per_dialogue:
#                 yield (prev_index + 1, cur_index)
#                 prev_index = cur_index - 1
#                 cur_num_tokens = 0

#         # If there is a leftover.
#         if cur_index - prev_index > 0:
#             yield (prev_index + 1, cur_index)
            
#         return


# @dataclass
# class Span:
#     def __init__(self, dialogue_index, first_utterance_index, last_utterance_index):
#         self.dialogue_index = dialogue_index
#         self.first_utterance_index = first_utterance_index
#         self.last_utterance_index = last_utterance_index


# class DialogueDataset(Dataset):
#     def __init__(self, dialogues, tokenizer, sample_maker):
#         self.tokenizer = tokenizer
#         self.sample_maker = sample_maker
#         self.utterance_spans = [span for span in self.sample_maker.iterate()]

#     def __getitem__(self, idx):
#         utterance_span = self.utterance_spans[idx]
#         dialogue = self.sample_maker.extract_dialogue_from_span(utterance_span)
#         self.tokenizer.encode_dialogue(dialogue)
#         return dialogue
    
#     def __iter__(self):
#         for utterance_span in self.sample_maker.iterate():
#             yield


# # class DialogueBatchProcessor:
# #     def __init__(self):
# #         pass
    
# #     def encode_dialogue(self, dialogue):
# #         pass