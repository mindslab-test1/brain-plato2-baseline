from dialogue_dataset.dataclasses import DialogueSample
from plato.plato_tokenizer import Plato2FineGrainedGenerationTokenizer, Plato2ResponseCoherenceEstimationTokenizer
import torch, copy, contextlib, yaml
import pytorch_lightning as pl
from torch.nn import Module
from torch import nn
import torch.nn.functional as F
from torch.optim.lr_scheduler import LambdaLR
from .pretrained_config_factories import PretrainedConfigFactoryForPlato2
from .pretrained_weight_factories import PretrainedBertModelWeightFactoryForPlato2
from utils.metrics import calculate_accuracy
from utils.learning_rate_schedulers import LinearWarmUpDecay
from utils.huggingface_compatibilities import process_attention_mask
from typing import Optional, Dict, Any, Sequence, Tuple, List
from transformers.generation_utils import GenerationMixin
from transformers.file_utils import ModelOutput, to_py_obj
from transformers.configuration_utils import PretrainedConfig
from utils.tokenizer_utils import pad_left
import torch.distributions.gumbel as gumbel
from base_tokenizers.base_tokenizer_factories import BaseTokenizerFactory


class PlatoGenerationMixin(GenerationMixin):
    # TODO: Is inheriting from a mixin bad idea?
    @staticmethod
    def _expand_inputs_for_generation(
        input_ids: torch.LongTensor,
        expand_size: int = 1,
        is_encoder_decoder: bool = False,
        attention_mask: torch.LongTensor = None,
        encoder_outputs: ModelOutput = None,
        **model_kwargs,
    ) -> Tuple[torch.LongTensor, Dict[str, Any]]:
        # Most of the code is taken from huggingface's generation_utils.py
        expanded_return_idx = (
            torch.arange(input_ids.shape[0]).view(-1, 1).repeat(1, expand_size).view(-1).to(input_ids.device)
        )
        input_ids = input_ids.index_select(0, expanded_return_idx)
            
        for key in ('position_ids', 'turn_ids', 'role_ids'):
            model_kwargs[key] = model_kwargs[key].index_select(0, expanded_return_idx)
        
        latent_act = model_kwargs.get('latent_act', None)
        if latent_act is not None:
            model_kwargs['latent_act'] = latent_act.index_select(0, expanded_return_idx)

        if attention_mask is not None:
            attention_mask = attention_mask.to(input_ids.device)
            model_kwargs["attention_mask"] = attention_mask.index_select(0, expanded_return_idx)

        return input_ids, model_kwargs

    def prepare_inputs_for_generation(
        self, input_ids: torch.LongTensor, past = None, **kwargs,
    ) -> Dict[str, Any]:
        # TODO: Technically they are all required arguments to the model.
        position_ids = kwargs.get('position_ids', None)
        turn_ids = kwargs.get('turn_ids', None)
        role_ids = kwargs.get('role_ids', None)

        kwargs['input_ids'] = input_ids
        kwargs['past_key_values'] = past

        # TODO: I guess a for loop could have simplified things a bit.
        if past is not None:
            kwargs['input_ids'] = input_ids[:, -1].unsqueeze(-1)

            if position_ids is not None:
                kwargs['position_ids'] = position_ids[:, -1].unsqueeze(-1)

            if turn_ids is not None:
                kwargs['turn_ids'] = turn_ids[:, -1].unsqueeze(-1)

            if role_ids is not None:
                kwargs['role_ids'] = role_ids[:, -1].unsqueeze(-1)

        return kwargs

    def _reorder_cache(self, past: Tuple[Tuple[torch.Tensor]], beam_idx: torch.Tensor) -> Tuple[Tuple[torch.Tensor]]:
        # Select key, value caches for the current hypotheses by iterating through the layers.
        return tuple(
            tuple(key_or_value.index_select(0, beam_idx.to(key_or_value.device)) for key_or_value in layer)
            for layer in past
        )

    @staticmethod
    def _update_model_kwargs_for_generation(
        outputs: ModelOutput, model_kwargs: Dict[str, Any], is_encoder_decoder: bool = False
    ) -> Dict[str, Any]:
        # update past
        if "past_key_values" in outputs:
            model_kwargs["past"] = outputs.past_key_values
        elif "mems" in outputs:
            model_kwargs["past"] = outputs.mems
        elif "past_buckets_states" in outputs:
            model_kwargs["past"] = outputs.past_buckets_states
        else:
            model_kwargs["past"] = None

        position_ids = model_kwargs['position_ids']
        model_kwargs['position_ids'] = torch.cat([position_ids, position_ids[:, -1].unsqueeze(-1) + 1], dim=-1)
        
        turn_ids = model_kwargs['turn_ids']
        model_kwargs['turn_ids'] = torch.cat([turn_ids, turn_ids[:, -1].unsqueeze(-1)], dim=-1)

        role_ids = model_kwargs['role_ids']
        model_kwargs['role_ids'] = torch.cat([role_ids, role_ids[:, -1].unsqueeze(-1)], dim=-1)

        attention_mask = model_kwargs['attention_mask']
        batch_size, seq_len, _ = attention_mask.shape
        new_seq_len = seq_len + 1
        new_attention_mask = attention_mask.new_zeros((batch_size, new_seq_len, new_seq_len))
        new_attention_mask[:, :-1, :-1] = attention_mask
        new_attention_mask[:, -1, :] = torch.cat([attention_mask[:, -1, :], attention_mask.new_ones(attention_mask.size(0), 1)], dim=-1)
        model_kwargs['attention_mask'] = new_attention_mask

        return model_kwargs

def process_batch_for_generation(batch: Dict[str, torch.Tensor]):
    bos_token_indices = batch['bos_token_index'].tolist()
    sliced_samples_dict = {}        

    for key, samples in batch.items():
        if key == 'bos_token_index' or not torch.is_tensor(samples) or samples.dim() == 0:
            continue
        
        sliced_samples_value = [
            sample[ [slice(None, bos_token_index + 1, None)] * sample.dim() ] 
            for sample, bos_token_index in zip(torch.unbind(samples, dim=0), bos_token_indices)
        ]
        sliced_samples_dict[key] = sliced_samples_value

    for key, samples in sliced_samples_dict.items():
        if samples[0].dim() == 0:
            sliced_samples_dict[key] = torch.cat(samples)

        if samples[0].dim() == 1:
            # TODO: zero may not be the actual padding id, but they will get masked anyway.
            sliced_samples_dict[key] = pad_left(samples, 0)

        # TODO: I think it could be generalized to higher dimensions, but I don't really care. 
        if samples[0].dim() == 2:
            first_dim_max = max([sample.size(0) for sample in samples])
            second_dim_max = max([sample.size(1) for sample in samples])

            # TODO: There is a chance that zero is not the value that indicates masked state.
            padded_batch = torch.full(size=[len(samples), first_dim_max, second_dim_max], fill_value=0)
            for batch_idx, sample in enumerate(samples):
                padded_batch[batch_idx, first_dim_max - sample.size(0):, second_dim_max - sample.size(1):] = sample
            sliced_samples_dict[key] = padded_batch

    return sliced_samples_dict

class PlatoEmbeddings(Module):
    def __init__(
        self, 
        vocab_size: int,
        hidden_dim: int,
        pad_token_id: int,
        max_position_embeddings: int,
        max_turn_embeddings: int,
        max_role_embeddings: int,
        hidden_dropout_prob: float,
        **kwargs,
    ):
        super().__init__()
        self.word_embeddings = nn.Embedding(vocab_size, hidden_dim, padding_idx=pad_token_id)
        self.position_embeddings = nn.Embedding(max_position_embeddings + 1, hidden_dim, padding_idx=max_position_embeddings)
        self.turn_embeddings = nn.Embedding(max_turn_embeddings + 1, hidden_dim, padding_idx=max_turn_embeddings)
        self.role_embeddings = nn.Embedding(max_role_embeddings + 1, hidden_dim, padding_idx=max_role_embeddings)
        self.layer_norm = nn.LayerNorm(hidden_dim)
        self.dropout = nn.Dropout(hidden_dropout_prob)

    def forward(self, input_ids, position_ids, turn_ids, role_ids):
        embeddings = self.word_embeddings(input_ids)
        embeddings += self.position_embeddings(position_ids)
        embeddings += self.turn_embeddings(turn_ids)
        embeddings += self.role_embeddings(role_ids)
        # TODO: Huggingface's BertEmbeddings applies dropout before layer norm and plato applies dropout after layer norm.
        # TODO: Dropout is not applied to the MASK token and latent embeddings in stage 2.1
        embeddings = self.layer_norm(embeddings)
        embeddings = self.dropout(embeddings)
        return embeddings
    
    def apply_layer_norm_and_dropout(self, embeddings):
        embeddings = self.layer_norm(embeddings)
        embeddings = self.dropout(embeddings)
        return embeddings
    
    @classmethod
    def from_pretrained(
        cls, 
        max_turn_embeddings: int,
        max_role_embeddings: int,
        hidden_dropout_prob: float,
        word_embeddings: Optional[torch.nn.Module] = None,
        position_embeddings: Optional[torch.nn.Module] = None,
        pad_token_id: int = None,
        vocab_size: int = None,
        hidden_dim: int = None,
        max_position_embeddings: int = None,
    ):
        if pad_token_id is not None:
            pad_token_id = word_embeddings.padding_idx
        if vocab_size is not None:
            vocab_size = word_embeddings.weight.size(0)
        if hidden_dim is not None:
            hidden_dim = word_embeddings.weight.size(1)
        if max_position_embeddings is not None:
            max_position_embeddings = position_embeddings.weight.size(0)

        plato_embeddings = cls(
            vocab_size=vocab_size,
            hidden_dim=hidden_dim,
            pad_token_id=pad_token_id,
            max_position_embeddings=max_position_embeddings,
            max_turn_embeddings=max_turn_embeddings,
            max_role_embeddings=max_role_embeddings,
            hidden_dropout_prob=hidden_dropout_prob,
        )
        with torch.no_grad():
            plato_embeddings.word_embeddings.weight = word_embeddings.weight
            # Assuming there is no padding id in the pretrained position embedding matrix.
            plato_embeddings.position_embeddings.weight[:-1] = position_embeddings.weight
        return plato_embeddings


class GenerationHead(Module):
    def __init__(self, hidden_size: int, vocab_size: int):
        # TODO: Make adding the bias optional.
        # TODO: Make hidden_transform optional.
        super().__init__()
        self.hidden_transform_linear = nn.Linear(hidden_size, hidden_size)
        self.hidden_transform_activation = nn.functional.gelu
        # TODO: There is no layer norm in PLATO unlike Huggingface's BERT. Let's just use it since our pretrained weighs are trained by using this.
        self.hidden_transform_layer_norm = nn.LayerNorm(hidden_size)
        # TODO: There is no bias in PLATO unlike Huggingface's BERT.
        self.prediction_linear = nn.Linear(hidden_size, vocab_size)

    def forward(self, hidden_states, decoder_weight = None):
        hidden_states = self.hidden_transform_linear(hidden_states)
        hidden_states = self.hidden_transform_activation(hidden_states)
        hidden_states = self.hidden_transform_layer_norm(hidden_states)

        if decoder_weight is None:
            hidden_states = self.prediction_linear(hidden_states)
        else:
            hidden_states = torch.matmul(decoder_weight, hidden_states.transpose(1, 2))
        return hidden_states


class PlatoGenerationOutput(ModelOutput):
    logits: torch.FloatTensor = None
    last_hidden_state: torch.FloatTensor
    past_key_values: Optional[Tuple[Tuple[torch.FloatTensor]]] = None
    # hidden_states: Optional[Tuple[torch.FloatTensor]] = None
    # attentions: Optional[Tuple[torch.FloatTensor]] = None

class PlatoCoarseGrainedGeneration(
    pl.LightningModule,
    PlatoGenerationMixin,
):
    def __init__(
            self, 
            max_turn_embeddings: int,
            max_role_embeddings: int,
            pretrained_config_factory: PretrainedConfigFactoryForPlato2,
            learning_rate: Optional[float] = None,
            learning_rate_decay_end_step: Optional[int] = None,
            learning_rate_warm_up_step: Optional[int] = None,
            learning_rate_min_decay_factor: Optional[float] = None,
        ):
        super().__init__()
        self.transfomer_encoder = pretrained_config_factory.get_transformer_encoder()
        self.pretrained_config_factory = pretrained_config_factory

        self.embeddings = PlatoEmbeddings(
            vocab_size=pretrained_config_factory.get_vocab_size(),
            hidden_dim=pretrained_config_factory.get_hidden_dim(),
            pad_token_id=pretrained_config_factory.pad_token_id(),
            max_position_embeddings=pretrained_config_factory.get_max_position_embeddings(),
            hidden_dropout_prob=pretrained_config_factory.get_hidden_dropout_prob(),
            max_turn_embeddings=max_turn_embeddings,
            max_role_embeddings=max_role_embeddings,
        )

        self.generation_head = GenerationHead(
            hidden_size=pretrained_config_factory.get_hidden_dim(),
            vocab_size=pretrained_config_factory.get_vocab_size(),
        )
        self.tie_word_embeddings()

        self.save_hyperparameters(
            'max_turn_embeddings',
            'max_role_embeddings',
            'learning_rate',
            'learning_rate_decay_end_step',
            'learning_rate_warm_up_step',
            'learning_rate_min_decay_factor',
        )

        self._huggingface_config = PretrainedConfig(
            is_encoder_decoder=False,
            is_decoder=True,
        )

    def forward(
        self,
        input_ids: torch.LongTensor,
        position_ids: torch.LongTensor, 
        turn_ids: torch.LongTensor, 
        role_ids: torch.LongTensor,
        attention_mask: torch.LongTensor,
        use_cache: bool = False,
        past_key_values: Tuple[Tuple[torch.FloatTensor]] = None,
        **kwargs,
    ):
        embeddings = self.embeddings(
            input_ids=input_ids,
            position_ids=position_ids,
            turn_ids=turn_ids, 
            role_ids=role_ids,
        )

        attention_mask = process_attention_mask(attention_mask)

        encoder_output = self.transfomer_encoder(
            hidden_states=embeddings,
            attention_mask=attention_mask,
            past_key_values=past_key_values,
            use_cache=use_cache,
        )

        logits = self.generation_head(
            hidden_states=encoder_output['last_hidden_state'],
            # decoder_weight=self.embeddings.word_embeddings.weight,
        )
        
        return PlatoGenerationOutput(
            logits=logits,
            last_hidden_state=encoder_output['last_hidden_state'],
            past_key_values=encoder_output.get('past_key_values', None),
        )

    def training_step(self, batch, batch_idx):
        # from pprint import pprint
        # for key, value in batch.items():
        #     print(key)
        #     pprint(value.tolist(), width=400)

        logits = self(**batch)['logits']
        # Assume that the labels are left-shifted so that shifting the logits is not necessary.
        loss = F.cross_entropy(logits.view(-1, self.pretrained_config_factory.get_vocab_size()), batch['labels'].view(-1))
        self.log('train_loss', loss, prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_perplexity', torch.exp(loss), prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log(
            'train_accuracy', 
            calculate_accuracy(predictions=logits, labels=batch['labels']), 
            prog_bar=False, 
            logger=True, 
            on_step=True, 
            on_epoch=True
        )
        return loss

    def validation_step(self, batch, batch_idx):
        logits = self(**batch)['logits']
        # Assume that the labels are shifted so that shifting the logits is not necessary.
        loss = F.cross_entropy(logits.view(-1, self.pretrained_config_factory.get_vocab_size()), batch['labels'].view(-1))
        self.log('val_loss', loss, prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log('val_perplexity', torch.exp(loss), prog_bar=True, logger=True, on_step=False, on_epoch=True)
        self.log(
            'val_accuracy', 
            calculate_accuracy(predictions=logits, labels=batch['labels']), 
            prog_bar=True, 
            logger=True, 
            on_step=False, 
            on_epoch=True,
        )

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.learning_rate)

        assert all([
            self.hparams.learning_rate_warm_up_step is not None,
            self.hparams.learning_rate_decay_end_step is not None,
            self.hparams.learning_rate_min_decay_factor is not None,
        ]), 'Learning rate related arguments should not be None when the model is used for training.'

        warm_up = LinearWarmUpDecay(
            learning_rate_warm_up_step=self.hparams.learning_rate_warm_up_step,
            learning_rate_decay_end_step=self.hparams.learning_rate_decay_end_step,
            learning_rate_min_decay_factor=self.hparams.learning_rate_min_decay_factor,
        )

        warmup_scheduler = {
            'scheduler': LambdaLR(optimizer=optimizer, lr_lambda=warm_up),
            'interval': 'step',
        }

        return [optimizer], [warmup_scheduler]

    def tie_word_embeddings(self):
        self.generation_head.prediction_linear.weight = self.embeddings.word_embeddings.weight
    
    def load_pretrained_weights(
        self, 
        pretrained_weight_factory: PretrainedBertModelWeightFactoryForPlato2
    ):
        self.transfomer_encoder.load_state_dict(pretrained_weight_factory.get_transformer_encoder().state_dict()) 
        self.embeddings.word_embeddings.load_state_dict(pretrained_weight_factory.get_word_embeddings().state_dict())

        with torch.no_grad():
            # Reset position embedding weights other than the padding index's embedding.
            self.embeddings.position_embeddings.weight[:-1] = pretrained_weight_factory.get_position_embeddings().weight
            # Reset token embedding weights. If current vocab size is larger than the pretrained model's, then skip the remaining ones.
            pretrained_word_embedding = pretrained_weight_factory.get_word_embeddings()
            self.embeddings.word_embeddings.weight[:pretrained_word_embedding.weight.size(0)] = pretrained_word_embedding.weight.data
        
        if pretrained_weight_factory.get_mlm_hidden_transform_linear() is not None:
            self.generation_head.hidden_transform_linear.load_state_dict(
                pretrained_weight_factory.get_mlm_hidden_transform_linear().state_dict()
            )

        # Even though an activation function is not necessarily composed of pretrained weights...
        if pretrained_weight_factory.get_mlm_hidden_transform_activation() is not None:
            self.generation_head.hidden_transform_activation = pretrained_weight_factory.get_mlm_hidden_transform_activation()
        if pretrained_weight_factory.get_mlm_hidden_transform_layer_norm() is not None:
            self.generation_head.hidden_transform_layer_norm.load_state_dict(
                pretrained_weight_factory.get_mlm_hidden_transform_layer_norm().state_dict()
            )
        if pretrained_weight_factory.get_mlm_prediction_bias() is not None:
            with torch.no_grad():
                # TODO: The bias term may not be of the same size.
                self.generation_head.prediction_linear.bias.copy_(pretrained_weight_factory.get_mlm_prediction_bias())

    @property
    def config(self):
        return self._huggingface_config

    def process_batch_for_generation(self, batch: Dict[str, torch.Tensor]):
        return process_batch_for_generation(batch), batch['input_ids'], batch['labels']


class LatenActRecognitionHead(Module):
    def __init__(self, hidden_size: int, max_latent_act_embeddings: int, gumbel_softmax_temperature: float):
        super().__init__()
        self.max_latent_act_embeddings = max_latent_act_embeddings
        self.linear = nn.Linear(hidden_size, max_latent_act_embeddings, bias=False)
        self.gumbel_softmax_temperature = gumbel_softmax_temperature
    
    def forward(self, hidden_states: torch.FloatTensor) -> torch.FloatTensor:
        logits = self.linear(hidden_states)
        if self.training: 
            log_softmax = F.log_softmax(logits, dim=-1)
            g = gumbel.Gumbel(0.0, 1.0).expand(log_softmax.size()).sample()
            g = g.to(log_softmax.device)
            gumbel_softmax = F.softmax((log_softmax + g / self.gumbel_softmax_temperature), dim=-1)
            return gumbel_softmax
        else:
            max_embedding_index = torch.argmax(logits, dim=-1)
            return F.one_hot(max_embedding_index, num_classes=self.max_latent_act_embeddings).float()


class BagOfWordsHead(Module):
    def __init__(self, hidden_size: int, vocab_size: int):
        super().__init__()
        self.hidden_transform_linear = nn.Linear(hidden_size, hidden_size)
        self.activation = F.gelu
        # PLATO does not share embeding weights when computing the BoW loss. And no bias term is added.
        self.prediction_linear = nn.Linear(hidden_size, vocab_size, bias=False)
    
    def forward(self, hidden_states: torch.FloatTensor) -> torch.FloatTensor:
        hidden_states = self.hidden_transform_linear(hidden_states)
        hidden_states = self.activation(hidden_states)
        hidden_states = self.prediction_linear(hidden_states)
        return hidden_states


class PlatoFineGrainedGeneration(
    pl.LightningModule,
    PlatoGenerationMixin,
):
    def __init__(
            self, 
            max_turn_embeddings: int,
            max_role_embeddings: int,
            max_latent_act_embeddings: int,
            gumbel_softmax_temperature: float,
            pretrained_config_factory: PretrainedConfigFactoryForPlato2,
            learning_rate: Optional[float] = None,
            adam_eps: Optional[float] = 1.0e-8,
            learning_rate_decay_end_step: Optional[int] = None,
            learning_rate_warm_up_step: Optional[int] = None,
            learning_rate_min_decay_factor: Optional[float] = None,
        ):
        super().__init__()
        self.save_hyperparameters(
            'max_turn_embeddings',
            'max_role_embeddings',
            'max_latent_act_embeddings',
            'gumbel_softmax_temperature',
            'learning_rate',
            'adam_eps',
            'learning_rate_decay_end_step',
            'learning_rate_warm_up_step',
            'learning_rate_min_decay_factor',
        )

        self.transfomer_encoder = pretrained_config_factory.get_transformer_encoder()
        self.pretrained_config_factory = pretrained_config_factory

        self.embeddings = PlatoEmbeddings(
            vocab_size=pretrained_config_factory.get_vocab_size(),
            hidden_dim=pretrained_config_factory.get_hidden_dim(),
            pad_token_id=pretrained_config_factory.pad_token_id(),
            max_position_embeddings=pretrained_config_factory.get_max_position_embeddings(),
            hidden_dropout_prob=pretrained_config_factory.get_hidden_dropout_prob(),
            max_turn_embeddings=max_turn_embeddings,
            max_role_embeddings=max_role_embeddings,
        )

        self.generation_head = GenerationHead(
            hidden_size=pretrained_config_factory.get_hidden_dim(),
            vocab_size=pretrained_config_factory.get_vocab_size(),
        )

        self.latent_act_recognition_head = LatenActRecognitionHead(
            hidden_size=pretrained_config_factory.get_hidden_dim(),
            max_latent_act_embeddings=self.hparams.max_latent_act_embeddings,
            gumbel_softmax_temperature=self.hparams.gumbel_softmax_temperature,
        )

        self.bag_of_words_head = BagOfWordsHead(
            hidden_size=pretrained_config_factory.get_hidden_dim(),
            vocab_size=pretrained_config_factory.get_vocab_size(),
        )

        self.latent_act_embeddings = nn.Embedding(
            num_embeddings=self.hparams.max_latent_act_embeddings,
            embedding_dim=pretrained_config_factory.get_hidden_dim(),
        )

        self.tie_word_embeddings()

        self._huggingface_config = PretrainedConfig(
            is_encoder_decoder=False,
            is_decoder=True,
        )

    def forward(
        self,
        input_ids: torch.LongTensor,
        position_ids: torch.LongTensor, 
        turn_ids: torch.LongTensor, 
        role_ids: torch.LongTensor,
        attention_mask: torch.LongTensor,
        latent_act: torch.LongTensor,
        use_cache: bool = False,
        past_key_values: Tuple[Tuple[torch.FloatTensor]] = None,
        **kwargs,
    ):
        generation_embeddings = self.embeddings(
            input_ids=input_ids,
            position_ids=position_ids,
            turn_ids=turn_ids, 
            role_ids=role_ids,
        )
        if past_key_values is None:
            latent_embedding = self.latent_act_embeddings(latent_act)
            embeddings = torch.cat([latent_embedding, generation_embeddings], dim=1)
        else:
            embeddings = generation_embeddings

        attention_mask = process_attention_mask(attention_mask)

        encoder_output = self.transfomer_encoder(
            hidden_states=embeddings,
            attention_mask=attention_mask,
            past_key_values=past_key_values,
            use_cache=use_cache,
        )

        logits = self.generation_head(
            hidden_states=encoder_output['last_hidden_state'],
            # decoder_weight=self.embeddings.word_embeddings.weight,
        )
        
        return PlatoGenerationOutput(
            logits=logits,
            last_hidden_state=encoder_output['last_hidden_state'],
            past_key_values=encoder_output.get('past_key_values', None),
        )
    
    def shared_step(
        self,
        input_ids: torch.LongTensor,
        position_ids: torch.LongTensor, 
        turn_ids: torch.LongTensor, 
        role_ids: torch.LongTensor,
        generation_attention_mask: torch.LongTensor,
        latent_act_attention_mask: torch.LongTensor,
        labels: torch.LongTensor,
        **kwargs,
    ):
        embeddings = self.embeddings(
            input_ids=input_ids,
            position_ids=position_ids,
            turn_ids=turn_ids, 
            role_ids=role_ids,
        )

        latent_act_attention_mask = process_attention_mask(latent_act_attention_mask)
        latent_embedding = self.latent_act_recognition(embeddings=embeddings, attention_mask=latent_act_attention_mask)
        latent_embedding = self.embeddings.apply_layer_norm_and_dropout(latent_embedding)

        generation_embeddings = torch.cat([latent_embedding.unsqueeze(1), embeddings[:, 1:, :]], dim=1)
        generation_attention_mask = process_attention_mask(generation_attention_mask)

        encoder_output = self.transfomer_encoder(
            hidden_states=generation_embeddings,
            attention_mask=generation_attention_mask,
        )
        last_hidden_state = encoder_output['last_hidden_state']

        bag_of_words_logits = self.bag_of_words_head(
            hidden_states=last_hidden_state[:, 0],
        )
        bag_of_words_logits = bag_of_words_logits.unsqueeze(1).expand(-1, generation_embeddings.size(1), -1)
        bag_of_words_loss = F.cross_entropy(
            bag_of_words_logits.reshape(-1, self.pretrained_config_factory.get_vocab_size()),
            labels.view(-1)
        )
        bag_of_words_accuracy = calculate_accuracy(predictions=bag_of_words_logits,labels=labels)

        generation_logits = self.generation_head(hidden_states=last_hidden_state)
        generation_loss = F.cross_entropy(
            generation_logits.view(-1, self.pretrained_config_factory.get_vocab_size()),
            labels.view(-1)
        )
        generation_accuracy = calculate_accuracy(predictions=generation_logits,labels=labels)

        return {
            'generation_loss': generation_loss,
            'generation_accuracy': generation_accuracy,
            'generation_logits': generation_logits,
            'bag_of_words_loss': bag_of_words_loss,
            'bag_of_words_accuracy': bag_of_words_accuracy,
            'bag_of_words_logits': bag_of_words_logits,
        }

    def latent_act_recognition(self, embeddings, attention_mask):
        encoder_output = self.transfomer_encoder(
            hidden_states=embeddings,
            attention_mask=attention_mask,
        )

        hidden_states = encoder_output['last_hidden_state']
        latent_act = self.latent_act_recognition_head(hidden_states[:, 0])
        latent_embedding = torch.matmul(latent_act, self.latent_act_embeddings.weight)

        return latent_embedding

    def training_step(self, batch, batch_idx):
        # from pprint import pprint
        # for key, value in batch.items():
        #     print(key)
        #     pprint(value.tolist(), width=400)

        output = self.shared_step(**batch)
        loss = output['generation_loss'] + output['bag_of_words_loss']

        # Assume that the labels are left-shifted so that shifting the logits is not necessary.
        self.log('train_generation_loss', output['generation_loss'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_generation_perplexity', torch.exp(output['generation_loss']), prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_generation_accuracy', output['generation_accuracy'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_bow_loss', output['bag_of_words_loss'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_bow_accuracy', output['bag_of_words_accuracy'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_combined_loss', loss, prog_bar=False, logger=True, on_step=True, on_epoch=True)

        return loss

    def validation_step(self, batch, batch_idx):
        output = self.shared_step(**batch)
        loss = output['generation_loss'] + output['bag_of_words_loss']

        # Assume that the labels are left-shifted so that shifting the logits is not necessary.
        self.log('val_generation_loss', output['generation_loss'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_generation_perplexity', torch.exp(output['generation_loss']), prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_generation_accuracy', output['generation_accuracy'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_bow_loss', output['bag_of_words_loss'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_bow_accuracy', output['bag_of_words_accuracy'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_combined_loss', loss, prog_bar=False, logger=True, on_step=False, on_epoch=True)

        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(
            self.parameters(), 
            lr=self.hparams.learning_rate,
            eps=self.hparams.adam_eps,
        )

        assert all([
            self.hparams.learning_rate_warm_up_step is not None,
            self.hparams.learning_rate_decay_end_step is not None,
            self.hparams.learning_rate_min_decay_factor is not None,
        ]), 'Learning rate related arguments should not be None when the model is used for training.'

        warm_up = LinearWarmUpDecay(
            learning_rate_warm_up_step=self.hparams.learning_rate_warm_up_step,
            learning_rate_decay_end_step=self.hparams.learning_rate_decay_end_step,
            learning_rate_min_decay_factor=self.hparams.learning_rate_min_decay_factor,
        )

        warmup_scheduler = {
            'scheduler': LambdaLR(optimizer=optimizer, lr_lambda=warm_up),
            'interval': 'step',
        }

        return [optimizer], [warmup_scheduler]

    def tie_word_embeddings(self):
        self.generation_head.prediction_linear.weight = self.embeddings.word_embeddings.weight
    
    @property
    def config(self):
        return self._huggingface_config
    
    def process_batch_for_generation(self, batch: Dict[str, torch.Tensor]):
        # Extract the first sample.
        one_sample_batch = {}
        for key, value in batch.items():
            one_sample_batch[key] = torch.cat([value[:1]] * self.hparams.max_latent_act_embeddings, dim=0)

        one_sample_batch['attention_mask'] = one_sample_batch['generation_attention_mask']
        latent_acts = torch.arange(0, self.hparams.max_latent_act_embeddings, device=one_sample_batch['input_ids'].device)
        one_sample_batch['latent_act'] = latent_acts.unsqueeze(-1)

        # Keep the original input_ids and labels.
        input_ids, labels = one_sample_batch['input_ids'], one_sample_batch['labels']
        input_ids = input_ids[:, 1:]

        one_sample_batch = process_batch_for_generation(copy.deepcopy(one_sample_batch))

        for key in ('input_ids', 'position_ids', 'turn_ids', 'role_ids'):
            one_sample_batch[key] = one_sample_batch[key][:, 1:]
        
        # from pprint import pprint
        # for key, value in one_sample_batch.items():
        #     print(key)
        #     pprint(value.tolist())

        return one_sample_batch, input_ids, labels
    
    @property
    def tokenizer(self):
        if getattr(self, '_tokenizer', None) is None:
            raise AttributeError
        return self._tokenizer
    
    @tokenizer.setter
    def tokenizer(self, tokenizer: Plato2FineGrainedGenerationTokenizer):
        self._tokenizer = tokenizer
        self._tokenizer_related_kwargs = {
            'bos_token_id': self.tokenizer.bos_token_id,
            'pad_token_id': self.tokenizer.pad_token_id,
            'eos_token_id': self.tokenizer.eos_token_id,
        }
    
    def generate_wrapper(self, dialogue_samples: Sequence[DialogueSample], **generate_kwargs):
        batch = self.tokenizer.make_batch_for_generataion(
            copy.deepcopy(dialogue_samples), 
            num_latent_acts=self.hparams.max_latent_act_embeddings
        )

        device = next(self.parameters()).device
        for key in batch:
            batch[key] = batch[key].to(device)

        generate_kwargs.update(batch)

        # generation_kwargs don't get overwritten just in case the user wants to 
        # specify special token ids that are different from the tokenizer's.
        generate_kwargs = dict(self._tokenizer_related_kwargs, **generate_kwargs)

        output = self.generate(
            **generate_kwargs,
        )

        responses = [self.tokenizer.decode(ids[batch['input_ids'].size(1):]) for ids in output] 
        responses = [
            responses[i : i + self.hparams.max_latent_act_embeddings]
            for i in range(0, len(responses), self.hparams.max_latent_act_embeddings)
        ] 

        return responses


class ResponseCoherenceEstimatorHead(nn.Module):
    def __init__(self, hidden_size: int):
        super().__init__()
        self.linear = nn.Linear(hidden_size, 1)

    def forward(self, hidden_state: torch.FloatTensor):
        logits = self.linear(hidden_state)
        return logits


class MaskedLanguageModelHead(nn.Module):
    def __init__(self, hidden_size: int, vocab_size: int):
        # TODO: Make adding the bias optional.
        # TODO: Make hidden_transform optional.
        super().__init__()
        self.hidden_transform_linear = nn.Linear(hidden_size, hidden_size)
        self.hidden_transform_activation = nn.functional.gelu
        self.hidden_transform_layer_norm = nn.LayerNorm(hidden_size)
        self.prediction_linear = nn.Linear(hidden_size, vocab_size)

    def forward(self, hidden_states):
        hidden_states = self.hidden_transform_linear(hidden_states)
        hidden_states = self.hidden_transform_activation(hidden_states)
        hidden_states = self.hidden_transform_layer_norm(hidden_states)
        hidden_states = self.prediction_linear(hidden_states)
        return hidden_states


class PlatoResponseCoherenceEstimation(pl.LightningModule):
    def __init__(
            self, 
            max_turn_embeddings: int,
            max_role_embeddings: int,
            pretrained_config_factory: PretrainedConfigFactoryForPlato2,
            learning_rate: Optional[float] = None,
            adam_eps: Optional[float] = 1.0e-8,
            learning_rate_decay_end_step: Optional[int] = None,
            learning_rate_warm_up_step: Optional[int] = None,
            learning_rate_min_decay_factor: Optional[float] = None,
        ):
        super().__init__()
        self.save_hyperparameters(
            'max_turn_embeddings',
            'max_role_embeddings',
            'learning_rate',
            'adam_eps',
            'learning_rate_decay_end_step',
            'learning_rate_warm_up_step',
            'learning_rate_min_decay_factor',
        )
        self._calculate_mlm_logits_in_forward = False

        self.transfomer_encoder = pretrained_config_factory.get_transformer_encoder()
        self.pretrained_config_factory = pretrained_config_factory

        self.embeddings = PlatoEmbeddings(
            vocab_size=pretrained_config_factory.get_vocab_size(),
            hidden_dim=pretrained_config_factory.get_hidden_dim(),
            pad_token_id=pretrained_config_factory.pad_token_id(),
            max_position_embeddings=pretrained_config_factory.get_max_position_embeddings(),
            hidden_dropout_prob=pretrained_config_factory.get_hidden_dropout_prob(),
            max_turn_embeddings=max_turn_embeddings,
            max_role_embeddings=max_role_embeddings,
        )

        self.masked_language_model_head = MaskedLanguageModelHead(
            hidden_size=pretrained_config_factory.get_hidden_dim(),
            vocab_size=pretrained_config_factory.get_vocab_size(),
        )

        self.response_coherence_estimator_head = ResponseCoherenceEstimatorHead(pretrained_config_factory.get_hidden_dim())

        self.tie_word_embeddings()

    def shared_step(
        self,
        mlm_batch: Dict[str, torch.Tensor],
        rce_batch: Dict[str, torch.Tensor],
        **kwargs,
    ):
        with self.switch_to_mlm_mode():
            mlm_logits = self(**mlm_batch)
        mlm_labels = mlm_batch['mlm_labels']

        mlm_loss = F.cross_entropy(
            mlm_logits.view(-1, self.pretrained_config_factory.get_vocab_size()), 
            mlm_labels.view(-1),
        )
        mlm_accuracy = calculate_accuracy(mlm_logits, mlm_labels)

        rce_logits = self(**rce_batch)
        rce_labels = rce_batch['rce_labels']
        rce_loss = F.binary_cross_entropy_with_logits(rce_logits.view(-1), rce_labels)

        positive_num_correct = torch.count_nonzero(torch.logical_and(rce_logits.view(-1) > 0, rce_labels > 0.5))
        positive_accuracy = positive_num_correct / torch.count_nonzero(rce_labels > 0.5)

        negative_num_correct = torch.count_nonzero(torch.logical_and(rce_logits.view(-1) <= 0, rce_labels < 0.5))
        negative_accuracy = negative_num_correct / torch.count_nonzero(rce_labels < 0.5)

        rce_accuracy = (positive_num_correct + negative_num_correct) / rce_labels.size(0)

        combined_loss = mlm_loss + rce_loss

        return {
            'mlm_logits': mlm_logits,
            'mlm_loss': mlm_loss,
            'mlm_accuracy': mlm_accuracy,
            'rce_logits': rce_logits,
            'rce_loss': rce_loss,
            'rce_positive_accuracy': positive_accuracy,
            'rce_negative_accuracy': negative_accuracy,
            'rce_accuracy': rce_accuracy,
            'combined_loss': combined_loss,
        }
    
    def forward(
        self,
        input_ids: torch.LongTensor,
        position_ids: torch.LongTensor, 
        turn_ids: torch.LongTensor, 
        role_ids: torch.LongTensor,
        attention_mask: torch.LongTensor,
        **kwargs,
    ):
        embeddings = self.embeddings(
            input_ids=input_ids,
            position_ids=position_ids,
            turn_ids=turn_ids, 
            role_ids=role_ids,
        )

        attention_mask = process_attention_mask(attention_mask)
        output = self.transfomer_encoder(
            hidden_states=embeddings,
            attention_mask=attention_mask,
        )
        hidden_states = output['last_hidden_state']

        if self._calculate_mlm_logits_in_forward:
            mlm_logits = self.masked_language_model_head(hidden_states=hidden_states)
            return mlm_logits
        else:
            rce_logits = self.response_coherence_estimator_head(hidden_states[:, 0])
            return rce_logits

    @contextlib.contextmanager
    def switch_to_mlm_mode(self):
        self._calculate_mlm_logits_in_forward = True
        yield None
        self._calculate_mlm_logits_in_forward = False

    def training_step(self, batch, batch_idx):
        # from pprint import pprint
        # for key, value in batch.items():
        #     print(key)
        #     pprint(value.tolist(), width=1000)

        output = self.shared_step(**batch)

        self.log('train_mlm_loss', output['mlm_loss'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_mlm_accuracy', output['mlm_accuracy'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_rce_positive_accuracy', output['rce_positive_accuracy'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_rce_negative_accuracy', output['rce_negative_accuracy'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_rce_accuracy', output['rce_accuracy'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_rce_loss', output['rce_loss'], prog_bar=False, logger=True, on_step=True, on_epoch=True)
        self.log('train_combined_loss', output['combined_loss'], prog_bar=False, logger=True, on_step=True, on_epoch=True)

        return output['combined_loss']
    
    def validation_step(self, batch, batch_idx):
        # from pprint import pprint
        # for key, value in batch.items():
        #     print(key)
        #     print(value.size())
        #     pprint(value.tolist(), width=1000)
        output = self.shared_step(**batch)

        self.log('val_mlm_loss', output['mlm_loss'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_mlm_accuracy', output['mlm_accuracy'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_rce_positive_accuracy', output['rce_positive_accuracy'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_rce_negative_accuracy', output['rce_negative_accuracy'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_rce_accuracy', output['rce_accuracy'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_rce_loss', output['rce_loss'], prog_bar=False, logger=True, on_step=False, on_epoch=True)
        self.log('val_combined_loss', output['combined_loss'], prog_bar=False, logger=True, on_step=False, on_epoch=True)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(
            self.parameters(), 
            lr=self.hparams.learning_rate,
            eps=self.hparams.adam_eps,
        )

        assert all([
            self.hparams.learning_rate_warm_up_step is not None,
            self.hparams.learning_rate_decay_end_step is not None,
            self.hparams.learning_rate_min_decay_factor is not None,
        ]), 'Learning rate related arguments should not be None when the model is used for training.'

        warm_up = LinearWarmUpDecay(
            learning_rate_warm_up_step=self.hparams.learning_rate_warm_up_step,
            learning_rate_decay_end_step=self.hparams.learning_rate_decay_end_step,
            learning_rate_min_decay_factor=self.hparams.learning_rate_min_decay_factor,
        )

        warmup_scheduler = {
            'scheduler': LambdaLR(optimizer=optimizer, lr_lambda=warm_up),
            'interval': 'step',
        }

        return [optimizer], [warmup_scheduler]

    def tie_word_embeddings(self):
        self.masked_language_model_head.prediction_linear.weight = self.embeddings.word_embeddings.weight

    @property
    def tokenizer(self):
        if getattr(self, '_tokenizer', None) is None:
            raise AttributeError
        return self._tokenizer
    
    @tokenizer.setter
    def tokenizer(self, tokenizer: Plato2ResponseCoherenceEstimationTokenizer):
        self._tokenizer = tokenizer

    @torch.no_grad()
    def rank_responses(self, contexts: Sequence[DialogueSample], responses_list: Sequence[str]):
        dialogue_candidates = []
        for context, responses in zip(contexts, responses_list):
            for response in responses:
                dialogue_sample_copy = copy.deepcopy(context)
                dialogue_sample_copy.response = response
                dialogue_candidates.append(dialogue_sample_copy)

        encoded_dialogue_candidates = [self.tokenizer.encode_dialogue(dialogue_candidate) for dialogue_candidate in dialogue_candidates]
        dialogue_candidates_batch = self.tokenizer.collate_fn(encoded_dialogue_candidates)

        device = next(self.parameters()).device
        for key in dialogue_candidates_batch:
            dialogue_candidates_batch[key] = dialogue_candidates_batch[key].to(device)

        logits = self(**dialogue_candidates_batch)
        probs_list = torch.sigmoid(logits).view(len(contexts), -1).tolist()
        logits_list = logits.view(len(contexts), -1).tolist()

        sorted_results_list = []
        for logits, probs, responses in zip(logits_list, probs_list, responses_list):
            sorted_results = []
            for logit, prob, response in sorted(zip(logits, probs, responses), key=lambda x: -x[0]):
                result = {
                    'logit': logit,
                    'probability': prob,
                    'response': response
                }
                sorted_results.append(result)
            sorted_results_list.append(sorted_results)
        
        return sorted_results_list


class PlatoForGeneration(nn.Module):
    def __init__(
        self,
        fine_grained_generation_checkpoint_path: str,
        response_coherence_estimation_checkpoint_path: str,
        pretrained_config_factory: PretrainedConfigFactoryForPlato2,
        fine_grained_tokenizer: Plato2FineGrainedGenerationTokenizer,
        response_coherence_tokenizer: Plato2ResponseCoherenceEstimationTokenizer,
    ):
        super().__init__()
        self.fine_grained_generation = PlatoFineGrainedGeneration.load_from_checkpoint(
            fine_grained_generation_checkpoint_path,
            pretrained_config_factory=pretrained_config_factory,
        )
        self.fine_grained_generation.tokenizer = fine_grained_tokenizer

        self.response_coherence_estimation = PlatoResponseCoherenceEstimation.load_from_checkpoint(
            response_coherence_estimation_checkpoint_path,
            pretrained_config_factory=pretrained_config_factory,
        )
        self.response_coherence_estimation.tokenizer = response_coherence_tokenizer

        self.eval()

    def generate_wrapper(
        self,
        contexts: Sequence[DialogueSample],
        max_length: int = 1024,
        max_new_tokens: int = 129,
        do_sample: bool = True,
        num_beams: int = 1,
        temperature: float = 1.0,
        top_k: int = 0,
        top_p: float = 0.5,
        bad_words_ids: List[List[int]] = [[]],
        **generate_kwargs,
    ) -> List[List[Dict[str, Any]]]:
        responses = self.fine_grained_generation.generate_wrapper(
            dialogue_samples=contexts,
            max_length=max_length,
            max_new_tokens=max_new_tokens,
            do_sample=do_sample,
            num_beams=num_beams,
            temperature=temperature,
            top_k=top_k,
            top_p=top_p,
            bad_words_ids=bad_words_ids,
            **generate_kwargs
        )
        sorted_results = self.response_coherence_estimation.rank_responses(contexts, responses)
        return sorted_results

    @classmethod
    def from_config_paths(
        cls,
        fine_grained_generation_checkpoint_path: str,
        response_coherence_estimation_checkpoint_path: str,
        pretrained_config_factory: PretrainedConfigFactoryForPlato2,
        base_tokenizer_factory: BaseTokenizerFactory,
        training_config_path_for_fine_grained: str,
        training_config_path_for_response_coherence_estimation: str,
    ):
        with open(training_config_path_for_fine_grained, 'r') as f:
            training_config_for_fine_grained = yaml.safe_load(f)
        
        tokenizer_init_args_for_fine_grained = training_config_for_fine_grained['data']['init_args']['tokenizer']['init_args']
        
        fine_grained_tokenizer = Plato2FineGrainedGenerationTokenizer(
            tokenizer_constructor=base_tokenizer_factory,
            eos_token_id=tokenizer_init_args_for_fine_grained['eos_token_id'],
            bos_token_id=tokenizer_init_args_for_fine_grained['bos_token_id'],
            pad_token_id=tokenizer_init_args_for_fine_grained['pad_token_id'],
            role_pad_token_id=tokenizer_init_args_for_fine_grained['role_pad_token_id'],
            turn_pad_token_id=tokenizer_init_args_for_fine_grained['turn_pad_token_id'],
            position_pad_token_id=tokenizer_init_args_for_fine_grained['position_pad_token_id'],
        )


        with open(training_config_path_for_response_coherence_estimation, 'r') as f:
            training_config_for_response_coherence_estimation = yaml.safe_load(f)
        
        tokenizer_init_args_for_response_coherence_estimation = training_config_for_response_coherence_estimation['data']['init_args']['tokenizer']['init_args']
        
        response_coherence_estimation_tokenizer = Plato2ResponseCoherenceEstimationTokenizer(
            tokenizer_constructor=base_tokenizer_factory,
            eos_token_id=tokenizer_init_args_for_response_coherence_estimation['eos_token_id'],
            bos_token_id=tokenizer_init_args_for_response_coherence_estimation['bos_token_id'],
            pad_token_id=tokenizer_init_args_for_response_coherence_estimation['pad_token_id'],
            role_pad_token_id=tokenizer_init_args_for_response_coherence_estimation['role_pad_token_id'],
            turn_pad_token_id=tokenizer_init_args_for_response_coherence_estimation['turn_pad_token_id'],
            position_pad_token_id=tokenizer_init_args_for_response_coherence_estimation['position_pad_token_id'],
            global_first_token_id=tokenizer_init_args_for_response_coherence_estimation['global_first_token_id'],
            response_replacement_prob=0.0
        )

        return cls(
            fine_grained_generation_checkpoint_path=fine_grained_generation_checkpoint_path,
            response_coherence_estimation_checkpoint_path=response_coherence_estimation_checkpoint_path,
            pretrained_config_factory=pretrained_config_factory,
            fine_grained_tokenizer=fine_grained_tokenizer,
            response_coherence_tokenizer=response_coherence_estimation_tokenizer
        )