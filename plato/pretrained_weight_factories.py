from transformers import (
    BertModel, 
    BertConfig,
    BertForMaskedLM,
)
import torch
from abc import ABC, abstractmethod


class PretrainedWeightFactoryForPlato2(ABC):

    @abstractmethod
    def get_transformer_encoder(self) -> torch.nn.Module:
        pass
    
    @abstractmethod
    def get_word_embeddings(self) -> torch.nn.Module:
        pass

    @abstractmethod
    def get_position_embeddings(self) -> torch.nn.Module:
        pass

    def get_mlm_hidden_transform_linear(self) -> torch.nn.Module:
        return None

    def get_mlm_hidden_transform_activation(self) -> torch.nn.Module:
        return None

    def get_mlm_hidden_transform_layer_norm(self) -> torch.nn.Module:
        return None

    def get_mlm_prediction_bias(self) -> torch.nn.Parameter:
        return None


class PretrainedBertModelWeightFactoryForPlato2(PretrainedWeightFactoryForPlato2):
    def __init__(self, pretrained_path: str):
        self.bert_model = BertModel.from_pretrained(pretrained_path)
    
    def get_transformer_encoder(self) -> torch.nn.Module:
        return self.bert_model.encoder

    def get_word_embeddings(self) -> int:
        return self.bert_model.embeddings.word_embeddings

    def get_position_embeddings(self) -> int:
        return self.bert_model.embeddings.position_embeddings


class PretrainedBertForMaskedLMWeightFactoryForPlato2(PretrainedWeightFactoryForPlato2):
    def __init__(self, pretrained_path: str):
        self.bert_for_masked_lm = BertForMaskedLM.from_pretrained(pretrained_path)
    
    def get_transformer_encoder(self) -> torch.nn.Module:
        return self.bert_for_masked_lm.bert.encoder

    def get_word_embeddings(self):
        return self.bert_for_masked_lm.bert.embeddings.word_embeddings

    def get_position_embeddings(self):
        return self.bert_for_masked_lm.bert.embeddings.position_embeddings

    def get_mlm_hidden_transform_linear(self):
        return self.bert_for_masked_lm.cls.predictions.transform.dense

    def get_mlm_hidden_transform_activation(self):
        return self.bert_for_masked_lm.cls.predictions.transform.transform_act_fn

    def get_mlm_hidden_transform_layer_norm(self):
        return self.bert_for_masked_lm.cls.predictions.transform.LayerNorm

    def get_mlm_prediction_bias(self):
        return self.bert_for_masked_lm.cls.predictions.decoder.bias