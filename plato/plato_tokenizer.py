import torch, random
from dialogue_dataset import (
    DialogueSample, 
    DialogueTokenizer
)
from typing import (
    List,
    Dict,
    Sequence,
)
from base_tokenizers import (
    BaseTokenizerFactory,
    BaseTokenizerInterface,
)
from dialogue_dataset.dialogue_tokenizer_mixins import (
    TokenEmbeddingIndexProcessingMixin,
    UtteranceLevelIndexProcessingMixin,
    UnifiedAttentionTokenIndexProcessingMixin,
    DictionaryCollateFnMixin,
    MaskedLanguageModelTokenProcessingMixin,
    BaseTokenizerCompositionMixin,
) 
from utils.tokenizer_utils import pad_left


class PlatoBaseTokenizer(
    BaseTokenizerCompositionMixin,
    TokenEmbeddingIndexProcessingMixin,
    UtteranceLevelIndexProcessingMixin,
):
    def __init__(
        self, 
        tokenizer_constructor: BaseTokenizerFactory, 
        eos_token_id: int,
        bos_token_id: int,
        pad_token_id: int,
        role_pad_token_id: int,
        turn_pad_token_id: int,
        position_pad_token_id: int,
        global_first_token_id: int = None,
    ):
        self.tokenizer: BaseTokenizerInterface = tokenizer_constructor.construct_tokenizer()

        # TODO: Get token strings not their ids.
        self.global_first_token_id = global_first_token_id

        self.eos_token_id = eos_token_id
        self.bos_token_id = bos_token_id
        self.pad_token_id = pad_token_id

        self.role_pad_token_id = role_pad_token_id
        self.turn_pad_token_id = turn_pad_token_id
        self.position_pad_token_id = position_pad_token_id

    def encode_dialogue(self, dialogue: DialogueSample):
        if not dialogue.is_encoded():
            dialogue.encode_with(self.tokenizer)

        context_input_ids_list = self.make_context_input_ids_list(
            context_input_ids_list=dialogue.context,
            context_bos_token_id=None,
            context_eos_token_id=self.eos_token_id,
        )
        response_input_ids = self.make_response_input_ids(
            response_input_ids=dialogue.response,
            response_bos_token_id=self.bos_token_id,
            response_eos_token_id=self.eos_token_id,
        )

        first_system_token_id_list = [torch.tensor([self.global_first_token_id])] if self.global_first_token_id is not None else [] 
        input_ids_list = first_system_token_id_list + context_input_ids_list
        input_ids = torch.cat(input_ids_list)

        position_ids = self.make_position_ids(
            input_ids_list=context_input_ids_list,
            position_global_first_token_id=self.position_pad_token_id if self.global_first_token_id is not None else None,
        )
        decoder_position_ids = self.make_position_ids(
            input_ids_list=[response_input_ids], 
            position_global_first_token_id=None,
        )

        role_ids = self.make_role_ids(
            input_ids_list=context_input_ids_list, 
            role_id_list=dialogue.context_speakers,
            role_global_first_token_id=self.role_pad_token_id if self.global_first_token_id is not None else None,
        )
        decoder_role_ids = self.make_role_ids(
            input_ids_list=[response_input_ids], 
            role_id_list=[dialogue.response_speaker],
            role_global_first_token_id=None,
        )

        turn_ids = self.make_turn_ids(
            input_ids_list=context_input_ids_list, 
            turn_global_first_token_id=self.turn_pad_token_id if self.global_first_token_id is not None else None,
            lowest_id=1,
        )
        decoder_turn_ids = self.make_turn_ids(
            input_ids_list=[response_input_ids],
            turn_global_first_token_id=None,
            lowest_id=0,
        )
        
        return {
            'input_ids': input_ids,
            'role_ids': role_ids,
            'turn_ids': turn_ids,
            'position_ids': position_ids,
            'decoder_input_ids': response_input_ids,
            'decoder_role_ids': decoder_role_ids,
            'decoder_turn_ids': decoder_turn_ids,
            'decoder_position_ids': decoder_position_ids,
        }


class Plato2CoarseGrainedGenerationTokenizer(
    PlatoBaseTokenizer,
    UnifiedAttentionTokenIndexProcessingMixin,
    DictionaryCollateFnMixin,
    DialogueTokenizer,
):
    def __init__(
        self, 
        tokenizer_constructor: BaseTokenizerFactory, 
        eos_token_id: int,
        bos_token_id: int,
        pad_token_id: int,
        role_pad_token_id: int,
        turn_pad_token_id: int,
        position_pad_token_id: int,
        global_first_token_id: int = None,
    ):
        super().__init__(
            tokenizer_constructor=tokenizer_constructor,
            eos_token_id=eos_token_id,
            bos_token_id=bos_token_id,
            pad_token_id=pad_token_id,
            role_pad_token_id=role_pad_token_id,
            turn_pad_token_id=turn_pad_token_id,
            position_pad_token_id=position_pad_token_id,
            global_first_token_id=global_first_token_id,
        )
        self.zero_dim_keys = (
            'bos_token_index',
        )
        self.one_dim_key_pad_pairs = (
            ('input_ids', self.pad_token_id),
            ('role_ids', self.role_pad_token_id),
            ('turn_ids', self.turn_pad_token_id),
            ('position_ids', self.position_pad_token_id),
            ('labels', -100),
        )
        self.two_dim_key_pad_pairs = (
            ('attention_mask', 0),
        )
        self.final_key_concatenation_keys_pairs = (
            ('input_ids', ('input_ids', 'decoder_input_ids')),
            ('role_ids', ('role_ids', 'decoder_role_ids')),
            ('turn_ids', ('turn_ids', 'decoder_turn_ids')),
            ('position_ids', ('position_ids', 'decoder_position_ids')),
        )

    def encode_dialogue(self, dialogue: DialogueSample):
        # Encode the dialogue sample in plato format.
        tokenizer_output = super().encode_dialogue(dialogue)

        # Get the index from which the response starts (bos token).
        bos_token_index = torch.tensor(tokenizer_output['input_ids'].size(0), dtype=torch.long)

        # Concatenate encoder, decoder inputs.
        tokenizer_output = self.concatenate_inputs(tokenizer_output)

        # Add labels.
        tokenizer_output['labels'] = self.make_generation_labels(
            input_ids=tokenizer_output['input_ids'],
            bos_token_index=bos_token_index,
        )

        # Add attention mask.
        tokenizer_output['attention_mask'] = self.make_seq2seq_mask(
            sequence_length=tokenizer_output['input_ids'].size(0),
            bos_token_index=bos_token_index,
        )

        # Add bos token index
        tokenizer_output['bos_token_index'] = bos_token_index

        return tokenizer_output
    
    def collate_fn(self, batch: List[Dict]):
        batch = self.pad_sequences(batch)
        return batch


class Plato2FineGrainedGenerationTokenizer(
    PlatoBaseTokenizer,
    UnifiedAttentionTokenIndexProcessingMixin,
    DictionaryCollateFnMixin,
    DialogueTokenizer,
):
    def __init__(
        self, 
        tokenizer_constructor: BaseTokenizerFactory, 
        eos_token_id: int,
        bos_token_id: int,
        pad_token_id: int,
        role_pad_token_id: int,
        turn_pad_token_id: int,
        position_pad_token_id: int,
        global_first_token_id: int = None,
    ):
        super().__init__(
            tokenizer_constructor=tokenizer_constructor,
            eos_token_id=eos_token_id,
            bos_token_id=bos_token_id,
            pad_token_id=pad_token_id,
            role_pad_token_id=role_pad_token_id,
            turn_pad_token_id=turn_pad_token_id,
            position_pad_token_id=position_pad_token_id,
            global_first_token_id=global_first_token_id,
        )
        self.zero_dim_keys = (
            'bos_token_index',
        )
        self.one_dim_key_pad_pairs = (
            ('input_ids', self.pad_token_id),
            ('role_ids', self.role_pad_token_id),
            ('turn_ids', self.turn_pad_token_id),
            ('position_ids', self.position_pad_token_id),
            ('labels', -100),
        )
        self.two_dim_key_pad_pairs = (
            ('generation_attention_mask', 0),
            ('latent_act_attention_mask', 0),
        )
        self.final_key_concatenation_keys_pairs = (
            ('input_ids', ('input_ids', 'decoder_input_ids')),
            ('role_ids', ('role_ids', 'decoder_role_ids')),
            ('turn_ids', ('turn_ids', 'decoder_turn_ids')),
            ('position_ids', ('position_ids', 'decoder_position_ids')),
        )

    def encode_dialogue(self, dialogue: DialogueSample):
        # Encode the dialogue sample in plato format.
        tokenizer_output = super().encode_dialogue(dialogue)

        # Get the index from which the response starts (bos token).
        bos_token_index = torch.tensor(tokenizer_output['input_ids'].size(0), dtype=torch.long)

        # Concatenate encoder, decoder inputs.
        tokenizer_output = self.concatenate_inputs(tokenizer_output)

        # Add labels.
        tokenizer_output['labels'] = self.make_generation_labels(
            input_ids=tokenizer_output['input_ids'],
            bos_token_index=bos_token_index,
        )

        tokenizer_output['generation_attention_mask'] = self.make_seq2seq_mask(
            sequence_length=tokenizer_output['input_ids'].size(0),
            bos_token_index=bos_token_index,
        )

        tokenizer_output['latent_act_attention_mask'] = self.make_bidirectional_mask(
            sequence_length=tokenizer_output['input_ids'].size(0),
        )

        # Add bos token index
        tokenizer_output['bos_token_index'] = bos_token_index

        return tokenizer_output

    def collate_fn(self, batch: List[Dict]):
        batch = self.pad_sequences(batch)
        return batch

    def make_batch_for_generataion(self, dialogue_samples: Sequence[DialogueSample], num_latent_acts: int):
        encoded_dialogues = [self.encode_dialogue_for_generation(dialogue_sample) for dialogue_sample in dialogue_samples]

        batch = {}
        for key in encoded_dialogues[0].keys():
            batch[key] = [encoded_dialogue[key] for encoded_dialogue in encoded_dialogues]

        batch = self.collate_fn_for_generation(batch)

        for key, value in batch.items():
            batch[key] = torch.repeat_interleave(value, num_latent_acts, 0)

        batch['latent_act'] = torch.arange(num_latent_acts).repeat(len(encoded_dialogues)).unsqueeze(1)

        return batch
    
    def encode_dialogue_for_generation(self, dialogue_sample: DialogueSample):
        encoded_dialogue = super().encode_dialogue(dialogue_sample)
        bos_token_index = torch.tensor(encoded_dialogue['input_ids'].size(0), dtype=torch.long)

        encoded_dialogue = self.concatenate_inputs(encoded_dialogue)
        encoded_dialogue['attention_mask'] = self.make_seq2seq_mask(encoded_dialogue['input_ids'].size(0), bos_token_index)

        return encoded_dialogue

    def collate_fn_for_generation(self, samples_dict: Dict[str, torch.Tensor]):
        for key, samples in samples_dict.items():
            if samples[0].dim() == 0:
                samples_dict[key] = torch.cat(samples)

            if samples[0].dim() == 1:
                # TODO: zero may not be the actual padding id, but they will get masked anyway.
                samples_dict[key] = pad_left(samples, 0)

            # TODO: I think it could be generalized to higher dimensions, but I don't really care. 
            if samples[0].dim() == 2:
                # Add one to account for the latent act embedding.
                first_dim_max = max([sample.size(0) for sample in samples]) + 1
                second_dim_max = max([sample.size(1) for sample in samples]) + 1

                # TODO: There is a chance that zero is not the value that indicates masked state.
                padded_batch = torch.full(size=[len(samples), first_dim_max, second_dim_max], fill_value=0)
                for batch_idx, sample in enumerate(samples):
                    padded_batch[batch_idx, first_dim_max - sample.size(0):, second_dim_max - sample.size(1):] = sample
                    # Let the latent act attend the context.
                    padded_batch[batch_idx, 0, second_dim_max - sample.size(1):-1] = 1
                # Let the context, respones pairs attend the corresponding latent acts.
                padded_batch[:, :, 0] = 1
                samples_dict[key] = padded_batch
        return samples_dict


class Plato2ResponseCoherenceEstimationTokenizer(
    PlatoBaseTokenizer,
    DictionaryCollateFnMixin,
    DialogueTokenizer,
):
    def __init__(
        self, 
        tokenizer_constructor: BaseTokenizerFactory, 
        eos_token_id: int,
        bos_token_id: int,
        pad_token_id: int,
        role_pad_token_id: int,
        turn_pad_token_id: int,
        position_pad_token_id: int,
        global_first_token_id: int = None,
        response_replacement_prob: float = 0.5,
    ):
        super().__init__(
            tokenizer_constructor=tokenizer_constructor,
            eos_token_id=eos_token_id,
            bos_token_id=bos_token_id,
            pad_token_id=pad_token_id,
            role_pad_token_id=role_pad_token_id,
            turn_pad_token_id=turn_pad_token_id,
            position_pad_token_id=position_pad_token_id,
            global_first_token_id=global_first_token_id,
        )
        self.response_replacement_prob = response_replacement_prob

        self.zero_dim_keys = (
            'rce_labels',
        )
        self.one_dim_key_pad_pairs = (
            ('input_ids', self.pad_token_id),
            ('role_ids', self.role_pad_token_id),
            ('turn_ids', self.turn_pad_token_id),
            ('position_ids', self.position_pad_token_id),
        )
        self.two_dim_key_pad_pairs = (
            ('attention_mask', 0),
        )
        self.final_key_concatenation_keys_pairs = (
            ('input_ids', ('input_ids', 'decoder_input_ids')),
            ('role_ids', ('role_ids', 'decoder_role_ids')),
            ('turn_ids', ('turn_ids', 'decoder_turn_ids')),
            ('position_ids', ('position_ids', 'decoder_position_ids')),
        )
    
    def collate_fn(self, batch: List[Dict]):
        self._shuffle_reponses_and_add_rce_labels(batch)
        self._add_masks(batch)
        self._concatenate_inputs(batch)
        batch = self.pad_sequences(batch)
        return batch
    
    def _shuffle_reponses_and_add_rce_labels(self, batch: List[Dict]):
        # Shuffling from the batch might not be the greatest idea in the world, but the other ways seem too daunting.
        for sample in batch:
            # This randomness handles well the case where the batch size is an odd number.
            if random.random() < self.response_replacement_prob:
                input_ids = sample['input_ids']
                # Filter out duplicate contexts such as the empty context. This might seem inefficient when there is no empty context.
                random_samples_list = [sample for sample in batch if not torch.equal(input_ids, sample['input_ids'])]

                # The probability of this event happening is fairly low unless the batch size is really small, so no calibration is performed.
                if len(random_samples_list) == 0:
                    sample['rce_labels'] = torch.tensor(1, dtype=torch.float)
                else:
                    random_sample = random.choice(random_samples_list)
                    sample['decoder_input_ids'] = random_sample['decoder_input_ids']
                    sample['decoder_position_ids'] = random_sample['decoder_position_ids']
                    sample['decoder_role_ids'] = torch.full_like(random_sample['decoder_role_ids'], sample['decoder_role_ids'][0])
                    sample['decoder_turn_ids'] = torch.full_like(random_sample['decoder_turn_ids'], sample['decoder_turn_ids'][0])
                    sample['rce_labels'] = torch.tensor(0, dtype=torch.float)
            else:
                sample['rce_labels'] = torch.tensor(1, dtype=torch.float)

    def _add_masks(self, batch):
        for sample in batch:
            sample['attention_mask'] = torch.ones([sample['input_ids'].size(0) + sample['decoder_input_ids'].size(0)] * 2, dtype=torch.long)
    
    def _concatenate_inputs(self, batch):
        for sample in batch:
            self.concatenate_inputs(sample)


class Plato2MaskedLanguageModelingTokenizer(
    PlatoBaseTokenizer,
    MaskedLanguageModelTokenProcessingMixin,
    DictionaryCollateFnMixin,
    DialogueTokenizer,
):
    def __init__(
        self, 
        tokenizer_constructor: BaseTokenizerFactory, 
        eos_token_id: int,
        bos_token_id: int,
        pad_token_id: int,
        mask_token_id: int,
        role_pad_token_id: int,
        turn_pad_token_id: int,
        position_pad_token_id: int,
        masking_rate: float,
        global_first_token_id: int = None,
    ):
        super().__init__(
            tokenizer_constructor=tokenizer_constructor,
            eos_token_id=eos_token_id,
            bos_token_id=bos_token_id,
            pad_token_id=pad_token_id,
            role_pad_token_id=role_pad_token_id,
            turn_pad_token_id=turn_pad_token_id,
            position_pad_token_id=position_pad_token_id,
            global_first_token_id=global_first_token_id,
        )
        self.mask_token_id = mask_token_id
        self.masking_rate = masking_rate

        self.one_dim_key_pad_pairs = (
            ('input_ids', self.pad_token_id),
            ('role_ids', self.role_pad_token_id),
            ('turn_ids', self.turn_pad_token_id),
            ('position_ids', self.position_pad_token_id),
            ('mlm_labels', -100),
        )
        self.two_dim_key_pad_pairs = (
            ('attention_mask', 0),
        )
        self.final_key_concatenation_keys_pairs = (
            ('input_ids', ('input_ids', 'decoder_input_ids')),
            ('role_ids', ('role_ids', 'decoder_role_ids')),
            ('turn_ids', ('turn_ids', 'decoder_turn_ids')),
            ('position_ids', ('position_ids', 'decoder_position_ids')),
            ('mlm_labels', ('mlm_labels', 'decoder_mlm_labels')),
        )

    def encode_dialogue(self, dialogue: DialogueSample):
        if not dialogue.is_encoded():
            dialogue.encode_with(self.tokenizer)

        masked_dialogue = self.mask_dialogue_token_ids(dialogue)
        masked_tokenizer_output = super().encode_dialogue(masked_dialogue)
        # TODO: There is some redundant computation involved here.
        unmasked_tokenizer_output = super().encode_dialogue(dialogue)


        encoder_mlm_labels = self.make_mlm_labels(
            masked_input_ids=masked_tokenizer_output['input_ids'],
            original_input_ids=unmasked_tokenizer_output['input_ids'],
        )
        masked_tokenizer_output['mlm_labels'] = encoder_mlm_labels

        decoder_mlm_labels = self.make_mlm_labels(
            masked_input_ids=masked_tokenizer_output['decoder_input_ids'],
            original_input_ids=unmasked_tokenizer_output['decoder_input_ids'],
        )
        masked_tokenizer_output['decoder_mlm_labels'] = decoder_mlm_labels

        masked_dialogue = self.concatenate_inputs(masked_tokenizer_output)
        masked_dialogue['attention_mask'] = torch.ones([masked_dialogue['input_ids'].size(0)] * 2, dtype=torch.long)

        return masked_tokenizer_output

    def collate_fn(self, batch: List[Dict]):
        batch = self.pad_sequences(batch)
        return batch


class Plato2_Stage_2_2_Tokenizer(
    BaseTokenizerCompositionMixin,
    DialogueTokenizer,
):
    def __init__(
        self, 
        tokenizer_constructor: BaseTokenizerFactory, 
        eos_token_id: int,
        bos_token_id: int,
        pad_token_id: int,
        mask_token_id: int,
        role_pad_token_id: int,
        turn_pad_token_id: int,
        position_pad_token_id: int,
        masking_rate: float,
        global_first_token_id: int = None,
        response_replacement_prob: float = 0.5,
    ):
        super().__init__()
        self.tokenizer: BaseTokenizerInterface = tokenizer_constructor.construct_tokenizer()
        self.response_coherence_estimation_tokenizer = Plato2ResponseCoherenceEstimationTokenizer(
            tokenizer_constructor=tokenizer_constructor,
            eos_token_id=eos_token_id,
            bos_token_id=bos_token_id,
            pad_token_id=pad_token_id,
            role_pad_token_id=role_pad_token_id,
            turn_pad_token_id=turn_pad_token_id,
            position_pad_token_id=position_pad_token_id,
            global_first_token_id=global_first_token_id,
            response_replacement_prob=response_replacement_prob,
        )

        self.masked_language_modeling_tokenizer = Plato2MaskedLanguageModelingTokenizer(
            tokenizer_constructor=tokenizer_constructor,
            eos_token_id=eos_token_id,
            bos_token_id=bos_token_id,
            pad_token_id=pad_token_id,
            role_pad_token_id=role_pad_token_id,
            turn_pad_token_id=turn_pad_token_id,
            position_pad_token_id=position_pad_token_id,
            global_first_token_id=global_first_token_id,
            mask_token_id=mask_token_id,
            masking_rate=masking_rate,
        )

    def encode_dialogue(self, dialogue: DialogueSample):
        mlm_tokenizer_output = self.masked_language_modeling_tokenizer.encode_dialogue(dialogue)
        rce_tokenizer_output = self.response_coherence_estimation_tokenizer.encode_dialogue(dialogue)

        return {
            'mlm_tokenizer_output': mlm_tokenizer_output,
            'rce_tokenizer_output': rce_tokenizer_output,
        }
    
    def collate_fn(self, batch: List[Dict]):
        mlm_batch = self.masked_language_modeling_tokenizer.collate_fn([sample['mlm_tokenizer_output'] for sample in batch])
        rce_batch = self.response_coherence_estimation_tokenizer.collate_fn([sample['rce_tokenizer_output'] for sample in batch])

        return {
            'mlm_batch': mlm_batch,
            'rce_batch': rce_batch,
        }