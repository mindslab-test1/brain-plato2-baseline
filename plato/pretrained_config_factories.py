from transformers import (
    BertModel, 
    BertConfig,
)
import torch
from abc import ABC, abstractmethod
from .cacheable_bert_encoder import CacheableBertEncoder

class PretrainedConfigFactoryForPlato2(ABC):

    @abstractmethod
    def get_transformer_encoder(self) -> torch.nn.Module:
        pass

    @abstractmethod
    def get_vocab_size(self) -> int:
        pass

    @abstractmethod
    def get_hidden_dim(self) -> int:
        pass
    
    @abstractmethod
    def pad_token_id(self) -> int:
        pass

    @abstractmethod
    def get_max_position_embeddings(self) -> int:
        pass
    
    @abstractmethod
    def get_hidden_dropout_prob(self) -> float:
        pass


class PretrainedBertConfigFactoryForPlato2(PretrainedConfigFactoryForPlato2):
    def __init__(self, config_file_path: str):
        self.bert_config = BertConfig.from_pretrained(config_file_path)
        self.bert_config.is_decoder = True
    
    def get_transformer_encoder(self) -> CacheableBertEncoder:
        return CacheableBertEncoder(self.bert_config)

    def get_vocab_size(self) -> int:
        return self.bert_config.vocab_size

    def get_hidden_dim(self) -> int:
        return self.bert_config.hidden_size
    
    def pad_token_id(self) -> int:
        return self.bert_config.pad_token_id

    def get_max_position_embeddings(self) -> int:
        return self.bert_config.max_position_embeddings
    
    def get_hidden_dropout_prob(self) -> float:
        return self.bert_config.hidden_dropout_prob