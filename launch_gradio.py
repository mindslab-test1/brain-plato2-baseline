import gradio as gr
from jsonargparse import ArgumentParser, ActionConfigFile
from plato.plato_model import PlatoForGeneration
from dialogue_dataset.dataclasses import DialogueSample
import itertools
from typing import List, Tuple, Dict, Any
import logging, pprint
from logging.handlers import RotatingFileHandler


class PlatoGradio:
    def __init__(
        self, 
        plato_for_generation: PlatoForGeneration, 
        generate_kwargs: dict, 
        generation_device: str,
        estimation_device: str,
        max_num_turns: int,
    ):
        self.plato_for_generation = plato_for_generation
        self.generate_kwargs = generate_kwargs
        self.generation_device = generation_device
        self.estimation_device = estimation_device
        self.plato_for_generation.eval()
        self.plato_for_generation.fine_grained_generation.to(self.generation_device)
        self.plato_for_generation.response_coherence_estimation.to(self.estimation_device)
        self.max_num_turns = max_num_turns

    def chat(self, message: str) -> str:
        message_response_pairs = gr.get_state() or []
        candidate_div = ''

        if not len(message_response_pairs) >= self.max_num_turns:
            context = self._construct_context(message_response_pairs, message)

            responses = self.plato_for_generation.generate_wrapper(
                contexts=[context],
                **self.generate_kwargs,
            )[0]
            response = responses[0]['response']

            message_response_pairs.append((message, response))
            gr.set_state(message_response_pairs)

            candidate_div = self._construct_candidate_div(responses)

            log_str = pprint.pformat(
                {'context': context, 'responses': responses, 'message_response_pairs': message_response_pairs}
            )
            logging.info(log_str)

        chat_div = self._construct_chat_div(message_response_pairs)

        return chat_div + candidate_div
    
    def _construct_context(self, message_response_pairs: List[Tuple[str, str]], message: str) -> DialogueSample:
        context = list(itertools.chain.from_iterable(message_response_pairs))
        context.append(message)

        context_speakers = [0, 1] * len(message_response_pairs)
        context_speakers.append(0)
        
        context = DialogueSample(
            response_speaker=1,
            response=None,
            context=context,
            context_speakers=context_speakers,
        )
        return context
    
    def _construct_candidate_div(self, responses: List[Dict[str, Any]]) -> str:
        # TODO: Extending the string this way may not be the most efficient way to go, but I really don't care.
        candidate_div = "<div class='chatbox'><hr>"
        for response in responses:
            cand_items = []
            for key, value in response.items():
                cand_items.append(f'{key}: {value}')
            cand_message = '<br>'.join(cand_items)
            candidate_div += f"<div class='cand_msg'>{cand_message}</div>"
        candidate_div += "</div>"
        return candidate_div
    
    def _construct_chat_div(self, message_response_pairs: List[Tuple[str, str]]) -> str:
        # TODO: Extending the string this way may not be the most efficient way to go, but I really don't care.
        chat_div = "<div class='chatbox'>"
        for user_msg, resp_msg in message_response_pairs:
            chat_div += f"<div class='user_msg'>{user_msg}</div>"
            chat_div += f"<div class='resp_msg'>{resp_msg}</div>"
        chat_div += "</div>"
        return chat_div


def parse_args():
    parser = ArgumentParser()
    parser.add_subclass_arguments(PlatoGradio, 'plato_gradio')
    parser.add_argument('--server_port', type=int, required=True)
    parser.add_argument('--server_name', required=True)
    parser.add_argument('--flagging_dir', default='gradio_logs/')
    parser.add_argument('--config', action=ActionConfigFile)

    parser.add_argument('--logging_file_path', default='gradio_logs/gradio_logs.log')
    parser.add_argument('--logging_level', default='INFO')
    parser.add_argument('--logging_max_bytes', type=int, default=1000000)
    parser.add_argument('--logging_backup_count', type=int, default=10)

    args = parser.parse_args()
    classes = parser.instantiate_subclasses(args)

    return args, classes

def set_logger(logging_file_path, logging_level, max_bytes=1073741824, backup_count=10):
    logging.basicConfig(
        handlers=[RotatingFileHandler(logging_file_path, maxBytes=max_bytes, backupCount=backup_count, mode='a')],
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=getattr(logging, logging_level),
    )

if __name__ == "__main__":
    args, classes = parse_args()

    set_logger(
        logging_file_path=args.logging_file_path,
        logging_level=args.logging_level,
        max_bytes=args.logging_max_bytes,
        backup_count=args.logging_backup_count,
    )

    iface = gr.Interface(
        fn=classes['plato_gradio'].chat, 
        inputs="text", 
        outputs="html", 
        css="""
            .chatbox {display:flex;flex-direction:column}
            .user_msg, .resp_msg, .cand_msg {padding:4px;margin:4px 0px;margin-top:4px;border-radius:4px;width:80%}
            .user_msg {background-color:cornflowerblue;color:white;align-self:start}
            .resp_msg {background-color:lightgray;align-self:self-end}
            .cand_msg {background-color:SlateGrey;align-self:self-end;color:WhiteSmoke}
        """, 
        allow_screenshot=True, 
        allow_flagging=True,
        flagging_dir=args.flagging_dir,
        server_port=args.server_port,
        server_name=args.server_name,
        verbose=True,
        title='TURING PLATO-2 Baseline Model',
        description='Chat with the model.'
    )

    iface.launch()