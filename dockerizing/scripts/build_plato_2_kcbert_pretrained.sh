#!/bin/bash

# Set env vars.
export $(grep -v "^#" .env | xargs)

docker build \
    -t "${IMAGE_NAME}:artifacts-plato_2_kcbert_pretrained-v1" \
    -f dockerizing/dockerfiles/Dockerfile_plato_2_kcbert_pretrained \
    .
