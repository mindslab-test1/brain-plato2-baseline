#!/bin/bash

# Set env vars.
export $(grep -v "^#" .env | xargs)

REPO_VERSION="$(git tag --points-at HEAD)"
REMOTE_URL="$(git config --get remote.origin.url)"
MODEL_TAG="plato_2_kcbert_pretrained-v1"

docker build \
    --build-arg artifacts_image="${IMAGE_NAME}:artifacts-${MODEL_TAG}" \
    -t "${IMAGE_NAME}:${REPO_VERSION}-${MODEL_TAG}-packed" \
    -f dockerizing/dockerfiles/Dockerfile_packed \
    "${REMOTE_URL}#${REPO_VERSION}"
