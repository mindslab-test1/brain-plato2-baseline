#!/bin/bash

# Set env vars.
export $(grep -v "^#" .env | xargs)

REPO_VERSION="$(git tag --points-at HEAD)"
REMOTE_URL="$(git config --get remote.origin.url)"

docker build \
    -t "${IMAGE_NAME}:${REPO_VERSION}" \
    -f dockerizing/dockerfiles/Dockerfile \
     "${REMOTE_URL}#${REPO_VERSION}"
