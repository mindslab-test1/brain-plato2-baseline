from plato.plato_model import PlatoForGeneration
from base_tokenizers.base_tokenizer_factories import BaseTokenizerFactory
from dialogue_dataset.dataclasses import DialogueSample
from plato import PretrainedBertConfigFactoryForPlato2
from pprint import pprint
from pytorch_lightning import seed_everything
from plato import PretrainedBertConfigFactoryForPlato2
from jsonargparse import ArgumentParser, ActionConfigFile

def parse_args():
    parser = ArgumentParser()
    parser.add_argument(
        '--fine_grained_generation_checkpoint_path',
        default='checkpoints/stage_2_1/checkpoints/plato_stage_2_1.ckpt',
    )
    parser.add_argument(
        '--response_coherence_estimation_checkpoint_path',
        default='checkpoints/stage_2_2/checkpoints/plato_stage_2_2.ckpt'
    )
    parser.add_argument(
        '--pretrained_config_factory_config_file_path',
        default='external_pretrained/kcbert/turing_model_large/config.json'
    )
    parser.add_argument(
        '--training_config_path_for_fine_grained',
        default='checkpoints/stage_2_1/config.yaml'
    )
    parser.add_argument(
        '--training_config_path_for_response_coherence_estimation',
        default='checkpoints/stage_2_2/config.yaml'
    )
    parser.add_subclass_arguments(BaseTokenizerFactory, 'base_tokenizer_factory')
    parser.add_argument('--config', action=ActionConfigFile)

    args = parser.parse_args()
    classes = parser.instantiate_subclasses(args)
    return args, classes

if __name__ == '__main__':
    args, classes = parse_args()

    model: PlatoForGeneration = PlatoForGeneration.from_config_paths(
        fine_grained_generation_checkpoint_path=args.fine_grained_generation_checkpoint_path,
        response_coherence_estimation_checkpoint_path=args.response_coherence_estimation_checkpoint_path,
        pretrained_config_factory=PretrainedBertConfigFactoryForPlato2(
            config_file_path=args.pretrained_config_factory_config_file_path
        ),
        base_tokenizer_factory=classes['base_tokenizer_factory'],
        training_config_path_for_fine_grained=args.training_config_path_for_fine_grained,
        training_config_path_for_response_coherence_estimation=args.training_config_path_for_response_coherence_estimation,
    )

    # Move the fine-grained generation model to the first GPU.
    model.fine_grained_generation.to('cuda:0')
    # Move the response coherence estimation model to CPU.
    model.response_coherence_estimation.to('cpu')

    dialogue_samples = [
        DialogueSample(
            response_speaker=1,
            response=None,
            context=[
                '내일 정말 좋은 일이 있을 것 같아.',
                '무슨 일이 있을 것 같은데?',
                '모르겠어. 그냥 좋은 일이 있을 것 같은 느낌이 들어.'
            ],
            context_speakers=[
                0, 1, 0,
            ]
        ),
    ]

    # Make the generation deterministic (optional).
    seed_everything(1)

    output = model.generate_wrapper(
        contexts=dialogue_samples,
        max_length=1024,
        max_new_tokens=129,
        do_sample=True,
        num_beams=1,
        temperature=1.0,
        top_k=0,
        top_p=0.5,
        bad_words_ids=[[30001]],
    )

    print('Output:')
    pprint(output)
    print(f'Selected response: {output[0][0]["response"]}')