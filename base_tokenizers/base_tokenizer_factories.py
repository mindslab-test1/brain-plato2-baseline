import importlib
from abc import ABC, abstractmethod
import sentencepiece as spm
from .base_tokenizers import (
    HuggingFaceTokenizerWrapper,
    SentencePieceWrapper,
)

class BaseTokenizerFactory(ABC):
    @abstractmethod
    def construct_tokenizer(self):
        pass

class HuggingFaceTokenizerWrapperFactory(BaseTokenizerFactory):
    """
    """
    def __init__(
        self, 
        tokenizer_class_path: str, 
        tokenizer_directory: str,
    ):
        self.tokenizer_class_path = tokenizer_class_path
        self.tokenizer_directory = tokenizer_directory
    
    def construct_tokenizer(self):
        module_path, class_name = self.tokenizer_class_path.rsplit('.', 1)
        tokenizer_module = importlib.import_module(module_path)
        tokenizer = getattr(tokenizer_module, class_name).from_pretrained(self.tokenizer_directory)
        tokenizer = HuggingFaceTokenizerWrapper(tokenizer)

        return tokenizer

class SentencePieceWrapperFactory(BaseTokenizerFactory):
    """
    """
    def __init__(
        self, 
        sp_model_file_path: str,
    ):
        self.sp_model_file_path = sp_model_file_path
    
    def construct_tokenizer(self):
        sp_model = spm.SentencePieceProcessor(self.sp_model_file_path)
        tokenizer = SentencePieceWrapper(sp_model)
        return tokenizer
