from abc import (
    ABC, 
    abstractmethod,
)
from typing import (
    Union,
    List,
)
import torch
from transformers.tokenization_utils_base import PreTrainedTokenizerBase
import sentencepiece as spm


class BaseTokenizerInterface(ABC):
    """Defines basic tokenizer interface.
    """

    @abstractmethod
    def encode(self, text: str) -> List[int]:
        """Convert a string to token ids.

        Args:
            text (str): A string to convert.

        Returns:
            List[int]: Encoded list of ints.
        """
        pass
    
    @abstractmethod
    def decode(self, ids: List[int]) -> str:
        """Convert list of token ids to a string.

        Args:
            ids (List[int]): A list of token ids.

        Returns:
            str: Decoded string.
        """
        pass 

    @abstractmethod
    def encode_as_pieces(self, text: str) -> List[str]:
        """Tokenize a string to tokens.

        Args:
            text (str): Input string to tokenize.

        Returns:
            List[str]: A list of tokens.
        """
        pass 

    @abstractmethod
    def id_to_piece(self, ids: Union[List[int], int]) -> Union[List[str], str]:
        """Convert ids to tokens.

        Args:
            ids (List[int]): Token ids.

        Returns:
            List[str]: A list of tokens.
        """
        pass


class SentencePieceWrapper(BaseTokenizerInterface):
    
    def __init__(self, tokenizer: spm.SentencePieceProcessor):
        """Initialize sentencepiece wrapper.

        Args:
            tokenizer (SentencePieceProcessor): A SentencePieceProcessor.
        """
        self.sentence_piece_model = tokenizer
    
    def encode(self, text: str):
        ids = self.sentence_piece_model.encode(text)
        return ids
    
    def decode(self, ids: List[int]):
        if torch.is_tensor(ids):
            ids = ids.tolist()    
        text = self.sentence_piece_model.decode(ids)
        return text

    def encode_as_pieces(self, text: str) -> List[str]:
        return self.sentence_piece_model.encode_as_pieces(text)
    
    def id_to_piece(self, ids: Union[List[int], int]) -> Union[List[str], str]:
        if torch.is_tensor(ids):
            ids = ids.tolist()    
        return self.sentence_piece_model.id_to_piece(ids)


class HuggingFaceTokenizerWrapper(BaseTokenizerInterface):

    def __init__(
        self, 
        tokenizer: PreTrainedTokenizerBase
    ):
        """Initialize HuggingFace TokenizerBase wrapper.

        Args:
            tokenizer (PreTrainedTokenizerBase): HuggingFace TokenizerBase instance.
        """
        self.tokenizer = tokenizer
    
    def encode(self, text: str):
        encoded = self.tokenizer.encode(
            text=text,
            add_special_tokens=False,
        )
        return encoded
    
    def decode(self, ids: List[int]):
        decoded = self.tokenizer.decode(
            token_ids=ids,
            skip_special_tokens=True,
            clean_up_tokenization_spaces=True,
        )
        return decoded
    
    def encode_as_pieces(self, text: str) -> List[str]:
        return self.tokenizer.tokenize(text)
    
    def id_to_piece(self, ids: Union[List[int], int]) -> Union[List[str], str]:
        return self.tokenizer.convert_ids_to_tokens(ids)