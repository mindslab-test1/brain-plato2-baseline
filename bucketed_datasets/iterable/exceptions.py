class GeneratorIsEmpty(Exception):
    """A custom exception used for informing end-of-iteration state.
    """
    # Python 3.7 or later converts StopIteration to RuntimeError.
    pass