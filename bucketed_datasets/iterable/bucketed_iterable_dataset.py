from __future__ import annotations
import random, math, time, contextlib
from torch.utils.data import IterableDataset, Dataset, get_worker_info
from typing import (
    Callable,
    List, 
    Sequence,
    Union,
    Any,
    TypeVar,
    Generic,
    Iterator,
)
from more_itertools import peekable
from .exceptions import GeneratorIsEmpty
from tqdm import tqdm
from abc import ABC
from bucketed_datasets.length_calculators import LengthCalculator
import torch.distributed as dist

T = TypeVar('T')

class BucketedIterableCallbackHooksMixin(ABC):
    """A class that is responsible for implementing the callback hooks in BucketedIterableDataset.
    """
    def on_dataset_setup_end(self) -> None:
        """Called when the inner dataset setup is complete.
        """
        for callback in self.callbacks:
            callback.on_dataset_setup_end(self)

    def on_pool_setup_end(self) -> None:
        """Called when the pool is filled with samples.
        """
        for callback in self.callbacks:
            callback.on_pool_setup_end(self)

    def on_buckets_setup_end(self) -> None:
        """Called when the pool is split into buckets. DEPRECATED. DO NOT USE IT.
        """
        for callback in self.callbacks:
            callback.on_buckets_setup_end(self)

    def on_yield_samples_end(self, samples: List[T]) -> None:
        """Called when samples are yielded from the outer dataset.

        Args:
            samples (List[Any]): The list of samples to be yielded.
        """
        for callback in self.callbacks:
            callback.on_yield_samples_end(self, samples)

    def on_pool_exhausted(self) -> None:
        """Called when the pool of samples are exhausted.
        """
        for callback in self.callbacks:
            callback.on_pool_exhausted(self)

    def on_iter_end(self) -> None:
        """Called when the code reaches at the very end of the outer dataset iteration.
        """
        for callback in self.callbacks:
            callback.on_iter_end(self)

    def on_make_pool_start(self, num_samples_in_pool: int) -> None:
        """Called at the start ot the pool making process.

        Args:
            num_samples_in_pool (int): The maximum number of samples a pool can contain.
        """
        for callback in self.callbacks:
            callback.on_make_pool_start(self, num_samples_in_pool)

    def on_append_a_sample_to_pool_end(self) -> None:
        """Called when a sample is appended to the pool.
        """
        for callback in self.callbacks:
            callback.on_append_a_sample_to_pool_end(self)

    def on_make_pool_end(self) -> None:
        """Called when the pool making is complete.
        """
        for callback in self.callbacks:
            callback.on_make_pool_end(self)

    def on_dataset_exhausted(self) -> None:
        """Called when the inner dataset has been exhausted.
        """
        for callback in self.callbacks:
            callback.on_dataset_exhausted(self)


class Bucket(Generic[T]):
    """A class representation of the bucket in bucketed iterable dataset.

    Its functionalities include providing the iterable interface, shuffling the samples, peeking over the next sample,
    and returning the number of samples left in the current iteration.
    """
    def __init__(self, samples: List[T], *args, **kwargs):
        """Initializes the bucket with the samples it will contain.
        
        The property num_samples_left is set to None.

        Args:
            samples (List[T]): A list of samples the bucket will hold.
        """

        self.samples = samples 
        self._num_samples_left = None
    
    def __iter__(self) -> Iterator[T]:
        """Wraps peekable around the samples and sets the num_samples_left attribute to zero.

        Returns:
            Iterator[T]: An iterator that yields a sample at a time.
        """
        self.peekable_samples = peekable(self.samples)
        self._num_samples_left = len(self.samples)
        return self
    
    def __next__(self) -> T:
        """Returns the next sample from the bucket.

        The property num_samples_left is set to zero at the end of the iteration.

        Raises:
            StopIteration: If there are no samples left in the bucket.

        Returns:
            T: The sample that is at the top of the bucket.
        """
        try:
            next_sample = next(self.peekable_samples)
            self._num_samples_left -= 1
            return next_sample

        except StopIteration as e:
            self._num_samples_left = 0
            raise e
    
    def peek(self):
        """Peeks the sample at the top of the bucket without consuming the sample from the iteration.

        The property num_samples_left is set to zero at the end of the iteration.

        Raises:
            StopIteration: If there are no samples left in the bucket.

        Returns:
            T: The sample that is at the top of the bucket.
        """
        try:
            return self.peekable_samples.peek()
        except StopIteration as e:
            self._num_samples_left = 0
            raise e
    
    def __len__(self) -> int:
        """Returns the length of the whole samples it was given at initialization.

        Returns:
            int: The length of the samples it was given at initialization.
        """
        return len(self.samples)
    
        # Returns the number of samples left in the current iteration.
    def shuffle(self):
        """Shuffles the samples in place.
        """
        random.shuffle(self.samples)
    
    @property
    def num_samples_left(self) -> int:
        """Returns the number of samples left in the current iteration.

        Raises:
            AttributeError: If __iter__ has not been called at all, not even once.

        Returns:
            int: The number of samples left in the current iteration.
        """
        if self._num_samples_left is None:
            raise AttributeError
        return self._num_samples_left


class Buckets:
    """A class that allows to iterate over the samples from a list of buckets each of which containing a portion of the whole samples.
    """
    def __init__(self, buckets: List[Bucket[T]]):
        """Initializes an instance of the class with a list of bukcets each of which containing a portion of the entire samples.

        Args:
            buckets (List[Bucket]): A list of bukcets each of which containing a portion of the entire samples.
        """
        self.buckets = buckets
    
    def is_empty(self) -> bool:
        """Checks if all of the buckets in the current iteration are exhausted.

        Returns:
            bool: True if all of the buckets in the current iteration are empty, else False.
        """
        if len(self.iterable_buckets) > 0:
            return False
        else:
            return True

    def __next__(self) -> T:
        """Extracts the next sample from the buckets.

        Raises:
            StopIteration: If all of the buckets in the current iteration are empty.

        Returns:
            T: The next sample from the list of buckets.
        """
        while not self.is_empty():
            if self.cur_bucket is None:
                self.cur_bucket = self._sample_a_bucket()
            try:
                return next(self.cur_bucket)
            except StopIteration:
                self._remove_bucket(self.cur_bucket)
                self.cur_bucket = None
                continue
        raise StopIteration
    
    def peek(self) -> T:
        """Peeks the next sample in the current iteration without actually comsuming the sample.

        Raises:
            StopIteration: If all of the buckets in the current iteration are empty.

        Returns:
            T: The next sample in the current iteration.
        """
        while not self.is_empty():
            if self.cur_bucket is None:
                self.cur_bucket = self._sample_a_bucket()
            try: 
                return self.cur_bucket.peek()
            except StopIteration:
                self._remove_bucket(self.cur_bucket)
                self.cur_bucket = None
                continue
        raise StopIteration
    
    def _remove_bucket(self, bucket: Bucket) -> None:
        """Removes the bucket from the iterable buckets list.

        Args:
            bucket ([Bucket]): The bucket to remove from the list of iterable buckets.
        """
        self.iterable_buckets.remove(bucket)

    def _sample_a_bucket(self) -> Bucket:
        """Randomly samples a bucket from the list of iterable buckets list.

        The sampling is weighted by the number of samples left in a bucket.

        Raises:
            StopIteration: If all of the buckets in the iterable list of buckets are empty.

        Returns:
            Bucket: The sampled bucket.
        """
        if self.is_empty():
            raise StopIteration
        bucket = random.choices(
            self.iterable_buckets, 
            weights=[bucket.num_samples_left for bucket in self.iterable_buckets], 
            k=1,
        )[0]
        return bucket

    @contextlib.contextmanager
    def sample_a_bucket(self):
        """Samples a bucket from which next samples will be drawn.

        The samples will be drawn from the sampled bucket as long as the context is active AND the sampled bucket has not been exhausted.
        If the sampled bucket is exhausted and the context is still active, it will sample another bucket from the 
        list of buckets in the current iteration.
        Hence there is no guarantee that the samples will be drawn from the same bucket even if they were drawn from the same context.

        The samples are no longer guaranteed to be from the sampled bucket when the context exits.

        Raises:
            StopIteration: If all of the buckets in the current iteration are empty.
        """
        if self.is_empty():
            raise StopIteration
        self.cur_bucket = self._sample_a_bucket()
        yield
        self.cur_bucket = None

    def __iter__(self) -> Iterator[T]:
        """Creates a list of buckets that are in iterable state.

        Returns:
            Iterator[T]: An iterator that yields samples.
        """
        self.iterable_buckets = [iter(bucket) for bucket in self.buckets]
        return self


class BucketedIterableDataset(IterableDataset, BucketedIterableCallbackHooksMixin):
    """Wraps bucketizing functionality to an iterable dataset.
    
    It first draws a pool of samples from the given dataset.
    The pool of samples are then bucketized by lengths calculated by a length calculator.
    A list of similarly sized samples are yielded every time by sampling a bucket from the list of buckets.
    The above process repeats itself when the current pool is exhuasted.

    This helps reduce the size of the padding area in a batch that is induced by the differences in the lengths of the samples in the batch.
    """
    def __init__(
        self, 
        samples: Sequence[T], 
        dataset_constructor: Callable[[], Union[Dataset, IterableDataset]],
        # length_calculator: Callable[[Any], int],
        length_calculator: LengthCalculator,
        max_length_per_batch: int,
        num_samples_in_pool: int,
        num_buckets: int,
        callbacks: Sequence[BucketedIterableDatasetCallback],
        infinite: bool = False,
    ):
        """Initializes an instance of BucketedIterableDataset class by giving it the samples and the dataset constructor.

        Note:
            There is no guarantee that a list of samples yielded is from the same bucket because another bucket gets used
            when the current bucket becomes emtpy and max_length_per_bucket is not yet reached. (i.e., there is stiil a room for more samples.)

        Args:
            samples (Sequence[T]): A list of samples with which the dataset will be initialized.
            dataset_constructor (Callable[[], Union[Dataset, IterableDataset]]): A constructor that will initialize the inner dataset.
            length_calculator (LengthCalculator): Essentially a subclass of a Callable type that takes a sample and returns the length of the sample.
            max_length_per_batch (int): The maximum size of a batch.
                Guarantees that max(*next_sample_lengths) * len(next_samples) is less than or equal to max_length_per_batch.
            num_samples_in_pool (int): The maximum number of samples a pool can contain.
            num_buckets (int): The number of buckets.
            callbacks (Sequence[BucketedIterableDatasetCallback]): A list of callbacks to carry out extra works such as drawing a progress bar.
            infinite (bool, optional: If True, it will cycle the dataset infinitely. Defaults to False.
        """
        super().__init__()
        self.samples = samples
        self.length_calculator = length_calculator
        self.max_length_per_batch = max_length_per_batch
        self.num_samples_in_pool = num_samples_in_pool
        self.num_buckets = num_buckets
        self.dataset_constructor = dataset_constructor
        self.callbacks = callbacks
        self.infinite = infinite
    
    def __iter__(self) -> Iterator[List[T]]:
        """Starts iteration by calculating the assigned portion of the samples and initializing the inner dataset with them.

        Returns:
            Iterator[List[T]]: An iterator that yields a list of similarly sized samples.
        """
        assigned_works = self._get_assigned_works()
        self.dataset = self.dataset_constructor(assigned_works)
        self.dataset_iter = iter(self.dataset)
        self.epoch = 0
        self.on_dataset_setup_end()

        done = False 
        while not done:

            try:
                self._make_pool()
            except GeneratorIsEmpty:
                done = True

            if len(self.pool) > 0:
                self.on_pool_setup_end()
                buckets = self._make_buckets(self.pool)
                buckets = iter(buckets)

                while True:
                    next_sample_length = 0
                    samples = []
                    cur_max = 0

                    try:
                        with buckets.sample_a_bucket():
                            while max(cur_max, next_sample_length) * (len(samples) + 1) <= self.max_length_per_batch:
                                sample = next(buckets)
                                cur_max = max(cur_max, self.length_calculator.calculate_length(sample))
                                samples.append(sample)
                                next_sample_length = self.length_calculator.calculate_length(buckets.peek())
                    except StopIteration:
                        if len(samples) > 0:
                            yield samples
                            self.on_yield_samples_end(samples)
                        break

                    yield samples 
                    self.on_yield_samples_end(samples)
                self.on_pool_exhausted()

            if done and self.infinite:
                self.epoch += 1
                done = False
                self.on_dataset_exhausted()
                self.dataset_iter = iter(self.dataset)
                self.on_dataset_setup_end()

        self.on_iter_end()
        return

    def _make_pool(self) -> None:
        """Makes a pool of samples by extracting samples from the inner dataset.
        """
        self.on_make_pool_start(num_samples_in_pool=self.num_samples_in_pool)

        # TODO: Consider multiprocessing.
        self.pool = []
        while self.num_samples_in_pool > len(self.pool):
            sample = next(self.dataset_iter)
            self.pool.append(sample)
            self.on_append_a_sample_to_pool_end()

        self.on_make_pool_end()

    def _make_buckets(self, pool: List[T]) -> Buckets[T]:
        """Splits the pool of samples into buckets, most likely, of length num_buckets.

        Args:
            pool (List[T]): A pool of samples.

        Returns:
            Buckets[T]: Buckets of length num_buckets containing the split samples.
        """
        pool.sort(key=self.length_calculator.calculate_length) 
        num_samples_per_bucket = math.ceil(len(pool) / self.num_buckets)

        buckets = [
            Bucket(samples=pool[i : i + num_samples_per_bucket])
            for i in range(0, len(pool), num_samples_per_bucket)
        ]
        for bucket in buckets:
            bucket.shuffle()

        # TODO: Consider removing the assert statement.
        assert all([self.max_length_per_batch >= self.length_calculator.calculate_length(sample) for sample in pool])
        return Buckets(buckets=buckets)
    
    def _get_assigned_works(self) -> List[T]:
        """Extracts the assigned portion of the samples in case multiprocessing or DDP is used.

        Returns:
            List[T]: An assinged portion of the samples.
        """
        if dist.is_initialized():
            rank = dist.get_rank() if dist.get_rank() != -1 else 0
            world_size = dist.get_world_size() if dist.get_world_size() != -1 else 1
        else:
            rank = 0; world_size = 1
            
        worker_info = get_worker_info()
        num_workers = worker_info.num_workers if worker_info is not None else 1
        worker_id = worker_info.id if worker_info is not None else 0

        num_samples_per_worker = math.ceil(len(self.samples) / (num_workers * world_size))
        start_index = (num_workers * rank + worker_id) * num_samples_per_worker
        end_index = start_index + num_samples_per_worker
        return self.samples[start_index : end_index]


class BucketedIterableDatasetCallback(ABC):
    """BucketedIterableDatasetCallback interface.
    """
    def on_dataset_setup_end(self, dataset: BucketedIterableDataset) -> None:
        """Called when the inner dataset setup is complete.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
        """
        pass

    def on_pool_setup_end(self, dataset: BucketedIterableDataset) -> None:
        """Called when the pool is filled with samples.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
        """
        pass

    def on_buckets_setup_end(self, dataset: BucketedIterableDataset) -> None:
        """Called when the pool is split into buckets. DEPRECATED. DO NOT USE IT.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
        """
        pass

    def on_yield_samples_end(self, dataset: BucketedIterableDataset, samples: List[Any]) -> None:
        """Called when samples are yielded from the outer dataset.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
            samples (List[Any]): The samples to be yielded.
        """
        pass

    def on_pool_exhausted(self, dataset: BucketedIterableDataset) -> None:
        """Called when the current pool of samples are exhausted.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
        """
        pass

    def on_iter_end(self, dataset: BucketedIterableDataset) -> None:
        """Called when the code reaches the very end of the outer dataset iteration.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
        """
        pass
    
    def on_make_pool_start(self, dataset: BucketedIterableDataset, num_samples_in_pool: int) -> None:
        """Called at the start ot the pool making process.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
            num_samples_in_pool (int): The maximum number of samples a pool may contain.
        """
        pass

    def on_append_a_sample_to_pool_end(self, dataset: BucketedIterableDataset) -> None:
        """Called when a sample is appended to the pool.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
        """
        pass

    def on_make_pool_end(self, dataset: BucketedIterableDataset) -> None:
        """Called when the pool making is complete.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
        """
        pass

    def on_dataset_exhausted(self, dataset: BucketedIterableDataset) -> None:
        """Called when the inner dataset has been exhausted.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the callback is invoked.
        """
        pass


TQDM_BAR_FORMAT = '{desc:<60}|{percentage:3.0f}%|{bar:50}{r_bar}'
"""str: tqdm progress bar format string used in progress bar related callback class implementations.
"""

def get_worker_id() -> int:
    """Determines the worker id.

    Returns:
        int: The worker id. 0 is returned even if multiprocessing is not used.
    """
    worker_info = get_worker_info()
    num_workers = worker_info.num_workers if worker_info is not None else 1
    worker_id = worker_info.id if worker_info is not None else 0

    if dist.is_initialized():
        rank = dist.get_rank() if dist.get_rank() != -1 else 0
    else:
        rank = 0

    worker_id = num_workers * rank + worker_id

    return worker_id

class BucketedIterableDatasetDatasetProgressBarCallback(BucketedIterableDatasetCallback):
    """A BucketedIterableDatasetCallback that implements the dataset progress bar functionality.
    """
    def __init__(self, progress_bar_gap: int = 3, progress_bar_refresh_rate: float = 1.0):
        """Configures the progress bar's position and refresh rate.

        Args:
            progress_bar_gap (int, optional): The vertical position of the progress bar. Defaults to 3.
            progress_bar_refresh_rate (float, optional): The progress bar refresh rate in seconds. Defaults to 1.0.
        """
        super().__init__()
        self.progress_bar_gap = progress_bar_gap
        self.progress_bar_refresh_rate = progress_bar_refresh_rate
        self.last_refresh_time = float('-inf')
        self.epoch = 0

    def on_dataset_setup_end(self, dataset: BucketedIterableDataset):
        """Initializes the progress bar using tqdm.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
        """
        worker_id = get_worker_id()
        if dataset.infinite:
            desc = f'[WORKER_ID: {worker_id:02d}] | Epoch: {dataset.epoch:02d} | Partial Dataset Progress Bar:'
        else:
            desc = f'[WORKER_ID: {worker_id:02d}] | Partial Dataset Progress Bar:'
        self.pbar_dataset = tqdm(
            total=len(dataset.dataset), 
            position=worker_id * 2 + self.progress_bar_gap, 
            desc=desc,
            bar_format=TQDM_BAR_FORMAT,
            leave=False,
        )

    def on_pool_setup_end(self, dataset: BucketedIterableDataset):
        """Updates the progress bar by calculating the number of samples drawn since the last update.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
        """
        self.pbar_dataset.update(len(dataset.dataset) - dataset.dataset.num_samples_left - self.pbar_dataset.n)

    def on_yield_samples_end(self, dataset: BucketedIterableDataset, samples: List[Any]):
        """Refreshes the progress bar if the time passed from the last refresh is greater than the refresh rate.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
            samples (List[Any]): The samples to be yielded.
        """
        cur_time = time.time()
        if cur_time - self.last_refresh_time > self.progress_bar_refresh_rate:
            self.pbar_dataset.refresh()
            self.last_refresh_time = cur_time
    
    def on_dataset_exhausted(self, dataset: BucketedIterableDataset):
        """Resets the progress bar with the updated epoch information.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
        """
        worker_id = get_worker_id()
        if dataset.infinite:
            desc = f'[WORKER_ID: {worker_id:02d}] | Epoch: {dataset.epoch:02d} | Partial Dataset Progress Bar:'
        else:
            desc = f'[WORKER_ID: {worker_id:02d}] | Partial Dataset Progress Bar:'
        self.pbar_dataset.set_description(desc)
        

    def on_iter_end(self, dataset: BucketedIterableDataset):
        """Closes the progress bar.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
        """
        self.pbar_dataset.close()


class BucketedIterableDatasetPoolProgressBarCallback(BucketedIterableDatasetCallback):
    """A BucketedIterableDatasetCallback that implements certain pool-related progress bar functionalities.
    """
    def __init__(self, progress_bar_gap: int = 4, mininterval: float = 0.1):
        """Configures the progress bar's position and minimum refresh interval.

        Args:
            progress_bar_gap (int, optional): The vertical position of the progress bar. Defaults to 4.
            mininterval (float, optional): The minimum update interval of the progress bar. Defaults to 0.1.
        """
        super().__init__()
        self.progress_bar_gap = progress_bar_gap
        self.mininterval = mininterval

    def on_pool_setup_end(self, dataset: BucketedIterableDataset):
        """Initializes a progress bar to display the pool consuming progress.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
        """
        worker_id = get_worker_id()
        if dataset.infinite:
            desc = f'[WORKER ID: {worker_id:02d}] | Epoch: {dataset.epoch:02d} | Making a pool of samples:'
        else:
            desc = f'[WORKER ID: {worker_id:02d}] | Making a pool of samples:'
        self.pbar_pool = tqdm(
            total=len(dataset.pool),
            position=worker_id * 2 + self.progress_bar_gap, 
            desc=desc,
            bar_format=TQDM_BAR_FORMAT,
            leave=False,
            mininterval=self.mininterval,
        )

    def on_yield_samples_end(self, dataset: BucketedIterableDataset, samples: List[Any]):
        """Increments the progress bar's state by the number of samples to yielded.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
            samples (List[Any]): The samples to be yielded.
        """
        self.pbar_pool.update(len(samples))

    def on_pool_exhausted(self, dataset: BucketedIterableDataset):
        """Closes the pool-comsuming related progress bar.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
        """
        self.pbar_pool.close()

    def on_make_pool_start(self, dataset: BucketedIterableDataset, num_samples_in_pool: int) -> None:
        """Initializes a progress bar to display the pool making progress.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
            num_samples_in_pool (int): The maximum number of samples a pool can contain.
        """
        worker_id = get_worker_id()
        if dataset.infinite:
            desc = f'[WORKER ID: {worker_id:02d}] | Epoch: {dataset.epoch:02d} | Making a pool of samples:'
        else:
            desc = f'[WORKER ID: {worker_id:02d}] | Making a pool of samples:'
        # TODO: Should an independent attribute name be used?
        self.pbar_pool = tqdm(
            total=num_samples_in_pool,
            position=worker_id * 2 + self.progress_bar_gap, 
            desc=desc,
            bar_format=TQDM_BAR_FORMAT,
            leave=False,
        )

    def on_append_a_sample_to_pool_end(self, dataset: BucketedIterableDataset) -> None:
        """Increments the progress bar's state by one each time a sample is added to the pool in making.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
        """
        self.pbar_pool.update(1)

    def on_make_pool_end(self, dataset: BucketedIterableDataset) -> None:
        """Closes the pool-making related progress bar.

        Args:
            dataset (BucketedIterableDataset): A BucketedIterableDataset instance itself from which the method is invoked.
        """
        self.pbar_pool.close()
