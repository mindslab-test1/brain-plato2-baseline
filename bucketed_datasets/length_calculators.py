from abc import ABC, abstractmethod
from typing import Any, Union, Sequence
from collections import Mapping

# Encoder only samples' lengths are simple enough to calculate, so making a class out of it might be an overkill but they are here just in case.

class LengthCalculator(ABC):
    """A base class that defines the interface of all LengthCalculator classes.
    """
    # TODO: Should this be a callable?

    @abstractmethod
    def calculate_length(self, sample: Any) -> int:
        """An abstract method that defines the length calculation interface.

        Args:
            sample (Any): The sample from which the length will be calculated.

        Returns:
            int: The length of the sample.
        """
        pass


def get_encoder_only_input_ids(
    sample: Any, 
    input_ids_key_or_attribute: Union[str, Sequence[str]]
) -> Any:
    """Extracts input ids from a sample of encoder only model.

    Args:
        sample (Any): The sample. It could be a dict or some other object.
        input_ids_key_or_attribute (Union[str, Sequence[str]]): A list of keys or attribute names by which the input ids can be extracted.
            When there is only one key or attribute name to be specified, a str is also an acceptable argument.

    Raises:
        ValueError: If the input ids could not be found by the provided keys or arttribute names.

    Returns:
        Any: The input ids extracted from the sample. Typical types of it could be List or torch.LongTensor.
    """
    if isinstance(input_ids_key_or_attribute, str):
        input_ids_key_or_attribute = [input_ids_key_or_attribute]

    for key_or_attribute in input_ids_key_or_attribute: 
        if isinstance(sample, Mapping):
            sample = sample[key_or_attribute]

        elif hasattr(sample, key_or_attribute):
            sample = getattr(sample, key_or_attribute)

        else:
            raise ValueError('Failed to find a matching input_ids_key_or_attribute.')

    return sample
    

class EncoderOnlyQuadratic(LengthCalculator):
    """A class used for calculating the quadratic length of the input destined for an encoder only model.
    """
    def __init__(
        self, 
        input_ids_key_or_attribute: Union[str, Sequence[str]],
    ):
        """Initializes an instance of EncoderOnlyQuadratic class.

        Args:
            input_ids_key_or_attribute (Union[str, Sequence[str]]): A list of keys or attribute names by which the input ids can be extracted.
                When there is only one key or attribute name to be specified, a str is also an acceptable argument.
        """
        self.input_ids_key_or_attribute = input_ids_key_or_attribute
    
    def calculate_length(self, sample: Any) -> int:
        """Calculates the quadratic length of the sample with given keys or attribute names.

        Args:
            sample (Any): A sample of the encoder only model. This could be a dict or any other object.

        Returns:
            int: The length of the sample squared.
        """
        input_ids = get_encoder_only_input_ids(sample, self.input_ids_key_or_attribute) 
        length = len(input_ids) ** 2
        return length


class EncoderOnlyLinear(LengthCalculator):
    """A class used for calculating the linear length of the input destined for an encoder only model.
    """
    def __init__(
        self, 
        input_ids_key_or_attribute: Union[str, Sequence[str]],
    ):
        """Initializes an instance of EncoderOnlyLinear class.

        Args:
            input_ids_key_or_attribute (Union[str, Sequence[str]]): A list of keys or attribute names by which the input ids can be extracted.
                When there is only one key or attribute name to be specified, a str is also an acceptable argument.
        """
        self.input_ids_key_or_attribute = input_ids_key_or_attribute
    
    def calculate_length(self, sample: Any) -> int:
        """Calculates the length of the sample with given keys or attribute names.

        Args:
            sample (Any): A sample of the encoder only model. This could be a dict or any other object.

        Returns:
            int: The length of the sample.
        """
        input_ids = get_encoder_only_input_ids(sample, self.input_ids_key_or_attribute) 
        length = len(input_ids)
        return length